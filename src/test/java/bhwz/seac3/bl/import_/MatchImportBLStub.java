package bhwz.seac3.bl.import_;

import java.io.File;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import bhwz.seac3.bl.import_.Data;
import bhwz.seac3.bl.import_.MatchImportBL;
import bhwz.seac3.blservice.ReDisplayControllerBLService;
import bhwz.seac3.blservice.import_.ErrorLogPrinter;
import bhwz.seac3.db.nbadata.SaveQueryDB;
import bhwz.seac3.dbservice.nbadata.SaveQueryDBService;
import bhwz.seac3.po.MatchPo;
import bhwz.seac3.po.PlayerScoreTablePo;

public class MatchImportBLStub extends MatchImportBL {
	public MatchImportBLStub(ReDisplayControllerBLService autoDisplayController) {
		super(autoDisplayController);
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see bhwz.seac3.bl.import_.MatchImportBL#importMatch(java.lang.String, bhwz.seac3.blservice.import_.ErrorLogPrinter)
	 */
	@Override
	public void importMatch(String path, ErrorLogPrinter elp) {
		if (Data.isValid(path)) {
			Data.withAllData(new File[1], new MockMatchesDataCheck(),
					file -> {
						SaveQueryDBService<MatchPo> db = new SaveQueryDB<>();
						try {
							db.save(_matchPo(file));
						} catch (Exception e) {
							e.printStackTrace();
						}
					}, elp);
		} else {
			elp.print("path is invalid!");
		}
	}

	private MatchPo _matchPo(File file) {
		MatchPo matchPo = new MatchPo();
		matchPo.setDate(System.currentTimeMillis());
		matchPo.setTeam1("t1");
		matchPo.setTeam2("t3");
		matchPo.setTeam2Score(100);
		matchPo.setTeam2Score(200);
		matchPo.setSectionScores(Arrays.asList("50-30","20-10","30-40","40-60"));
		Set<PlayerScoreTablePo> s=new HashSet<>();
		PlayerScoreTablePo s1=new PlayerScoreTablePo();
		s1.set三分出手数(3);
		s1.set三分命中数(2);
		PlayerScoreTablePo s2=new PlayerScoreTablePo();
		s2.set三分出手数(6);
		s2.set三分命中数(4);
		s.add(s1);
		s.add(s2);
		matchPo.setTeam1PlayersScore(s);
		return matchPo;
	}

}
