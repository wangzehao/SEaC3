package bhwz.seac3.blservice;

import org.junit.Test;

import bhwz.seac3.LocalBLServiceManager;
import bhwz.seac3.blservice.import_.ImportBLService;

public class ImportBLServiceTest {

	@Test
	public void test() {
		ImportBLService bl = LocalBLServiceManager.getImportBLService();
		bl._import("DataDemo",
				s -> System.out.println(s));
	}

}
