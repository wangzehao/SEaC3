package bhwz.seac3.blservice;

import org.junit.Test;

import bhwz.seac3.LocalBLServiceManager;
import bhwz.seac3.RMIClientBLServiceManager;
import bhwz.seac3.blservice.import_.ImportBLService;
import bhwz.seac3.blservice.query.HotspotQueryBLService;
import bhwz.seac3.blservice.query.MatchQueryBLService;
import bhwz.seac3.blservice.query.PlayerQueryBLService;
import bhwz.seac3.blservice.query.TeamQueryBLService;

public class QueryBLServiceTest {

	@Test
	public void test() {
		ImportBLService bl = LocalBLServiceManager.getImportBLService();
		bl._import("DataDemo",
				s -> System.out.println(s));
		HotspotQueryBLService hbl=RMIClientBLServiceManager.getBLService(HotspotQueryBLService.class);
		try {
			hbl.QueryPlayersRankedBy条件("13-14");
		hbl.QueryPlayersRankedBy条件("得分");
		hbl.QueryPlayersRankedBy条件("篮板");
		hbl.QueryPlayersRankedBy条件("助攻");
		hbl.QueryPlayersRankedBy条件("盖帽");
		hbl.QueryPlayersRankedBy条件("抢断");
		hbl.QueryPlayersRankedBy条件("场均得分");
		hbl.QueryPlayersRankedBy条件("场均篮板");
		hbl.QueryPlayersRankedBy条件("场均助攻");
		hbl.QueryPlayersRankedBy条件("场均盖帽");
		hbl.QueryPlayersRankedBy条件("场均抢断");
		hbl.QueryPlayersRankedBy条件("三分%");
		hbl.QueryPlayersRankedBy条件("投篮%");
		hbl.QueryPlayersRankedBy条件("罚球%");
		hbl.QueryPlayersRankedBy条件("场均得分进步");
		hbl.QueryPlayersRankedBy条件("场均篮板进步");
		hbl.QueryPlayersRankedBy条件("场均助攻进步");
		
		hbl.QueryTeamsRankedBy条件("场均得分");
		hbl.QueryTeamsRankedBy条件("场均篮板");
		hbl.QueryTeamsRankedBy条件("场均助攻");
		hbl.QueryTeamsRankedBy条件("场均盖帽");
		hbl.QueryTeamsRankedBy条件("场均抢断");
		hbl.QueryTeamsRankedBy条件("三分%");
		hbl.QueryTeamsRankedBy条件("投篮%");
		hbl.QueryTeamsRankedBy条件("罚球%");
		hbl.QueryTeamsRankedBy条件("犯规");
		hbl.QueryTeamsRankedBy条件("失误");
		hbl.QueryTeamsRankedBy条件("防守篮板数");
		hbl.QueryTeamsRankedBy条件("进攻篮板数");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		MatchQueryBLService mbl=RMIClientBLServiceManager.getBLService(MatchQueryBLService.class);
		try {
			mbl.queryAllMatches("13-14");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		PlayerQueryBLService pbl=RMIClientBLServiceManager.getBLService(PlayerQueryBLService.class);
		try {
			pbl.query场均数据("13-14");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			pbl.query赛季总数据("13-14");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		TeamQueryBLService tbl=RMIClientBLServiceManager.getBLService(TeamQueryBLService.class);
		try {
			tbl.query场均数据("13-14");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			tbl.query赛季总数据("13-14");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
