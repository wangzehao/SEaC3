package bhwz.seac3.dbservice;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;

import bhwz.seac3.DBServiceManager;
import bhwz.seac3.dbservice.nbadata.SaveQueryDBService;
import bhwz.seac3.dbservice.nbadata.SeasonDBService;
import bhwz.seac3.po.MatchPo;
import bhwz.seac3.po.PlayerPo;
import bhwz.seac3.po.PlayerScoreTablePo;
import bhwz.seac3.po.TeamPo;

public class SeasonDBTest {
	
	SeasonDBService sdb=DBServiceManager.getSeasonDBService();
	SaveQueryDBService<MatchPo> mdb=DBServiceManager.getMatchDBService();

	@Test
	public void test() throws Exception {
		MatchPo m1=new MatchPo();
		m1.setTeam1("t123");
		m1.setTeam2("t223");
		Set<PlayerScoreTablePo> team1PlayersScore=new HashSet<PlayerScoreTablePo>();
		team1PlayersScore.add(new PlayerScoreTablePo(){
			{
				this.set球员名("t123player");
			}
		});
		m1.setTeam1PlayersScore(team1PlayersScore);
		m1.setDate(System.currentTimeMillis());
		m1.setSeason("12-13");
		
		MatchPo m2=new MatchPo();
		m2.setTeam1("t111");
		m2.setTeam2("t222");
		m2.setDate(System.currentTimeMillis());
		m2.setSeason("13-14");
		
		mdb.save(m1);
		mdb.save(m2);
		
		List<String> seasons=sdb.queryAllSeasons();
		assertTrue(seasons.get(1).equals("12-13"));
		assertTrue(seasons.get(0).equals("13-14"));
		
		TeamPo tqid=new TeamPo();
		tqid.set缩写("t123");
		seasons=sdb.queryJoinedSeasons(tqid);
		assertTrue(seasons.contains("12-13"));
		assertTrue(!seasons.contains("13-14"));
		
		PlayerPo pqid=new PlayerPo();
		pqid.setName("t123player");
		seasons=sdb.queryJoinedSeasons(pqid);
		assertTrue(seasons.contains("12-13"));
		assertTrue(!seasons.contains("13-14"));
		
		mdb.deleteById(m1);
		mdb.deleteById(m2);
	}

}
