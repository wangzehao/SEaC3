package bhwz.seac3.dbservice;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import bhwz.seac3.DBServiceManager;
import bhwz.seac3.dbservice.nbadata.PlayerMatchDBService;
import bhwz.seac3.dbservice.nbadata.SaveQueryDBService;
import bhwz.seac3.dbservice.nbadata.TeamMatchDBService;
import bhwz.seac3.po.MatchPo;
import bhwz.seac3.po.PlayerPo;
import bhwz.seac3.po.PlayerScoreTablePo;

public class PlayerAndTeamMatchDBServiceTest {
	private SaveQueryDBService<MatchPo> mdb = DBServiceManager.getMatchDBService();
	private PlayerMatchDBService pmdb=DBServiceManager.getPlayerMatchDBService();
	private TeamMatchDBService tmdb=DBServiceManager.geTeamMatchDBService();

	@Test
	public void test() {
		try{
			MatchPo m1=new MatchPo();
			m1.setDate(System.currentTimeMillis());
			m1.setTeam1("t11");
			m1.setTeam2("t22");
			m1.setTeam1Score(100);
			m1.setTeam2Score(200);
			m1.setSectionScores(Arrays.asList("50-100","50-100"));
			Set<PlayerScoreTablePo> s=new HashSet<>();
			PlayerScoreTablePo st1=new PlayerScoreTablePo();
			st1.set球员名("player1");
			st1.set三分出手数(3);
			st1.set三分命中数(2);
			PlayerScoreTablePo st2=new PlayerScoreTablePo();
			st2.set球员名("player2");
			st2.set三分出手数(6);
			st2.set三分命中数(4);
			s.add(st1);
			s.add(st2);
			m1.setTeam1PlayersScore(s);
			mdb.save(m1);
			
			addMatchToPlayer(m1, st1.get球员名());
			addMatchToPlayer(m1, st2.get球员名());
			
			MatchPo m2=new MatchPo();
			m2.setDate(System.currentTimeMillis());
			m2.setTeam1("t11");
			m2.setTeam2("t22");
			Set<PlayerScoreTablePo> s2=new HashSet<>();
			PlayerScoreTablePo s2t1=new PlayerScoreTablePo();
			s2t1.set球员名("player1");
			s2t1.set三分出手数(4);
			s2t1.set三分命中数(2);
			s2.add(s2t1);
			m2.setTeam2PlayersScore(s2);
			mdb.save(m2);
			
			addMatchToPlayer(m2, s2t1.get球员名());
			
			//PlayerMatchDBServiceTest
			assertTrue(pmdb.getPlayerMatchList("player1",null).contains(m1));
			assertTrue(pmdb.getPlayerMatchList("player1",null).contains(m2));
			assertTrue(pmdb.getPlayerMatchList("player2",null).contains(m1));
			
			//TeamMatchDBServiecTest
			assertTrue(tmdb.getTeamMatchList("t11", null).contains(m1));
			assertTrue(tmdb.getTeamMatchList("t11", null).contains(m2));
			assertTrue(tmdb.getTeamMatchList("t22", null).contains(m1));
			assertTrue(tmdb.getTeamMatchList("t22", null).contains(m2));
			
			mdb.deleteById(m1);
			mdb.deleteById(m2);
		}catch(Exception e){
			e.printStackTrace();
			fail(e.toString());
		}
	}

	private void addMatchToPlayer(MatchPo m,String playerName) throws Exception{
		PlayerPo p=new PlayerPo();
		p.setName(playerName);
		pmdb.addMatchToPlayer(m, p);
	}

}
