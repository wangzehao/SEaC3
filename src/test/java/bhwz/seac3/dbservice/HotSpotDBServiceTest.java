package bhwz.seac3.dbservice;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import bhwz.seac3.DBServiceManager;
import bhwz.seac3.dbservice.nbadata.HotSpotDBService;
import bhwz.seac3.dbservice.nbadata.SaveQueryDBService;
import bhwz.seac3.po.MatchPo;

public class HotSpotDBServiceTest {
	SaveQueryDBService<MatchPo> mdb=DBServiceManager.getMatchDBService();
	HotSpotDBService hdb=DBServiceManager.getHotSpotDBService();
	@Test
	public void test() {
		long nineDaysAfter=System.currentTimeMillis()+9*24*60*60*1000;
		try {
			Thread.sleep(10);
		} catch (InterruptedException e) {
		}
		long tenDaysAfter=System.currentTimeMillis()+10*24*60*60*1000;
		MatchPo m1=new MatchPo();
		m1.setDate(nineDaysAfter);
		m1.setTeam1("t1");
		m1.setTeam2("t2");
		MatchPo m2=new MatchPo();
		m2.setDate(tenDaysAfter);
		m2.setTeam1("t3");
		m2.setTeam2("t4");
		MatchPo m3=new MatchPo();
		m3.setDate(tenDaysAfter);
		m3.setTeam1("t5");
		m3.setTeam2("t6");
		List<MatchPo> result=null;
		try {
			mdb.save(m1);
			mdb.save(m2);
			mdb.save(m3);
			result=hdb.query当天比赛();
		} catch (Exception e) {
			e.printStackTrace();
		}		
		assertTrue(result.size()==2);
		for(MatchPo po:result){
			assertTrue(!po.getTeam1().equals("t1"));
		}
	}

}
