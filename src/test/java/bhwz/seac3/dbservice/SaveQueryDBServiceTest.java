package bhwz.seac3.dbservice;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;

import bhwz.seac3.DBServiceManager;
import bhwz.seac3.dbservice.nbadata.SaveQueryDBService;
import bhwz.seac3.po.MatchPo;
import bhwz.seac3.po.PlayerScoreTablePo;
import bhwz.seac3.po.TeamGameDataPo;
import bhwz.seac3.po.TeamPo;

public class SaveQueryDBServiceTest {
	private SaveQueryDBService<TeamPo> tdb = DBServiceManager
			.getTeamDBService();
	private SaveQueryDBService<MatchPo> mdb = DBServiceManager
			.getMatchDBService();
	private SaveQueryDBService<TeamGameDataPo> pddb = DBServiceManager
			.getTeamGameDataDBService();

	@Test
	public void testTeam() {
		try {
			TeamPo t1 = new TeamPo();
			t1.set缩写("p1");
			t1.set球队全名("p1f");
			t1.set主场("zc");
			t1.set建立时间("jlsj");
			t1.set所在地("szd");
			t1.set赛区("sq");
			tdb.save(t1);// test save
			List<TeamPo> l = tdb.queryAll(t1.getClass());// test queryAll
			assertTrue(l.contains(t1));
			assertTrue(tdb.queryById(t1).equals(t1));// test queryById
			t1.set球队全名("p1f2");
			tdb.updateById(t1);// test updateById
			assertTrue(tdb.queryById(t1).equals(t1));
			tdb.deleteById(t1);// test deleteById
			assertTrue(tdb.queryById(t1) == null);
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}

	@Test
	public void testMatch() {
		try {
			MatchPo m1 = new MatchPo();
			m1.setDate(System.currentTimeMillis());
			m1.setTeam1("t1");
			m1.setTeam2("t2");
			m1.setTeam1Score(100);
			m1.setTeam2Score(200);
			m1.setSectionScores(Arrays.asList("50-100", "50-100"));
			Set<PlayerScoreTablePo> s = new HashSet<>();
			PlayerScoreTablePo s1 = new PlayerScoreTablePo();
			s1.set三分出手数(3);
			s1.set三分命中数(2);
			PlayerScoreTablePo s2 = new PlayerScoreTablePo();
			s2.set三分出手数(6);
			s2.set三分命中数(4);
			s.add(s1);
			s.add(s2);
			m1.setTeam1PlayersScore(s);
			mdb.save(m1);
			assertTrue(mdb.queryAll(m1.getClass()).contains(m1));
			assertTrue(mdb.queryById(m1).equals(m1));
			mdb.deleteById(m1);
			assertTrue(mdb.queryById(m1) == null);
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}

	@Test
	public void testGameData() {
		try {
			TeamGameDataPo d1 = new TeamGameDataPo();
			d1.set赛季("test");
			d1.set球队简称("ABC");
			d1.set胜利场数(10);
			pddb.save(d1);
			assertTrue(pddb.queryAll(TeamGameDataPo.class).contains(d1));
			assertTrue(pddb.queryById(d1).equals(d1));
			d1.set胜利场数(20);
			pddb.updateById(d1);
			assertTrue(pddb.queryById(d1).equals(d1));
			pddb.deleteById(d1);
			assertTrue(pddb.queryById(d1)==null);
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}

}
