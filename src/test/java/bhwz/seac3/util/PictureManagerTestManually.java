package bhwz.seac3.util;

import java.awt.Image;
import java.io.IOException;

import javax.swing.JFrame;

import org.junit.Test;

public class PictureManagerTestManually {

	@Test
	public void test() throws InterruptedException, IOException, ClassNotFoundException {
		final String path="images/Aaron Brooks.png";
		byte[] byteimage=new PictureManager("png").encodeImage(path);
		Image image=new PictureManager("png").decodeImage(byteimage);
		JFrame f = new JFrame(){
			private static final long serialVersionUID = 1L;

			@Override
			public void paint(java.awt.Graphics g) {
				super.paint(g);
				g.drawImage(image, 0, 0, null);
			}
		};
		f.setBounds(0, 0, 1600, 900);
		f.setVisible(true);
		Thread.sleep(3000);
	}

}
