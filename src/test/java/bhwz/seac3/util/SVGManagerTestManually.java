package bhwz.seac3.util;

import java.io.*;

import javax.swing.*;

import org.junit.Test;
import org.w3c.dom.svg.SVGDocument;
import org.apache.batik.swing.JSVGCanvas;

public class SVGManagerTestManually {

	@Test
	public void test() throws IOException, InterruptedException, ClassNotFoundException {
		final String path="images/ATL.svg";
		JFrame f = new JFrame();
		JSVGCanvas canvas = new JSVGCanvas();
		f.add(canvas);
		SVGDocument svgd;
		byte[] bytesvg=new SVGManager().encodeImage(path);
		svgd =new SVGManager().decodeImage(bytesvg);
		canvas.setSVGDocument(svgd);
		canvas.setBounds(0, 0, 900, 800);
		f.setBounds(0, 0, 1600, 900);
		f.setVisible(true);
		Thread.sleep(3000);
	}

}
