package bhwz.seac3.util;

import static org.junit.Assert.*;

import org.junit.Test;

public class RecursiveCommandExecuterTest {

	@Test
	public void test() {
		String cmd = "c";
		String[] result = { "r" };
		RecursiveCommandExecuter<String> father = new RecursiveCommandExecuter<String>();
		father.addExecuteWay(c -> result[0] = result[0] + "-f=" + c);
		father.addExecuteWay(c -> result[0] = result[0] + "-f=" + c);
		father.setPreCommandProcessor(c -> c + "*prep");
		father.setPostCommandProcessor(c -> c + "*postp");

		RecursiveCommandExecuter<String> child1 = new RecursiveCommandExecuter<String>();
		child1.addExecuteWay(c -> result[0] = result[0] + "-c1=" + c);
		father.addSubExecuter(child1);
		RecursiveCommandExecuter<String> child2 = new RecursiveCommandExecuter<String>();
		child2.addExecuteWay(c -> result[0] = result[0] + "-c2=" + c);
		father.addSubExecuter(child2);

		try {
			father.execute(cmd);
		} catch (Exception e) {
			e.printStackTrace();
		}

		assertTrue(result[0].startsWith("r-f=c*prep-f=c*prep"));
		assertTrue(result[0].endsWith("-c1=c*prep*postp-c2=c*prep*postp")
				|| result[0].endsWith("-c2=c*prep*postp-c1=c*prep*postp"));
	}

}
