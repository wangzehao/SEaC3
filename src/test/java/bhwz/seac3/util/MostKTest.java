package bhwz.seac3.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class MostKTest {
	

	private int k;
	private List<ComparableData> list;
	private MostK m;
	
	@Before
	public void init(){
		k=5;
		list=new ArrayList<ComparableData>();
		String nameList[]={"hyy","byc","wzh","zp","msy","fx","asd","sdf","dfg","zxc"};
		String season="13-14";
		double dataList[]={3,2,4,1,6,0,6,0,4,5};
		ComparableData data=null;
		for(int i=0;i<10;i++){
			data=new ComparableData();
			data.setName(nameList[i]);
			data.setSeason(season);
			data.setData(dataList[i]);
			list.add(data);
		}
	}

	@Test
	public void testGetMostK() {
	    m=new MostK(k,list);
	    List<ComparableData> result=m.getMostK();
	    Iterator<ComparableData> itor=result.iterator();
	    while(itor.hasNext()){
	    	ComparableData temp=itor.next();
	    	print(temp);
	    }
	}
	
	private void print(ComparableData data){
		System.out.println("名字:"+data.getName());
		System.out.println("赛季:"+data.getSeason());
		System.out.println("数据:"+data.getData());
	}


}
