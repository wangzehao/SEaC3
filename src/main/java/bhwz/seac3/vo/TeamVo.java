package bhwz.seac3.vo;

import java.util.Arrays;

public class TeamVo extends Vo{
	private static final long serialVersionUID = -4745831391029238573L;
	//from TeamPo
	private String 球队简称;
	private String 球队全名;	
	private String 所在地;
	private String 赛区;
	private String 分区;
	private String 主场;
	private String 建立时间;	
	//from TeamIconPo
	private byte[] teamIcon;
	//from TeamGameDataPo
	private int 胜利场数;	
	private int 对手得分;
	private double 进攻回合;
	private double 防守回合;
	private int 对手进攻篮板数;
	private int 对方防守篮板数;
	//from GameDataPo
	private String 赛季;
	private int 比赛场数;
	private double 投篮数;
	private double 罚球数;
	private double 失误数;	
	private double 断球数;
	private double 助攻数;
	private double 盖帽数;
	private double 犯规数;
	private double 比赛得分;
	private double 投篮命中数;
	private double 三分命中数;
	private double 三分出手数;
	private double 罚球命中数;
	private double 前场篮板数;
	private double 后场篮板数;	
	//from DataComputer
	private double 命中率;
	private double 三分命中率;
	private double 罚球命中率;
	private double 篮板数;
	private double 胜率;
	private double 进攻效率;
	private double 防守效率;
	private double 进攻篮板效率;
	private double 防守篮板效率;
	private double 抢断效率;
	private double 助攻率;
	private double 比赛信息;
	
	public long get比赛日期() {
		return 比赛日期;
	}
	public void set比赛日期(long 比赛日期) {
		this.比赛日期 = 比赛日期;
	}
	private long 比赛日期;
	public String get球队简称() {
		return 球队简称;
	}
	public double get比赛信息() {
		return 比赛信息;
	}
	public void set比赛信息(double 比赛信息) {
		this.比赛信息 = 比赛信息;
	}
	public void set球队简称(String 球队简称) {
		this.球队简称 = 球队简称;
	}
	public String get球队全名() {
		return 球队全名;
	}
	public void set球队全名(String 球队全名) {
		this.球队全名 = 球队全名;
	}
	public String get所在地() {
		return 所在地;
	}
	public void set所在地(String 所在地) {
		this.所在地 = 所在地;
	}
	public String get赛区() {
		return 赛区;
	}
	public void set赛区(String 赛区) {
		this.赛区 = 赛区;
	}
	public String get分区() {
		return 分区;
	}
	public void set分区(String 分区) {
		this.分区 = 分区;
	}
	public String get主场() {
		return 主场;
	}
	public void set主场(String 主场) {
		this.主场 = 主场;
	}
	public String get建立时间() {
		return 建立时间;
	}
	public void set建立时间(String 建立时间) {
		this.建立时间 = 建立时间;
	}
	public byte[] getTeamIcon() {
		return teamIcon;
	}
	public void setTeamIcon(byte[] teamIcon) {
		this.teamIcon = teamIcon;
	}
	public int get胜利场数() {
		return 胜利场数;
	}
	public void set胜利场数(int 胜利场数) {
		this.胜利场数 = 胜利场数;
	}
	public int get对手得分() {
		return 对手得分;
	}
	public void set对手得分(int 对手得分) {
		this.对手得分 = 对手得分;
	}
	public double get进攻回合() {
		return 进攻回合;
	}
	public void set进攻回合(double 进攻回合) {
		this.进攻回合 = 进攻回合;
	}
	public double get防守回合() {
		return 防守回合;
	}
	public void set防守回合(double 防守回合) {
		this.防守回合 = 防守回合;
	}
	public int get对手进攻篮板数() {
		return 对手进攻篮板数;
	}
	public void set对手进攻篮板数(int 对手进攻篮板数) {
		this.对手进攻篮板数 = 对手进攻篮板数;
	}
	public int get对方防守篮板数() {
		return 对方防守篮板数;
	}
	public void set对方防守篮板数(int 对方防守篮板数) {
		this.对方防守篮板数 = 对方防守篮板数;
	}
	public String get赛季() {
		return 赛季;
	}
	public void set赛季(String 赛季) {
		this.赛季 = 赛季;
	}
	public int get比赛场数() {
		return 比赛场数;
	}
	public void set比赛场数(int 比赛场数) {
		this.比赛场数 = 比赛场数;
	}
	public double get投篮数() {
		return 投篮数;
	}
	public void set投篮数(double 投篮数) {
		this.投篮数 = 投篮数;
	}
	public double get罚球数() {
		return 罚球数;
	}
	public void set罚球数(double 罚球数) {
		this.罚球数 = 罚球数;
	}
	public double get失误数() {
		return 失误数;
	}
	public void set失误数(double 失误数) {
		this.失误数 = 失误数;
	}
	public double get断球数() {
		return 断球数;
	}
	public void set断球数(double 断球数) {
		this.断球数 = 断球数;
	}
	public double get助攻数() {
		return 助攻数;
	}
	public void set助攻数(double 助攻数) {
		this.助攻数 = 助攻数;
	}
	public double get盖帽数() {
		return 盖帽数;
	}
	public void set盖帽数(double 盖帽数) {
		this.盖帽数 = 盖帽数;
	}
	public double get犯规数() {
		return 犯规数;
	}
	public void set犯规数(double 犯规数) {
		this.犯规数 = 犯规数;
	}
	public double get比赛得分() {
		return 比赛得分;
	}
	public void set比赛得分(double 比赛得分) {
		this.比赛得分 = 比赛得分;
	}
	public double get投篮命中数() {
		return 投篮命中数;
	}
	public void set投篮命中数(double 投篮命中数) {
		this.投篮命中数 = 投篮命中数;
	}
	public double get三分命中数() {
		return 三分命中数;
	}
	public void set三分命中数(double 三分命中数) {
		this.三分命中数 = 三分命中数;
	}
	public double get三分出手数() {
		return 三分出手数;
	}
	public void set三分出手数(double 三分出手数) {
		this.三分出手数 = 三分出手数;
	}
	public double get罚球命中数() {
		return 罚球命中数;
	}
	public void set罚球命中数(double 罚球命中数) {
		this.罚球命中数 = 罚球命中数;
	}
	public double get前场篮板数() {
		return 前场篮板数;
	}
	public void set前场篮板数(double 前场篮板数) {
		this.前场篮板数 = 前场篮板数;
	}
	public double get后场篮板数() {
		return 后场篮板数;
	}
	public void set后场篮板数(double 后场篮板数) {
		this.后场篮板数 = 后场篮板数;
	}
	public double get命中率() {
		return 命中率;
	}
	public void set命中率(double 命中率) {
		this.命中率 = 命中率;
	}
	public double get三分命中率() {
		return 三分命中率;
	}
	public void set三分命中率(double 三分命中率) {
		this.三分命中率 = 三分命中率;
	}
	public double get罚球命中率() {
		return 罚球命中率;
	}
	public void set罚球命中率(double 罚球命中率) {
		this.罚球命中率 = 罚球命中率;
	}
	public double get篮板数() {
		return 篮板数;
	}
	public void set篮板数(double 篮板数) {
		this.篮板数 = 篮板数;
	}
	public double get胜率() {
		return 胜率;
	}
	public void set胜率(double 胜率) {
		this.胜率 = 胜率;
	}
	public double get进攻效率() {
		return 进攻效率;
	}
	public void set进攻效率(double 进攻效率) {
		this.进攻效率 = 进攻效率;
	}
	public double get防守效率() {
		return 防守效率;
	}
	public void set防守效率(double 防守效率) {
		this.防守效率 = 防守效率;
	}
	public double get进攻篮板效率() {
		return 进攻篮板效率;
	}
	public void set进攻篮板效率(double 进攻篮板效率) {
		this.进攻篮板效率 = 进攻篮板效率;
	}
	public double get防守篮板效率() {
		return 防守篮板效率;
	}
	public void set防守篮板效率(double 防守篮板效率) {
		this.防守篮板效率 = 防守篮板效率;
	}
	public double get抢断效率() {
		return 抢断效率;
	}
	public void set抢断效率(double 抢断效率) {
		this.抢断效率 = 抢断效率;
	}
	public double get助攻率() {
		return 助攻率;
	}
	public void set助攻率(double 助攻率) {
		this.助攻率 = 助攻率;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(teamIcon);
		long temp;
		temp = Double.doubleToLongBits(三分出手数);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(三分命中数);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(三分命中率);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((主场 == null) ? 0 : 主场.hashCode());
		result = prime * result + ((分区 == null) ? 0 : 分区.hashCode());
		temp = Double.doubleToLongBits(前场篮板数);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(助攻数);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(助攻率);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(后场篮板数);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(命中率);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(失误数);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + 对手得分;
		result = prime * result + 对手进攻篮板数;
		result = prime * result + 对方防守篮板数;
		result = prime * result + ((建立时间 == null) ? 0 : 建立时间.hashCode());
		result = prime * result + ((所在地 == null) ? 0 : 所在地.hashCode());
		temp = Double.doubleToLongBits(投篮命中数);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(投篮数);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(抢断效率);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(断球数);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(比赛信息);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + 比赛场数;
		temp = Double.doubleToLongBits(比赛得分);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + (int) (比赛日期 ^ (比赛日期 >>> 32));
		temp = Double.doubleToLongBits(犯规数);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((球队全名 == null) ? 0 : 球队全名.hashCode());
		result = prime * result + ((球队简称 == null) ? 0 : 球队简称.hashCode());
		temp = Double.doubleToLongBits(盖帽数);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(篮板数);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(罚球命中数);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(罚球命中率);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(罚球数);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + 胜利场数;
		temp = Double.doubleToLongBits(胜率);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((赛区 == null) ? 0 : 赛区.hashCode());
		result = prime * result + ((赛季 == null) ? 0 : 赛季.hashCode());
		temp = Double.doubleToLongBits(进攻回合);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(进攻效率);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(进攻篮板效率);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(防守回合);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(防守效率);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(防守篮板效率);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TeamVo other = (TeamVo) obj;
		if (!Arrays.equals(teamIcon, other.teamIcon))
			return false;
		if (Double.doubleToLongBits(三分出手数) != Double
				.doubleToLongBits(other.三分出手数))
			return false;
		if (Double.doubleToLongBits(三分命中数) != Double
				.doubleToLongBits(other.三分命中数))
			return false;
		if (Double.doubleToLongBits(三分命中率) != Double
				.doubleToLongBits(other.三分命中率))
			return false;
		if (主场 == null) {
			if (other.主场 != null)
				return false;
		} else if (!主场.equals(other.主场))
			return false;
		if (分区 == null) {
			if (other.分区 != null)
				return false;
		} else if (!分区.equals(other.分区))
			return false;
		if (Double.doubleToLongBits(前场篮板数) != Double
				.doubleToLongBits(other.前场篮板数))
			return false;
		if (Double.doubleToLongBits(助攻数) != Double.doubleToLongBits(other.助攻数))
			return false;
		if (Double.doubleToLongBits(助攻率) != Double.doubleToLongBits(other.助攻率))
			return false;
		if (Double.doubleToLongBits(后场篮板数) != Double
				.doubleToLongBits(other.后场篮板数))
			return false;
		if (Double.doubleToLongBits(命中率) != Double.doubleToLongBits(other.命中率))
			return false;
		if (Double.doubleToLongBits(失误数) != Double.doubleToLongBits(other.失误数))
			return false;
		if (对手得分 != other.对手得分)
			return false;
		if (对手进攻篮板数 != other.对手进攻篮板数)
			return false;
		if (对方防守篮板数 != other.对方防守篮板数)
			return false;
		if (建立时间 == null) {
			if (other.建立时间 != null)
				return false;
		} else if (!建立时间.equals(other.建立时间))
			return false;
		if (所在地 == null) {
			if (other.所在地 != null)
				return false;
		} else if (!所在地.equals(other.所在地))
			return false;
		if (Double.doubleToLongBits(投篮命中数) != Double
				.doubleToLongBits(other.投篮命中数))
			return false;
		if (Double.doubleToLongBits(投篮数) != Double.doubleToLongBits(other.投篮数))
			return false;
		if (Double.doubleToLongBits(抢断效率) != Double
				.doubleToLongBits(other.抢断效率))
			return false;
		if (Double.doubleToLongBits(断球数) != Double.doubleToLongBits(other.断球数))
			return false;
		if (Double.doubleToLongBits(比赛信息) != Double
				.doubleToLongBits(other.比赛信息))
			return false;
		if (比赛场数 != other.比赛场数)
			return false;
		if (Double.doubleToLongBits(比赛得分) != Double
				.doubleToLongBits(other.比赛得分))
			return false;
		if (比赛日期 != other.比赛日期)
			return false;
		if (Double.doubleToLongBits(犯规数) != Double.doubleToLongBits(other.犯规数))
			return false;
		if (球队全名 == null) {
			if (other.球队全名 != null)
				return false;
		} else if (!球队全名.equals(other.球队全名))
			return false;
		if (球队简称 == null) {
			if (other.球队简称 != null)
				return false;
		} else if (!球队简称.equals(other.球队简称))
			return false;
		if (Double.doubleToLongBits(盖帽数) != Double.doubleToLongBits(other.盖帽数))
			return false;
		if (Double.doubleToLongBits(篮板数) != Double.doubleToLongBits(other.篮板数))
			return false;
		if (Double.doubleToLongBits(罚球命中数) != Double
				.doubleToLongBits(other.罚球命中数))
			return false;
		if (Double.doubleToLongBits(罚球命中率) != Double
				.doubleToLongBits(other.罚球命中率))
			return false;
		if (Double.doubleToLongBits(罚球数) != Double.doubleToLongBits(other.罚球数))
			return false;
		if (胜利场数 != other.胜利场数)
			return false;
		if (Double.doubleToLongBits(胜率) != Double.doubleToLongBits(other.胜率))
			return false;
		if (赛区 == null) {
			if (other.赛区 != null)
				return false;
		} else if (!赛区.equals(other.赛区))
			return false;
		if (赛季 == null) {
			if (other.赛季 != null)
				return false;
		} else if (!赛季.equals(other.赛季))
			return false;
		if (Double.doubleToLongBits(进攻回合) != Double
				.doubleToLongBits(other.进攻回合))
			return false;
		if (Double.doubleToLongBits(进攻效率) != Double
				.doubleToLongBits(other.进攻效率))
			return false;
		if (Double.doubleToLongBits(进攻篮板效率) != Double
				.doubleToLongBits(other.进攻篮板效率))
			return false;
		if (Double.doubleToLongBits(防守回合) != Double
				.doubleToLongBits(other.防守回合))
			return false;
		if (Double.doubleToLongBits(防守效率) != Double
				.doubleToLongBits(other.防守效率))
			return false;
		if (Double.doubleToLongBits(防守篮板效率) != Double
				.doubleToLongBits(other.防守篮板效率))
			return false;
		return true;
	}

	
}