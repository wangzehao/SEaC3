package bhwz.seac3.vo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MatchVo extends Vo{
	private static final long serialVersionUID = 4355543295579893383L;
	private long date;
	private String season;
	private String team1;
	private String team2;
	private int team1Score;
	private int team2Score;
	private List<String> sectionScores=new ArrayList<>();
	private Set<PlayerScoreTableVo> team1PlayersScore=new HashSet<>();
	private Set<PlayerScoreTableVo> team2PlayersScore=new HashSet<>();
	
	
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public long getDate() {
		return date;
	}
	public void setDate(long date) {
		this.date = date;
	}
	public String getTeam1() {
		return team1;
	}
	public void setTeam1(String team1) {
		this.team1 = team1;
	}
	public String getTeam2() {
		return team2;
	}
	public void setTeam2(String team2) {
		this.team2 = team2;
	}
	public int getTeam1Score() {
		return team1Score;
	}
	public void setTeam1Score(int team1Score) {
		this.team1Score = team1Score;
	}
	public int getTeam2Score() {
		return team2Score;
	}
	public void setTeam2Score(int team2Score) {
		this.team2Score = team2Score;
	}
	public Set<PlayerScoreTableVo> getTeam1PlayersScore() {
		return team1PlayersScore;
	}
	public void setTeam1PlayersScore(Set<PlayerScoreTableVo> team1PlayersScore) {
		this.team1PlayersScore = team1PlayersScore;
	}
	public Set<PlayerScoreTableVo> getTeam2PlayersScore() {
		return team2PlayersScore;
	}
	public void setTeam2PlayersScore(Set<PlayerScoreTableVo> team2PlayersScore) {
		this.team2PlayersScore = team2PlayersScore;
	}	
	public List<String> getSectionScores() {
		return sectionScores;
	}
	public void setSectionScores(List<String> sectionScores) {
		this.sectionScores = sectionScores;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (date ^ (date >>> 32));
		result = prime * result + ((season == null) ? 0 : season.hashCode());
		result = prime * result
				+ ((sectionScores == null) ? 0 : sectionScores.hashCode());
		result = prime * result + ((team1 == null) ? 0 : team1.hashCode());
		result = prime
				* result
				+ ((team1PlayersScore == null) ? 0 : team1PlayersScore
						.hashCode());
		result = prime * result + team1Score;
		result = prime * result + ((team2 == null) ? 0 : team2.hashCode());
		result = prime
				* result
				+ ((team2PlayersScore == null) ? 0 : team2PlayersScore
						.hashCode());
		result = prime * result + team2Score;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MatchVo other = (MatchVo) obj;
		if (date != other.date)
			return false;
		if (season == null) {
			if (other.season != null)
				return false;
		} else if (!season.equals(other.season))
			return false;
		if (sectionScores == null) {
			if (other.sectionScores != null)
				return false;
		} else if (!sectionScores.equals(other.sectionScores))
			return false;
		if (team1 == null) {
			if (other.team1 != null)
				return false;
		} else if (!team1.equals(other.team1))
			return false;
		if (team1PlayersScore == null) {
			if (other.team1PlayersScore != null)
				return false;
		} else if (!team1PlayersScore.equals(other.team1PlayersScore))
			return false;
		if (team1Score != other.team1Score)
			return false;
		if (team2 == null) {
			if (other.team2 != null)
				return false;
		} else if (!team2.equals(other.team2))
			return false;
		if (team2PlayersScore == null) {
			if (other.team2PlayersScore != null)
				return false;
		} else if (!team2PlayersScore.equals(other.team2PlayersScore))
			return false;
		if (team2Score != other.team2Score)
			return false;
		return true;
	}
	

}
