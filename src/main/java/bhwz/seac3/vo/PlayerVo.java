package bhwz.seac3.vo;

import java.util.Arrays;

public class PlayerVo extends Vo{
	private static final long serialVersionUID = 7519906270318208031L;
	//from PlayerPo
	private String name;
	private int number;
	private String position;
	private int height;
	private int weight;
	private long birth;
	private int age;
	private int exp;
	private String school;
	//from PlayerPortraitPicturePo
	private byte[] portraitPicture;
	//from PlayerActionPicturePo
	private byte[] actionPicture;
	//from PlayerGameDataPo
	private int 先发场数;
	private int 在场时间;
	private String 所属球队;
	private String 所属联盟;
	//from GameDataPo
	private String 赛季;
	private int 比赛场数;
	private double 投篮数;
	private double 罚球数;
	private double 失误数;	
	private double 断球数;
	private double 助攻数;
	private double 盖帽数;
	private double 犯规数;
	private double 比赛得分;
	private double 投篮命中数;
	private double 三分命中数;
	private double 三分出手数;
	private double 罚球命中数;
	private double 前场篮板数;
	private double 后场篮板数;
	//from DataComputer
	private double 命中率;
	private double 效率;
	private double 得分近五场提升率;
	private double 篮板近五场提升率;
	private double 助攻近五场提升率;
	private double GmSc效率值;
	private double 真实投篮命中率;
	private double 投篮效率;
	private double 篮板率;
	private double 篮板数;
	private double 罚球命中率;
	private double 三分命中率;
	private double 投篮命中率;
	private double 进攻篮板率;
	private double 防守篮板率;
	private double 助攻率;
	private double 抢断率;
	private double 盖帽率;
	private double 失误率;
	private double 使用率;
	private double 比赛信息;
	
	private long 比赛日期;
	public long get比赛日期() {
		return 比赛日期;
	}
	public void set比赛日期(long 比赛日期) {
		this.比赛日期 = 比赛日期;
	}
	public double get比赛信息() {
		return 比赛信息;
	}
	public void set比赛信息(double 比赛信息) {
		this.比赛信息 = 比赛信息;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public int getWeight() {
		return weight;
	}
	public void setWeight(int weight) {
		this.weight = weight;
	}
	public long getBirth() {
		return birth;
	}
	public void setBirth(long birth) {
		this.birth = birth;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public int getExp() {
		return exp;
	}
	public void setExp(int exp) {
		this.exp = exp;
	}
	public String getSchool() {
		return school;
	}
	public void setSchool(String school) {
		this.school = school;
	}
	public byte[] getPortraitPicture() {
		return portraitPicture;
	}
	public void setPortraitPicture(byte[] portraitPicture) {
		this.portraitPicture = portraitPicture;
	}
	public byte[] getActionPicture() {
		return actionPicture;
	}
	public void setActionPicture(byte[] actionPicture) {
		this.actionPicture = actionPicture;
	}
	public int get先发场数() {
		return 先发场数;
	}
	public void set先发场数(int 先发场数) {
		this.先发场数 = 先发场数;
	}
	public int get在场时间() {
		return 在场时间;
	}
	public void set在场时间(int 在场时间) {
		this.在场时间 = 在场时间;
	}
	public String get所属球队() {
		return 所属球队;
	}
	public void set所属球队(String 所属球队) {
		this.所属球队 = 所属球队;
	}
	public String get赛季() {
		return 赛季;
	}
	public void set赛季(String 赛季) {
		this.赛季 = 赛季;
	}
	public int get比赛场数() {
		return 比赛场数;
	}
	public void set比赛场数(int 比赛场数) {
		this.比赛场数 = 比赛场数;
	}
	public double get投篮数() {
		return 投篮数;
	}
	public void set投篮数(double 投篮数) {
		this.投篮数 = 投篮数;
	}
	public double get罚球数() {
		return 罚球数;
	}
	public void set罚球数(double 罚球数) {
		this.罚球数 = 罚球数;
	}
	public double get失误数() {
		return 失误数;
	}
	public void set失误数(double 失误数) {
		this.失误数 = 失误数;
	}
	public double get断球数() {
		return 断球数;
	}
	public void set断球数(double 断球数) {
		this.断球数 = 断球数;
	}
	public double get助攻数() {
		return 助攻数;
	}
	public void set助攻数(double 助攻数) {
		this.助攻数 = 助攻数;
	}
	public double get盖帽数() {
		return 盖帽数;
	}
	public void set盖帽数(double 盖帽数) {
		this.盖帽数 = 盖帽数;
	}
	public double get犯规数() {
		return 犯规数;
	}
	public void set犯规数(double 犯规数) {
		this.犯规数 = 犯规数;
	}
	public double get比赛得分() {
		return 比赛得分;
	}
	public void set比赛得分(double 比赛得分) {
		this.比赛得分 = 比赛得分;
	}
	public double get投篮命中数() {
		return 投篮命中数;
	}
	public void set投篮命中数(double 投篮命中数) {
		this.投篮命中数 = 投篮命中数;
	}
	public double get三分命中数() {
		return 三分命中数;
	}
	public void set三分命中数(double 三分命中数) {
		this.三分命中数 = 三分命中数;
	}
	public double get三分出手数() {
		return 三分出手数;
	}
	public void set三分出手数(double 三分出手数) {
		this.三分出手数 = 三分出手数;
	}
	public double get罚球命中数() {
		return 罚球命中数;
	}
	public void set罚球命中数(double 罚球命中数) {
		this.罚球命中数 = 罚球命中数;
	}
	public double get前场篮板数() {
		return 前场篮板数;
	}
	public void set前场篮板数(double 前场篮板数) {
		this.前场篮板数 = 前场篮板数;
	}
	public double get后场篮板数() {
		return 后场篮板数;
	}
	public void set后场篮板数(double 后场篮板数) {
		this.后场篮板数 = 后场篮板数;
	}
	public double get命中率() {
		return 命中率;
	}
	public void set命中率(double 命中率) {
		this.命中率 = 命中率;
	}
	public double get效率() {
		return 效率;
	}
	public void set效率(double 效率) {
		this.效率 = 效率;
	}
	public double get得分近五场提升率() {
		return 得分近五场提升率;
	}
	public void set得分近五场提升率(double 得分近五场提升率) {
		this.得分近五场提升率 = 得分近五场提升率;
	}
	public double get篮板近五场提升率() {
		return 篮板近五场提升率;
	}
	public void set篮板近五场提升率(double 篮板近五场提升率) {
		this.篮板近五场提升率 = 篮板近五场提升率;
	}
	public double get助攻近五场提升率() {
		return 助攻近五场提升率;
	}
	public void set助攻近五场提升率(double 助攻近五场提升率) {
		this.助攻近五场提升率 = 助攻近五场提升率;
	}
	public double getGmSc效率值() {
		return GmSc效率值;
	}
	public void setGmSc效率值(double gmSc效率值) {
		GmSc效率值 = gmSc效率值;
	}
	public double get真实投篮命中率() {
		return 真实投篮命中率;
	}
	public void set真实投篮命中率(double 真实投篮命中率) {
		this.真实投篮命中率 = 真实投篮命中率;
	}
	public double get投篮效率() {
		return 投篮效率;
	}
	public void set投篮效率(double 投篮效率) {
		this.投篮效率 = 投篮效率;
	}
	public double get篮板率() {
		return 篮板率;
	}
	public void set篮板率(double 篮板率) {
		this.篮板率 = 篮板率;
	}
	public double get篮板数() {
		return 篮板数;
	}
	public void set篮板数(double 篮板数) {
		this.篮板数 = 篮板数;
	}
	public double get罚球命中率() {
		return 罚球命中率;
	}
	public void set罚球命中率(double 罚球命中率) {
		this.罚球命中率 = 罚球命中率;
	}
	public double get三分命中率() {
		return 三分命中率;
	}
	public void set三分命中率(double 三分命中率) {
		this.三分命中率 = 三分命中率;
	}
	public double get投篮命中率() {
		return 投篮命中率;
	}
	public void set投篮命中率(double 投篮命中率) {
		this.投篮命中率 = 投篮命中率;
	}
	public double get进攻篮板率() {
		return 进攻篮板率;
	}
	public void set进攻篮板率(double 进攻篮板率) {
		this.进攻篮板率 = 进攻篮板率;
	}
	public double get防守篮板率() {
		return 防守篮板率;
	}
	public void set防守篮板率(double 防守篮板率) {
		this.防守篮板率 = 防守篮板率;
	}
	public double get助攻率() {
		return 助攻率;
	}
	public void set助攻率(double 助攻率) {
		this.助攻率 = 助攻率;
	}
	public double get抢断率() {
		return 抢断率;
	}
	public void set抢断率(double 抢断率) {
		this.抢断率 = 抢断率;
	}
	public double get盖帽率() {
		return 盖帽率;
	}
	public void set盖帽率(double 盖帽率) {
		this.盖帽率 = 盖帽率;
	}
	public double get失误率() {
		return 失误率;
	}
	public void set失误率(double 失误率) {
		this.失误率 = 失误率;
	}
	public double get使用率() {
		return 使用率;
	}
	public void set使用率(double 使用率) {
		this.使用率 = 使用率;
	}
	public String get所属联盟() {
		return 所属联盟;
	}
	public void set所属联盟(String 所属联盟) {
		this.所属联盟 = 所属联盟;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(GmSc效率值);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + Arrays.hashCode(actionPicture);
		result = prime * result + age;
		result = prime * result + (int) (birth ^ (birth >>> 32));
		result = prime * result + exp;
		result = prime * result + height;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + number;
		result = prime * result + Arrays.hashCode(portraitPicture);
		result = prime * result
				+ ((position == null) ? 0 : position.hashCode());
		result = prime * result + ((school == null) ? 0 : school.hashCode());
		result = prime * result + weight;
		temp = Double.doubleToLongBits(三分出手数);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(三分命中数);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(三分命中率);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(使用率);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + 先发场数;
		temp = Double.doubleToLongBits(前场篮板数);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(助攻数);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(助攻率);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(助攻近五场提升率);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(后场篮板数);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(命中率);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + 在场时间;
		temp = Double.doubleToLongBits(失误数);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(失误率);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(得分近五场提升率);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((所属球队 == null) ? 0 : 所属球队.hashCode());
		result = prime * result + ((所属联盟 == null) ? 0 : 所属联盟.hashCode());
		temp = Double.doubleToLongBits(投篮命中数);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(投篮命中率);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(投篮效率);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(投篮数);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(抢断率);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(效率);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(断球数);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(比赛信息);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + 比赛场数;
		temp = Double.doubleToLongBits(比赛得分);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(犯规数);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(盖帽数);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(盖帽率);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(真实投篮命中率);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(篮板数);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(篮板率);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(篮板近五场提升率);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(罚球命中数);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(罚球命中率);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(罚球数);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((赛季 == null) ? 0 : 赛季.hashCode());
		temp = Double.doubleToLongBits(进攻篮板率);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(防守篮板率);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PlayerVo other = (PlayerVo) obj;
		if (Double.doubleToLongBits(GmSc效率值) != Double
				.doubleToLongBits(other.GmSc效率值))
			return false;
		if (!Arrays.equals(actionPicture, other.actionPicture))
			return false;
		if (age != other.age)
			return false;
		if (birth != other.birth)
			return false;
		if (exp != other.exp)
			return false;
		if (height != other.height)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (number != other.number)
			return false;
		if (!Arrays.equals(portraitPicture, other.portraitPicture))
			return false;
		if (position == null) {
			if (other.position != null)
				return false;
		} else if (!position.equals(other.position))
			return false;
		if (school == null) {
			if (other.school != null)
				return false;
		} else if (!school.equals(other.school))
			return false;
		if (weight != other.weight)
			return false;
		if (Double.doubleToLongBits(三分出手数) != Double
				.doubleToLongBits(other.三分出手数))
			return false;
		if (Double.doubleToLongBits(三分命中数) != Double
				.doubleToLongBits(other.三分命中数))
			return false;
		if (Double.doubleToLongBits(三分命中率) != Double
				.doubleToLongBits(other.三分命中率))
			return false;
		if (Double.doubleToLongBits(使用率) != Double.doubleToLongBits(other.使用率))
			return false;
		if (先发场数 != other.先发场数)
			return false;
		if (Double.doubleToLongBits(前场篮板数) != Double
				.doubleToLongBits(other.前场篮板数))
			return false;
		if (Double.doubleToLongBits(助攻数) != Double.doubleToLongBits(other.助攻数))
			return false;
		if (Double.doubleToLongBits(助攻率) != Double.doubleToLongBits(other.助攻率))
			return false;
		if (Double.doubleToLongBits(助攻近五场提升率) != Double
				.doubleToLongBits(other.助攻近五场提升率))
			return false;
		if (Double.doubleToLongBits(后场篮板数) != Double
				.doubleToLongBits(other.后场篮板数))
			return false;
		if (Double.doubleToLongBits(命中率) != Double.doubleToLongBits(other.命中率))
			return false;
		if (在场时间 != other.在场时间)
			return false;
		if (Double.doubleToLongBits(失误数) != Double.doubleToLongBits(other.失误数))
			return false;
		if (Double.doubleToLongBits(失误率) != Double.doubleToLongBits(other.失误率))
			return false;
		if (Double.doubleToLongBits(得分近五场提升率) != Double
				.doubleToLongBits(other.得分近五场提升率))
			return false;
		if (所属球队 == null) {
			if (other.所属球队 != null)
				return false;
		} else if (!所属球队.equals(other.所属球队))
			return false;
		if (所属联盟 == null) {
			if (other.所属联盟 != null)
				return false;
		} else if (!所属联盟.equals(other.所属联盟))
			return false;
		if (Double.doubleToLongBits(投篮命中数) != Double
				.doubleToLongBits(other.投篮命中数))
			return false;
		if (Double.doubleToLongBits(投篮命中率) != Double
				.doubleToLongBits(other.投篮命中率))
			return false;
		if (Double.doubleToLongBits(投篮效率) != Double
				.doubleToLongBits(other.投篮效率))
			return false;
		if (Double.doubleToLongBits(投篮数) != Double.doubleToLongBits(other.投篮数))
			return false;
		if (Double.doubleToLongBits(抢断率) != Double.doubleToLongBits(other.抢断率))
			return false;
		if (Double.doubleToLongBits(效率) != Double.doubleToLongBits(other.效率))
			return false;
		if (Double.doubleToLongBits(断球数) != Double.doubleToLongBits(other.断球数))
			return false;
		if (Double.doubleToLongBits(比赛信息) != Double
				.doubleToLongBits(other.比赛信息))
			return false;
		if (比赛场数 != other.比赛场数)
			return false;
		if (Double.doubleToLongBits(比赛得分) != Double
				.doubleToLongBits(other.比赛得分))
			return false;
		if (Double.doubleToLongBits(犯规数) != Double.doubleToLongBits(other.犯规数))
			return false;
		if (Double.doubleToLongBits(盖帽数) != Double.doubleToLongBits(other.盖帽数))
			return false;
		if (Double.doubleToLongBits(盖帽率) != Double.doubleToLongBits(other.盖帽率))
			return false;
		if (Double.doubleToLongBits(真实投篮命中率) != Double
				.doubleToLongBits(other.真实投篮命中率))
			return false;
		if (Double.doubleToLongBits(篮板数) != Double.doubleToLongBits(other.篮板数))
			return false;
		if (Double.doubleToLongBits(篮板率) != Double.doubleToLongBits(other.篮板率))
			return false;
		if (Double.doubleToLongBits(篮板近五场提升率) != Double
				.doubleToLongBits(other.篮板近五场提升率))
			return false;
		if (Double.doubleToLongBits(罚球命中数) != Double
				.doubleToLongBits(other.罚球命中数))
			return false;
		if (Double.doubleToLongBits(罚球命中率) != Double
				.doubleToLongBits(other.罚球命中率))
			return false;
		if (Double.doubleToLongBits(罚球数) != Double.doubleToLongBits(other.罚球数))
			return false;
		if (赛季 == null) {
			if (other.赛季 != null)
				return false;
		} else if (!赛季.equals(other.赛季))
			return false;
		if (Double.doubleToLongBits(进攻篮板率) != Double
				.doubleToLongBits(other.进攻篮板率))
			return false;
		if (Double.doubleToLongBits(防守篮板率) != Double
				.doubleToLongBits(other.防守篮板率))
			return false;
		return true;
	}
	
}
