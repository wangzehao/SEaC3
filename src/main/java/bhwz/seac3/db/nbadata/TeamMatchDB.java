package bhwz.seac3.db.nbadata;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.*;

import bhwz.seac3.db.Database;
import bhwz.seac3.dbservice.nbadata.TeamMatchDBService;
import bhwz.seac3.po.MatchPo;

public class TeamMatchDB implements TeamMatchDBService{

	@SuppressWarnings("unchecked")
	@Override
	public List<MatchPo> getTeamMatchList(String teamName, String season)
			throws Exception {
		List<MatchPo> result=new ArrayList<>();
		Database.executeTransaction(s->{
			Query q;
			if(season!=null){
			q=s.createQuery("from MatchPo match where (match.team1=? or match.team2=?) and match.season=?");
				q.setString(0, teamName);
				q.setString(1, teamName);
				q.setString(2, season);
			}else{
				q=s.createQuery("from MatchPo match where match.team1=? or match.team2=?");
				q.setString(0, teamName);
				q.setString(1, teamName);
			}
			q.list().forEach(o->{
				result.add((MatchPo) o);
			});
		});
		return result;
	}

}
