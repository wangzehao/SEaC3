package bhwz.seac3.db.nbadata;

import java.util.ArrayList;
import java.util.List;



import org.hibernate.Query;

import bhwz.seac3.db.Database;
import bhwz.seac3.dbservice.nbadata.HotSpotDBService;
import bhwz.seac3.po.MatchPo;

public class HotSpotDB implements HotSpotDBService{

	@SuppressWarnings("unchecked")
	@Override
	public List<MatchPo> query当天比赛() throws Exception{
		List<MatchPo> todayMatches=new ArrayList<>();
		Database.executeTransaction(s->{
			Query q=s.createQuery("select max(po.date) from MatchPo po");
			
			Object today_=q.list().get(0);
			if(today_==null)
				return;
			long today=(long)today_;
			Query q2=s.createQuery("from MatchPo po where po.date>?");
			q2.setLong(0, today-24*60*60*1000+1);
			q2.list().forEach(o->{
				todayMatches.add((MatchPo) o);
			});
		});
		return todayMatches;
	}

}
