package bhwz.seac3.db.nbadata;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import bhwz.seac3.po.SimpleSaveQuery;

public class PlayerMatchesEntity implements SimpleSaveQuery{
	private String playerId;
	private List<String> matchesId=new ArrayList<>();
	
	public String getMatchesIdEntity() {
		StringBuilder result=new StringBuilder(100*25);
		for(int i=0;i<matchesId.size();i++){
			result.append(matchesId.get(i)+":");
		}
		String r= result.toString();
		return r;
	}
	public void setMatchesIdEntity(String s){
		if(s.endsWith(":")){
			s=s.substring(0, s.length()-1);
			if(!s.contains(":")){
				matchesId=Arrays.asList(s);
			}
		}
		matchesId=Arrays.asList(s.split(":"));
	}
	
	public String getPlayerId() {
		return playerId;
	}
	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}
	public List<String> getMatchesId() {
		return matchesId;
	}
	public void setMatchesId(List<String> matchesId) {
		this.matchesId = matchesId;
	}
	@Override
	public Serializable getId() {
		return playerId;
	}
}
