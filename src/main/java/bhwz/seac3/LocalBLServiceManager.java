package bhwz.seac3;


import bhwz.seac3.bl.RedisplayControllerBL;
import bhwz.seac3.bl.datacomputer.DataComputer;
import bhwz.seac3.bl.import_.ImportBL;
import bhwz.seac3.blservice.ReDisplayControllerBLService;
import bhwz.seac3.blservice.datacomputer.MatchDataAppendBLService;
import bhwz.seac3.blservice.datacomputer.PerGamePlayerDataComputeBLService;
import bhwz.seac3.blservice.datacomputer.PerGameTeamDataComputeBLService;
import bhwz.seac3.blservice.import_.ImportBLService;

public class LocalBLServiceManager {
	public static ImportBLService getImportBLService(){
		return new ImportBL(getRedisplayControllerBLService());
	}
	
	public static MatchDataAppendBLService getMatchDataAppendBLService(){
		return new DataComputer();
	}
	
	public static PerGamePlayerDataComputeBLService getPerGamePlayerDataComputer(){
		return new DataComputer();
	}
	
	public static PerGameTeamDataComputeBLService getGameTeamDataComputer(){
		return new DataComputer();
	}

	static ReDisplayControllerBLService rcbls=new RedisplayControllerBL();
	public static ReDisplayControllerBLService getRedisplayControllerBLService(){
		return rcbls;
	}
}
