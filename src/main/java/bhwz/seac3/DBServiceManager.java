package bhwz.seac3;


import bhwz.seac3.db.nbadata.*;
import bhwz.seac3.dbservice.nbadata.*;
import bhwz.seac3.po.*;

public class DBServiceManager {
	
	public static SaveQueryDBService<PlayerPo> getPlayerDBService(){
		return new SaveQueryDB<>();
	}
	
	public static SaveQueryDBService<MatchPo> getMatchDBService(){
		return new SaveQueryDB<>();
	}
	
	public static SaveQueryDBService<TeamPo> getTeamDBService(){
		return new SaveQueryDB<>();
	}
	
	public static SaveQueryDBService<PlayerGameDataPo> getPlayerGameDataDBService(){
		return new SaveQueryDB<>();
	}
	
	public static SaveQueryDBService<TeamGameDataPo> getTeamGameDataDBService(){
		return new SaveQueryDB<>();
	}
	
	public static SaveQueryDBService<PlayerActionPicturePo> getPlayerActionPictureDBService(){
		return new SaveQueryDB<>();
	}
	
	public static SaveQueryDBService<PlayerPortraitPicturePo> getPlayerPortraitPictureDBService(){
		return new SaveQueryDB<>();
	}
	
	public static SaveQueryDBService<PlayerCaculationDataPo> getPlayerCaculationDataDBService(){
		return new SaveQueryDB<>();
	}
	
	public static SaveQueryDBService<TeamIconPo> getTeamIconDBService(){
		return new SaveQueryDB<>();
	}
	
	public static PlayerMatchDBService getPlayerMatchDBService(){
		return new PlayerMatchDB(new SaveQueryDB<>(),getMatchDBService());
	}
	
	public static TeamMatchDBService geTeamMatchDBService(){
		return new TeamMatchDB();
	}
	
	public static HotSpotDBService getHotSpotDBService(){
		return new HotSpotDB();
	}
	
	public static SeasonDBService getSeasonDBService(){
		return new SeasonDB();
	}
}
