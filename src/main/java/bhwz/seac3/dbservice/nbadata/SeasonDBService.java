package bhwz.seac3.dbservice.nbadata;

import java.util.List;

import bhwz.seac3.po.PlayerPo;
import bhwz.seac3.po.TeamPo;

public interface SeasonDBService {
	/**
	 * 所有返回的Season按照时间降序排列，最近赛季的index为0
	 */
	List<String> queryAllSeasons() throws Exception;
	/**
	 * 所有返回的Season按照时间降序排列，最近赛季的index为0
	 */
	List<String> queryJoinedSeasons(PlayerPo player) throws Exception;
	/**
	 * 所有返回的Season按照时间降序排列，最近赛季的index为0
	 */
	List<String> queryJoinedSeasons(TeamPo team) throws Exception;
}
