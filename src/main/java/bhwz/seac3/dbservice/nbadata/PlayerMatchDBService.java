package bhwz.seac3.dbservice.nbadata;

import java.util.List;

import bhwz.seac3.po.MatchPo;
import bhwz.seac3.po.PlayerPo;

public interface PlayerMatchDBService {
	/**
	 * if(season==null) return all matches
	 */
	public List<MatchPo> getPlayerMatchList(String playerName, String season)
			throws Exception;

	public default List<MatchPo> getPlayerLatestFiveMatches(String playerName,
			String season) throws Exception {
		List<MatchPo> result = getPlayerMatchList(playerName, season);
		if(result==null)
			return null;
		result.sort((MatchPo o1, MatchPo o2) -> {
			if (o1.getDate() < o2.getDate())
				return 1;
			else if (o1.getDate() > o2.getDate())
				return -1;
			else
				return 0;
		});
		if(result.size()>5)
			return result.subList(0, 5);
		else
			return result;
	}

	public void addMatchToPlayer(MatchPo match, PlayerPo player)
			throws Exception;
}
