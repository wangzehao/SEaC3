package bhwz.seac3.dbservice.nbadata;

import java.util.List;

import bhwz.seac3.po.MatchPo;

public interface TeamMatchDBService {
	public List<MatchPo> getTeamMatchList(String teamName,String season) throws Exception;
}
