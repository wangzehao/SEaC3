package bhwz.seac3.dbservice.nbadata;

import java.util.List;

import bhwz.seac3.po.SimpleSaveQuery;

public interface SaveQueryDBService<SSQ extends SimpleSaveQuery> {
	public void save(SSQ po) throws Exception;
	public void updateById(SSQ po) throws Exception;
	public void deleteById(SSQ po) throws Exception;
	public SSQ queryById(SSQ id) throws Exception;
	public List<SSQ> queryAll(@SuppressWarnings("rawtypes") Class c) throws Exception;
}
