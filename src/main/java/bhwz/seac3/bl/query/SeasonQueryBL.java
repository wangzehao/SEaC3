package bhwz.seac3.bl.query;

import java.util.List;

import bhwz.seac3.blservice.query.SeasonQueryBLService;
import bhwz.seac3.dbservice.nbadata.SeasonDBService;
import bhwz.seac3.po.PlayerPo;
import bhwz.seac3.po.TeamPo;
import bhwz.seac3.vo.PlayerVo;
import bhwz.seac3.vo.TeamVo;

public class SeasonQueryBL implements SeasonQueryBLService{
	SeasonDBService db;
	
	public SeasonQueryBL(SeasonDBService db) {
		this.db=db;
	}

	@Override
	public List<String> queryAllSeasons() throws Exception {
		return db.queryAllSeasons();
	}

	@Override
	public List<String> queryJoinedSeasons(PlayerVo player) throws Exception {
		PlayerPo qid=new PlayerPo();
		qid.setName(player.getName());
		return db.queryJoinedSeasons(qid);
	}

	@Override
	public List<String> queryJoinedSeasons(TeamVo team) throws Exception {
		TeamPo qid=new TeamPo();
		qid.set缩写(team.get球队简称());
		return db.queryJoinedSeasons(qid);
	}

}
