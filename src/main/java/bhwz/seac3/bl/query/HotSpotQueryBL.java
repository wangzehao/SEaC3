package bhwz.seac3.bl.query;

import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import bhwz.seac3.DBServiceManager;
import bhwz.seac3.bl.datacomputer.DataComputer;
import bhwz.seac3.blservice.datacomputer.PerGamePlayerDataComputeBLService;
import bhwz.seac3.blservice.datacomputer.PerGameTeamDataComputeBLService;
import bhwz.seac3.blservice.query.HotspotQueryBLService;
import bhwz.seac3.blservice.query.PlayerQueryBLService;
import bhwz.seac3.blservice.query.TeamQueryBLService;
import bhwz.seac3.dbservice.nbadata.HotSpotDBService;
import bhwz.seac3.dbservice.nbadata.SaveQueryDBService;
import bhwz.seac3.dbservice.nbadata.SeasonDBService;
import bhwz.seac3.po.MatchPo;
import bhwz.seac3.po.PlayerActionPicturePo;
import bhwz.seac3.po.PlayerCaculationDataPo;
import bhwz.seac3.po.PlayerGameDataPo;
import bhwz.seac3.po.PlayerPo;
import bhwz.seac3.po.PlayerPortraitPicturePo;
import bhwz.seac3.po.PlayerScoreTablePo;
import bhwz.seac3.po.TeamGameDataPo;
import bhwz.seac3.po.TeamIconPo;
import bhwz.seac3.po.TeamPo;
import bhwz.seac3.ui.user.SeasonSelectComboBox;
import bhwz.seac3.util.ComparableData;
import bhwz.seac3.util.MostK;
import bhwz.seac3.util.PictureManager;
import bhwz.seac3.vo.PlayerVo;
import bhwz.seac3.vo.TeamVo;

public class HotSpotQueryBL extends UnicastRemoteObject implements HotspotQueryBLService {

	private static final long serialVersionUID = 1L;
	private HotSpotDBService hsdb;
	private SaveQueryDBService<PlayerPo> pdb;
	private SaveQueryDBService<PlayerGameDataPo> pgddb;
	private SaveQueryDBService<PlayerPortraitPicturePo> pppdb;
	private SaveQueryDBService<TeamGameDataPo> tgddb;
	private SaveQueryDBService<TeamIconPo> tidb;
	private SaveQueryDBService<TeamPo> tdb;
	private SaveQueryDBService<PlayerCaculationDataPo> pcdb;
	private PerGamePlayerDataComputeBLService cbl;
	private PerGameTeamDataComputeBLService cbl2;
	private PlayerQueryBLService pqbl;
	private SaveQueryDBService<PlayerActionPicturePo> apdb;
	private TeamQueryBLService tqbl;
	
	public HotSpotQueryBL() throws Exception{
		hsdb=DBServiceManager.getHotSpotDBService();
		pdb=DBServiceManager.getPlayerDBService();
		pgddb=DBServiceManager.getPlayerGameDataDBService();
		pppdb=DBServiceManager.getPlayerPortraitPictureDBService();
		tgddb=DBServiceManager.getTeamGameDataDBService();
		tidb=DBServiceManager.getTeamIconDBService();
		tdb=DBServiceManager.getTeamDBService();
		pcdb=DBServiceManager.getPlayerCaculationDataDBService();
		cbl=new DataComputer();
		apdb=DBServiceManager.getPlayerActionPictureDBService();
		pqbl=new PlayerQueryBL(cbl,pdb,pgddb,apdb,pppdb,pcdb);
		tqbl=new TeamQueryBL(cbl2,tdb,tgddb,tidb);
	}

	@Override
	public List<PlayerVo> QueryPlayersRankedBy条件(String 筛选条件) throws Exception{
		// TODO Auto-generated method stub
		List<MatchPo> matches=null;
		String season=null;
		Set<PlayerScoreTablePo> team1=null;
		Set<PlayerScoreTablePo> team2=null;
		List<PlayerScoreTablePo> datas=null;
		try {
			matches=hsdb.query当天比赛();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    if(matches==null||matches.size()==0){
	    	return null;
	    }
	    Iterator<MatchPo> itor=matches.iterator();
	    datas=new ArrayList<PlayerScoreTablePo>();
	    Iterator<PlayerScoreTablePo> itor2;
	    while(itor.hasNext()){
	    	MatchPo temp=itor.next();
	    	team1=temp.getTeam1PlayersScore();
	    	team2=temp.getTeam2PlayersScore();
	    	itor2=team1.iterator();
	    	while(itor2.hasNext()){
	    		datas.add(itor2.next());
	    	}
	    	itor2=team2.iterator();
	    	while(itor2.hasNext()){
	    		datas.add(itor2.next());
	    	}
	    	SeasonDBService sdb=DBServiceManager.getSeasonDBService();
	    	season=sdb.queryAllSeasons().get(0);
	    }
	    
	    
		switch(筛选条件){
		case "得分":return queryPlayerBy得分(datas,season);
		case "篮板":return queryPlayerBy篮板(datas,season);
		case "助攻":return queryPlayerBy助攻(datas,season);
		case "盖帽":return queryPlayerBy盖帽(datas,season);
		case "抢断":return queryPlayerBy抢断(datas,season);
		case "场均得分":return queryPlayerBy场均得分(season);
		case "场均篮板":return queryPlayerBy场均篮板(season);
		case "场均助攻":return queryPlayerBy场均助攻(season);
		case "场均盖帽":return queryPlayerBy场均盖帽(season);
		case "场均抢断":return queryPlayerBy场均抢断(season);
		case "三分%":return queryPlayerBy三分命中率(season);
		case "投篮%":return queryPlayerBy投篮命中率(season);
		case "罚球%":return queryPlayerBy罚球命中率(season);
		case "场均得分进步":return queryPlayerBy场均得分进步(season);
		case "场均篮板进步":return queryPlayerBy场均篮板进步(season);
		case "场均助攻进步":return queryPlayerBy场均助攻进步(season);
        default:return null;		
		}
	}
	
	@Override
	public List<TeamVo> QueryTeamsRankedBy条件(String 筛选条件)  throws Exception{
		// TODO Auto-generated method stub
		List<MatchPo> matches=null;
		String season=new SeasonSelectComboBox().getSeason();
		try {
			matches=hsdb.query当天比赛();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    if(matches==null||matches.size()==0){
	    	return null;
	    }
	    Iterator<MatchPo> itor=matches.iterator();
	    if(itor.hasNext()){
	    	season=itor.next().getSeason();
	    }
		switch(筛选条件){
		case "场均得分":return queryTeamBy场均得分(season);
		case "场均篮板":return queryTeamBy场均篮板(season);
		case "场均助攻":return queryTeamBy场均助攻(season);
		case "场均盖帽":return queryTeamBy场均盖帽(season);
		case "场均抢断":return queryTeamBy场均抢断(season);
		case "三分%":return queryTeamBy三分命中率(season);
		case "投篮%":return queryTeamBy投篮命中率(season);
		case "罚球%":return queryTeamBy罚球命中率(season);
		case "犯规":return queryTeamBy犯规(season);
		case "失误":return queryTeamBy失误(season);
		case "防守篮板数":return queryTeamBy防守篮板数(season);
		case "进攻篮板数":return queryTeamBy进攻篮板数(season);
        default:return null;		
		}
	}
	
	enum SeasonData{
	    场均得分,
	    场均篮板,
	    场均助攻,
	    场均盖帽,
	    场均抢断,
	    三分命中率,
	    投篮命中率,
	    罚球命中率,
	    场均得分进步,
	    场均篮板进步,
	    场均助攻进步
	}
	
	private List<PlayerVo> queryPlayerBy赛季数据(String season,SeasonData 赛季数据){
		List<PlayerVo> list=null;
		try {
			list=pqbl.query场均数据(season);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(list==null||list.size()==0){
			return null;
		}
        List<ComparableData> datas=new ArrayList<ComparableData>();
        Iterator<PlayerVo> itor=list.iterator();
        while(itor.hasNext()){
            ComparableData player=new ComparableData();
            PlayerVo temp=null;
        	temp=itor.next();
        	int 比赛场数=temp.get比赛场数();
        	switch(赛季数据){
        	case 场均得分:
        		if(比赛场数==0){
        		   player.setData(0);	
        		}else{
        		   player.setData(temp.get比赛得分()*1.0/比赛场数);
        		}
        		break;
        	case 场均篮板:
        	    if(比赛场数==0){
     		       player.setData(0);	
     		    }else{
     		       player.setData(temp.get篮板数()*1.0/比赛场数);
     		    }
     		    break;
        	case 场均助攻:
        		if(比赛场数==0){
     		       player.setData(0);	
     		    }else{
     		       player.setData(temp.get助攻数()*1.0/比赛场数);
     		    }
     		    break;
        	case 场均盖帽:
        		if(比赛场数==0){
     		       player.setData(0);	
     		    }else{
     		       player.setData(temp.get盖帽数()*1.0/比赛场数);
     		    }
     		    break;
        	case 场均抢断:
        		if(比赛场数==0){
     		       player.setData(0);	
     		    }else{
     		       player.setData(temp.get断球数()*1.0/比赛场数);
     		    }
     		    break;
        	case 三分命中率:player.setData(temp.get三分命中率());break;
        	case 投篮命中率:player.setData(temp.get投篮命中率());break;
     	    case 罚球命中率:player.setData(temp.get罚球命中率());break;
        	case 场均得分进步:player.setData(temp.get得分近五场提升率());break;
        	case 场均篮板进步:player.setData(temp.get篮板近五场提升率());break;
        	case 场均助攻进步:player.setData(temp.get助攻近五场提升率());break;
        	}
        	player.setName(temp.getName());
        	player.setSeason(season);
        	datas.add(player);
        }
        MostK m=new MostK(5,datas);
        datas=m.getMostK();
        Iterator<ComparableData> itor2=datas.iterator();
		List<PlayerVo> hot=new ArrayList<PlayerVo>();
		PlayerVo p_vo=null;
		PlayerGameDataPo pgd_po=null;
		while(itor2.hasNext()){
			ComparableData player=null;
			player=itor2.next();
			p_vo=new PlayerVo();
			p_vo.setName(player.getName());
			p_vo.set比赛信息(player.getData());
			PlayerPo p_po=new PlayerPo();
			p_po.setName(player.getName());
			PlayerPortraitPicturePo ppp_po=new PlayerPortraitPicturePo();
			ppp_po.setKey(player.getName());
			pgd_po=new PlayerGameDataPo();
			pgd_po.setId(player.getName()+";"+player.getSeason());
			try {
				if(pdb.queryById(p_po)==null){
					p_vo.setPosition(null);
				}else{
				    p_vo.setPosition(pdb.queryById(p_po).getPosition());
				}
				if(pppdb.queryById(ppp_po)==null){
					PictureManager pm=new PictureManager("png");
                    p_vo.setPortraitPicture(pm.encodeImage(getClass().getResource("/picture/black.png").toURI().getPath()));
					//p_vo.setPortraitPicture(null);
				}else{
					p_vo.setPortraitPicture(pppdb.queryById(ppp_po).getPicture());
				}
				if(pgddb.queryById(pgd_po)==null){
					p_vo.set所属球队(null);
				}else{
                    p_vo.set所属球队(pgddb.queryById(pgd_po).get所属球队());
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
			hot.add(p_vo);
		}
		return hot;
	}
	
	private List<PlayerVo> queryPlayerBy场均得分进步(String season){
		return queryPlayerBy赛季数据(season,SeasonData.场均得分进步);
	}
	
	private List<PlayerVo> queryPlayerBy场均篮板进步(String season){
		return queryPlayerBy赛季数据(season,SeasonData.场均篮板进步);
	}
	
	private List<PlayerVo> queryPlayerBy场均助攻进步(String season){
		return queryPlayerBy赛季数据(season,SeasonData.场均助攻进步);
	}
	
	enum TodayData{
	    得分,
	    篮板,
	    助攻,
	    盖帽,
	    抢断
	}
	
	private List<PlayerVo> queryPlayerBy今日表现(List<PlayerScoreTablePo> list,String season,TodayData todayData){
		if(list==null||list.size()==0){
			return null;
		}
		
		List<ComparableData> datas=new ArrayList<ComparableData>();
		Iterator<PlayerScoreTablePo> itor=list.iterator();
		PlayerScoreTablePo temp=null;

		while(itor.hasNext()){
			temp=itor.next();
			ComparableData data=null;
			data=new ComparableData();
			data.setName(temp.get球员名());
			data.setSeason(season);
            switch(todayData){
            case 得分:data.setData(temp.get个人得分());break;
            case 篮板:data.setData(temp.get总篮板数());break;
            case 助攻:data.setData(temp.get助攻数());break;
            case 盖帽:data.setData(temp.get盖帽数());break;
            case 抢断:data.setData(temp.get抢断数());break;
            }
			datas.add(data);
		}
		
		MostK mk=new MostK(5,datas);
		datas=mk.getMostK();
		Iterator<ComparableData> itor2=datas.iterator();
		List<PlayerVo> hot=new ArrayList<PlayerVo>();
		PlayerVo p_vo=null;
		while(itor2.hasNext()){
			ComparableData data=new ComparableData();
			data=itor2.next();
			p_vo=new PlayerVo();
			p_vo.setName(data.getName());
			p_vo.set比赛信息(data.getData());
			PlayerPo p_po=new PlayerPo();
			p_po.setName(data.getName());
			PlayerPortraitPicturePo ppp_po=new PlayerPortraitPicturePo();
			ppp_po.setKey(data.getName());
			PlayerGameDataPo pgd_po=new PlayerGameDataPo();
			pgd_po.setId(data.getName()+";"+data.getSeason());
			try {
				if(pdb.queryById(p_po)==null){
					p_vo.setPosition(null);
				}else{
				    p_vo.setPosition(pdb.queryById(p_po).getPosition());
				}
				if(pppdb.queryById(ppp_po)==null){
					PictureManager pm=new PictureManager("png");
                    p_vo.setPortraitPicture(pm.encodeImage(getClass().getResource("/picture/black.png").toURI().getPath()));
					//p_vo.setPortraitPicture(null);
				}else{
					p_vo.setPortraitPicture(pppdb.queryById(ppp_po).getPicture());
				}
				if(pgddb.queryById(pgd_po)==null){
					p_vo.set所属球队(null);
				}else{
                    p_vo.set所属球队(pgddb.queryById(pgd_po).get所属球队());
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			hot.add(p_vo);
		}
		return hot;
	}

	private List<PlayerVo> queryPlayerBy得分(List<PlayerScoreTablePo> list,String season){
		return queryPlayerBy今日表现(list,season,TodayData.得分);
	} 
	
	private List<PlayerVo> queryPlayerBy篮板(List<PlayerScoreTablePo> list,String season){
		return queryPlayerBy今日表现(list,season,TodayData.篮板);
	} 
	
	private List<PlayerVo> queryPlayerBy助攻(List<PlayerScoreTablePo> list,String season){
		return queryPlayerBy今日表现(list,season,TodayData.助攻);
	} 
	
	private List<PlayerVo> queryPlayerBy盖帽(List<PlayerScoreTablePo> list,String season){
		return queryPlayerBy今日表现(list,season,TodayData.盖帽);
	} 
	
	private List<PlayerVo> queryPlayerBy抢断(List<PlayerScoreTablePo> list,String season){
		return queryPlayerBy今日表现(list,season,TodayData.抢断);
	} 
	
	private List<PlayerVo> queryPlayerBy场均得分(String season){
		return queryPlayerBy赛季数据(season,SeasonData.场均得分);
	}
	
	private List<PlayerVo> queryPlayerBy场均篮板(String season){
		return queryPlayerBy赛季数据(season,SeasonData.场均篮板);
	}

	private List<PlayerVo> queryPlayerBy场均助攻(String season){
		return queryPlayerBy赛季数据(season,SeasonData.场均助攻);
	}
	
	private List<PlayerVo> queryPlayerBy场均盖帽(String season){
		return queryPlayerBy赛季数据(season,SeasonData.场均盖帽);
	}
	
	private List<PlayerVo> queryPlayerBy场均抢断(String season){
		return queryPlayerBy赛季数据(season,SeasonData.场均抢断);
	}
	
	private List<PlayerVo> queryPlayerBy三分命中率(String season){
		return queryPlayerBy赛季数据(season,SeasonData.三分命中率);
	}
	
	private List<PlayerVo> queryPlayerBy投篮命中率(String season){
		return queryPlayerBy赛季数据(season,SeasonData.投篮命中率);
	}
	
	private List<PlayerVo> queryPlayerBy罚球命中率(String season){
		return queryPlayerBy赛季数据(season,SeasonData.罚球命中率);
	}
	
	//场均得分，场均篮板，场均助攻，场均盖帽，场均抢断,三分投篮罚球命中率
	
	enum TeamData{
	 场均得分,
	 场均篮板,
	 场均助攻,
	 场均盖帽,
	 场均抢断,
	 三分命中率,
	 投篮命中率,
	 罚球命中率,
	 犯规,
	 失误,
	 防守篮板数,
	 进攻篮板数,
	}
	
	private List<TeamVo> queryTeamBy赛季数据(String season,TeamData teamData){
		List<TeamVo> list=null;
		List<ComparableData> datas=null;

		
	    try {
			list=tqbl.query场均数据(season);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        if(list==null||list.size()==0){
        	return null;
        }
        
        datas=new ArrayList<ComparableData>();
        TeamVo tvo=null;
        
        Iterator<TeamVo> itor=list.iterator();
        while(itor.hasNext()){
    		  ComparableData temp=null;
        	  temp=new ComparableData();
              tvo=itor.next();
              temp.setName(tvo.get球队简称());
              temp.setSeason(season);
              switch(teamData){
              case 场均得分:temp.setData(tvo.get比赛得分());break;
              case 场均篮板:temp.setData(tvo.get篮板数());break;
              case 场均助攻:temp.setData(tvo.get助攻数());break;
              case 场均盖帽:temp.setData(tvo.get盖帽数());break;
              case 场均抢断:temp.setData(tvo.get断球数());break;
              case 三分命中率:temp.setData(tvo.get三分命中率());break;
              case 投篮命中率:temp.setData(tvo.get命中率());break;
              case 罚球命中率:temp.setData(tvo.get罚球命中率());break;
              case 犯规:temp.setData(tvo.get犯规数());break;
              case 失误:temp.setData(tvo.get失误数());break;
              case 防守篮板数:temp.setData(tvo.get后场篮板数());break;
              case 进攻篮板数:temp.setData(tvo.get前场篮板数());break;
              }
              datas.add(temp);
        }
        
        MostK m=new MostK(5,datas);
        datas=m.getMostK();
        Iterator<ComparableData> itor2=datas.iterator();
		List<TeamVo> hot=new ArrayList<TeamVo>();
		TeamVo t_vo=null;
		while(itor2.hasNext()){
			ComparableData temp=null;
			temp=itor2.next();
			t_vo=new TeamVo();
			t_vo.set球队简称(temp.getName());
			t_vo.set比赛信息(temp.getData());
			TeamPo t_po=new TeamPo();
			t_po.set缩写(temp.getName());
			TeamIconPo ti_po=new TeamIconPo();
			ti_po.setKey(temp.getName());
			//tgd_po=new TeamGameDataPo();
			//tgd_po.setId(temp.getName()+";"+temp.getSeason());
			try {
                t_vo.setTeamIcon((tidb.queryById(ti_po).getPicture()));
                t_vo.set球队全名(tdb.queryById(t_po).get球队全名());
                t_vo.set分区(tdb.queryById(t_po).get分区());
                t_vo.set赛区(tdb.queryById(t_po).get赛区());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			hot.add(t_vo);
		}
		return hot;
	}
	
	private List<TeamVo> queryTeamBy罚球命中率(String season){
		return queryTeamBy赛季数据(season,TeamData.罚球命中率);
	}
	
	private List<TeamVo> queryTeamBy三分命中率(String season){
		return queryTeamBy赛季数据(season,TeamData.三分命中率);
	}
	
	private List<TeamVo> queryTeamBy投篮命中率(String season){
		return queryTeamBy赛季数据(season,TeamData.投篮命中率);
	}
	
	private List<TeamVo> queryTeamBy场均得分(String season){
		return queryTeamBy赛季数据(season,TeamData.场均得分);
	}
	
	private List<TeamVo> queryTeamBy场均助攻(String season){
		return queryTeamBy赛季数据(season,TeamData.场均助攻);
	}
	
	private List<TeamVo> queryTeamBy场均盖帽(String season){
		return queryTeamBy赛季数据(season,TeamData.场均盖帽);
	}
	
	private List<TeamVo> queryTeamBy场均篮板(String season){
		return queryTeamBy赛季数据(season,TeamData.场均篮板);
	}
	
	private List<TeamVo> queryTeamBy场均抢断(String season){
		return queryTeamBy赛季数据(season,TeamData.场均抢断);
	}
	
	private List<TeamVo> queryTeamBy失误(String season){
		return queryTeamBy赛季数据(season,TeamData.失误);
	}
	
	private List<TeamVo> queryTeamBy犯规(String season){
		return queryTeamBy赛季数据(season,TeamData.犯规);
	}
	
	
	private List<TeamVo> queryTeamBy防守篮板数(String season){
		return queryTeamBy赛季数据(season,TeamData.防守篮板数);
	}
	
	
	private List<TeamVo> queryTeamBy进攻篮板数(String season){
		return queryTeamBy赛季数据(season,TeamData.进攻篮板数);
	}
	
	
}
