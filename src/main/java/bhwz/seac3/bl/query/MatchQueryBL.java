package bhwz.seac3.bl.query;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import bhwz.seac3.blservice.query.MatchQueryBLService;
import bhwz.seac3.dbservice.nbadata.HotSpotDBService;
import bhwz.seac3.dbservice.nbadata.PlayerMatchDBService;
import bhwz.seac3.dbservice.nbadata.SaveQueryDBService;
import bhwz.seac3.dbservice.nbadata.TeamMatchDBService;
import bhwz.seac3.po.MatchPo;
import bhwz.seac3.po.PlayerScoreTablePo;
import bhwz.seac3.vo.MatchVo;
import bhwz.seac3.vo.PlayerScoreTableVo;

public class MatchQueryBL extends UnicastRemoteObject implements MatchQueryBLService{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private SaveQueryDBService<MatchPo> sqdb;
	private PlayerMatchDBService pm;
	private TeamMatchDBService pmd;
	private HotSpotDBService hdb;
	public MatchQueryBL(SaveQueryDBService<MatchPo> sqdb,PlayerMatchDBService pm,TeamMatchDBService pmd,HotSpotDBService hdb) throws RemoteException {
		super();
		this.sqdb=sqdb;
		this.pm=pm;
		this.pmd=pmd;
		this.hdb=hdb;
	}

	@Override
	public List<MatchVo> queryAllMatches(String season) throws Exception {
		List<MatchPo> pos=sqdb.queryAll(MatchPo.class);
		List<MatchVo> result=new LinkedList<MatchVo>();
		result=pos.parallelStream().filter(s->s.getSeason().equals(season)).map(s->_vo(s)).collect(Collectors.toList());
		return result;
	}

	@Override
	public List<MatchVo> queryByTeam(String team, String season) throws Exception {
		List<MatchPo> pos=pmd.getTeamMatchList(team, season);
		List<MatchVo> result=new LinkedList<MatchVo>();
		pos.forEach(s->{
			result.add(_vo(s));
		});
		return result;
	}

	@Override
	public List<MatchVo> queryByPlayer(String player, String season) throws Exception {
		List<MatchPo> pos=pm.getPlayerMatchList(player, season);
		List<MatchVo> result=new LinkedList<MatchVo>();
		pos.forEach(s->{
			result.add(_vo(s));
		});
		return result;
	}
	
	
	private MatchVo _vo(MatchPo po){
		MatchVo vo=new MatchVo();
		if(po!=null){
		vo.setDate(po.getDate());
		vo.setSeason(po.getSeason());
		vo.setSectionScores(po.getSectionScores());
		vo.setTeam1(po.getTeam1());
	    vo.setTeam1Score(po.getTeam1Score());
	    vo.setTeam2(po.getTeam2());
	    vo.setTeam2Score(po.getTeam2Score());
	    Set<PlayerScoreTablePo> team1Po=po.getTeam1PlayersScore();
	    Set<PlayerScoreTableVo> team1Vo=new HashSet<>();
	    for(PlayerScoreTablePo p:team1Po ){
	    	PlayerScoreTableVo v=new PlayerScoreTableVo();
	    	v.set三分出手数(p.get三分出手数());
	    	v.set三分命中数(p.get三分命中数());
	    	v.set个人得分(p.get个人得分());
	    	v.set位置(p.get位置());
	    	v.set助攻数(p.get助攻数());
	    	v.set在场时间(p.get在场时间());
	    	v.set失误数(p.get失误数());
	    	v.set总篮板数(p.get总篮板数());
	    	v.set投篮出手数(p.get投篮出手数());
	    	v.set投篮命中数(p.get投篮命中数());
	    	v.set抢断数(p.get抢断数());
	    	v.set犯规数(p.get犯规数());
	    	v.set犯规数(p.get犯规数());
	    	v.set球员名(p.get球员名());
	    	v.set盖帽数(p.get盖帽数());
	    	v.set罚球出手数(p.get罚球出手数());
	    	v.set罚球命中数(p.get罚球命中数());
	    	v.set进攻篮板数(p.get进攻篮板数());
	    	v.set防守篮板数(p.get防守篮板数());
	    	team1Vo.add(v);
	    }
	    Set<PlayerScoreTablePo> team2Po=po.getTeam2PlayersScore();
	    Set<PlayerScoreTableVo> team2Vo=new HashSet<>();
	    for(PlayerScoreTablePo p:team2Po ){
	    	PlayerScoreTableVo v=new PlayerScoreTableVo();
	    	v.set三分出手数(p.get三分出手数());
	    	v.set三分命中数(p.get三分命中数());
	    	v.set个人得分(p.get个人得分());
	    	v.set位置(p.get位置());
	    	v.set助攻数(p.get助攻数());
	    	v.set在场时间(p.get在场时间());
	    	v.set失误数(p.get失误数());
	    	v.set总篮板数(p.get总篮板数());
	    	v.set投篮出手数(p.get投篮出手数());
	    	v.set投篮命中数(p.get投篮命中数());
	    	v.set抢断数(p.get抢断数());
	    	v.set犯规数(p.get犯规数());
	    	v.set犯规数(p.get犯规数());
	    	v.set球员名(p.get球员名());
	    	v.set盖帽数(p.get盖帽数());
	    	v.set罚球出手数(p.get罚球出手数());
	    	v.set罚球命中数(p.get罚球命中数());
	    	v.set进攻篮板数(p.get进攻篮板数());
	    	v.set防守篮板数(p.get防守篮板数());
	    	team2Vo.add(v);
	    }
		vo.setTeam1PlayersScore(team1Vo);
        vo.setTeam2PlayersScore(team2Vo);
		}else{
			vo=null;
		}
		return vo;
	}

	@Override
	public MatchVo queryById(String id) throws Exception {
		if(id==null)
			return null;
		MatchPo qid=new MatchPo();
		qid.setId(id);
		MatchPo po=sqdb.queryById(qid);
		return _vo(po);
	}

	@Override
	public List<MatchVo> query当天比赛() throws Exception {
		return hdb.query当天比赛().stream().map(e->_vo(e)).collect(Collectors.toList());
	}


}
