package bhwz.seac3.bl.import_;

import java.io.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;

import org.junit.Test;

import bhwz.seac3.blservice.import_.ErrorLogPrinter;
import bhwz.seac3.db.nbadata.SaveQueryDB;
import bhwz.seac3.dbservice.nbadata.SaveQueryDBService;
import bhwz.seac3.po.PlayerActionPicturePo;
import bhwz.seac3.po.PlayerPo;
import bhwz.seac3.po.PlayerPortraitPicturePo;
import bhwz.seac3.util.SVGManager;

public class PlayerImportBL{

	public void importPlayerInfo(String path, ErrorLogPrinter elp) {
		if (Data.isValid(path)) {
			Data.withAllData(Data.getFiles(path), file->{return true;},
					file -> {
						SaveQueryDBService<PlayerPo> db = new SaveQueryDB<>();
						try {
							PlayerPo player=_playerPo(file,elp);
							if(player!=null){
								db.save(player);	
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					},elp);
		}else{
			elp.print("文件路径不合法!");
		}
		
		
	}

	public void importPlayerActionPicture(String path, ErrorLogPrinter elp) {
		if (Data.isValid(path)) {
			Data.withAllData(Data.getFiles(path), file->{return true;},
					file -> {
						SaveQueryDBService<PlayerActionPicturePo> db = new SaveQueryDB<>();
						try {
							db.save(_playerActionPicturePo(file));
						} catch (Exception e) {
							e.printStackTrace();
						}
					},elp);
		}else{
			elp.print("文件路径不合法!");
		}
		
	}

	public void importPlayerPortraitPicture(String path, ErrorLogPrinter elp) {
		if (Data.isValid(path)) {
			Data.withAllData(Data.getFiles(path), file->{return true;},
					file -> {
						SaveQueryDBService<PlayerPortraitPicturePo> db = new SaveQueryDB<>();
						try {
							db.save(_playerPortraitPicture(file));
						} catch (Exception e) {
							e.printStackTrace();
						}
					},elp);
		}else{
			elp.print("文件路径不合法!");
		}
		
	}
	private PlayerPo _playerPo(File file,ErrorLogPrinter elp){
		PlayerPo po=new PlayerPo();
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file),"UTF-8"));
			String line = br.readLine();
			Map<String,String> player=new HashMap<>();
			int i=-1;
			while (line != null) {
				if (Pattern.compile("\\w").matcher(line).find()) {
					String newLine = line.replaceAll("\\s│", "│").replaceAll("\\s║", "").replaceAll("║", "");
					i++;
					player.put(playerProperties[i],newLine.substring(newLine.indexOf("│")+1).trim());
				}
				line = br.readLine();
			}
			br.close();
			po.setName(player.get("name"));
			po.setNumber(player.get("number").equals("N/A")?-1:Integer.parseInt(player.get("number")));
			po.setPosition(player.get("position"));
			//写入inch
			String height=player.get("height");
			po.setHeight(Integer.parseInt(height.substring(0,height.indexOf("-")))*12+Integer.parseInt(height.substring(height.indexOf("-")+1)));
			po.setWeight(Integer.parseInt(player.get("weight")));
			po.setBirth(new SimpleDateFormat("MMM dd, yyyy",Locale.US).parse(player.get("birth")).getTime());
			po.setAge(Integer.parseInt(player.get("age")));
			po.setExp(player.get("exp").equals("R")?1:Integer.parseInt(player.get("exp")));
			po.setSchool(player.get("school"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch(NumberFormatException e){
			e.printStackTrace();
			elp.print(file.getName()+" 数据格式不正确!");
			return null;
		}
		return po;
	}
	private PlayerActionPicturePo _playerActionPicturePo(File file){
		PlayerActionPicturePo po=new PlayerActionPicturePo();
		po.setKey(file.getName().substring(0,file.getName().indexOf(".")));
		po.setPicture(new SVGManager().encodeImage(file.getPath()));
		return po;
	}
	private PlayerPortraitPicturePo _playerPortraitPicture(File file){
		PlayerPortraitPicturePo po=new PlayerPortraitPicturePo();
		po.setKey(file.getName().substring(0,file.getName().indexOf(".")));
		po.setPicture(new SVGManager().encodeImage(file.getPath()));
		return po;
	}
	String[] playerProperties={"name","number","position","height","weight","birth","age","exp","school"};
	
	@Test
	public void test_PlayerPO(){
		PlayerImportBL bl=new PlayerImportBL();
	//	bl.importPlayerPortraitPicture("/home/ivan/portrait", s->System.out.println(s));
	//	bl.importPlayerActionPicture("/home/ivan/playerInfo/portrait", s->System.out.println(s));
		bl.importPlayerInfo("/home/ivan/info", s->System.out.println(s));
	}
	
}
