package bhwz.seac3.bl.import_;

import java.io.File;
import java.util.function.Consumer;

import bhwz.seac3.blservice.ReDisplayControllerBLService;
import bhwz.seac3.blservice.import_.ErrorLogPrinter;
import bhwz.seac3.po.MatchPo;

public class Match extends Data{
	public static void withAllData(ReDisplayControllerBLService autoDisplayController,File[] files, MatchFilter filter, Consumer<MatchPo> consumer,ErrorLogPrinter elp) {
		int i=0;
		for (int j=0;j<files.length;j++) {
			MatchPo po=new MatchPo();
			File file=files[j];
			if (filter.matches(file,po,elp)) {
				consumer.accept(po);
			}else{
				elp.print(file.getName()+" 数据被过滤!"+"  "+(++i));
			}
			try{
				autoDisplayController.redisplay();
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		elp.print("共有"+i+"条数据被过滤!");
		elp.print("比赛导入完成");
		try{
			autoDisplayController.forceRedisplay();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
