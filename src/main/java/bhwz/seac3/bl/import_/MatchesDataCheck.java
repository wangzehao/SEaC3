package bhwz.seac3.bl.import_;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import bhwz.seac3.blservice.import_.ErrorLogPrinter;
import bhwz.seac3.po.MatchPo;
import bhwz.seac3.po.PlayerScoreTablePo;

public class MatchesDataCheck implements MatchFilter {
	String[] playerData = { "球员名", "位置", "在场时间", "投篮命中数", "投篮出手数", "三分命中数",
			"三分出手数", "罚球命中数", "罚球出手数", "进攻篮板数", "防守篮板数", "总篮板数", "助攻数", "抢断数",
			"盖帽数", "失误数", "犯规数", "个人得分" };
	String error = "不合法";

	@Override
	@SuppressWarnings("resource")
	public boolean matches(File file, MatchPo match, ErrorLogPrinter elp) {
		Set<PlayerScoreTablePo> team1PlayersScore = new HashSet<>();
		Set<PlayerScoreTablePo> team2PlayersScore = new HashSet<>();
		boolean isTeam1Player = true;
		try {
			BufferedReader bf = new BufferedReader(new InputStreamReader(new FileInputStream(file),"UTF-8"));;
			String line = bf.readLine();
			int i = 1;
			while (line != null) {
				if (line != "") {
					if (i == 1) {
						String filename = file.getName().trim();
						String[] matchInfo = line.trim().split(";");
						int dateStart = filename.indexOf("_") + 1;
						//赛季
						match.setSeason(filename.substring(0,dateStart-1));
						if (!filename.substring(dateStart, dateStart + 5)
								.equals(matchInfo[0])) { // is
							// date
							// correct
							elp.print(file.getName() + " 's date is incorrect!");
							return false;
						}
						//比赛时间
						String year="";
						if(Integer.parseInt(matchInfo[0].substring(0,2))<5){
							//赛季下年
							 year="20"+file.getName().substring(3,5);
						}else{
							 year="20"+file.getName().substring(0,2);
						}
						String date=year+"-"+matchInfo[0];
						Date d1 = null;
						try {
							d1 = new SimpleDateFormat("yyyy-MM-dd").parse(date);
						} catch (ParseException e) {
							e.printStackTrace();
						}
						match.setDate(d1.getTime());
						
						int split1 = matchInfo[1].indexOf("-");
						match.setTeam1(matchInfo[1].substring(0, split1));
						match.setTeam2(matchInfo[1].substring(split1 + 1));
						if (!filename.substring(filename.lastIndexOf("_") + 1,
								filename.lastIndexOf("-")).equals(
								match.getTeam1())
								|| !filename.substring(
										filename.lastIndexOf("-") + 1).equals(
										match.getTeam2())) { // is team correct
							elp.print(file.getName()
									+ " 's TEAM1_TEAM2 is incorrect!");
							return false;
						}
						int split2 = matchInfo[2].indexOf("-");
						match.setTeam1Score(Integer.parseInt(matchInfo[2]
								.substring(0, split2)));
						match.setTeam2Score(Integer.parseInt(matchInfo[2]
								.substring(split2 + 1)));
					} else if (i == 2) {
						String[] section = line.trim().split(";");
						// is sectionScore correct
						int team1SectionSum = 0;
						int team2SectionSum = 0;
						for (int j = 0; j < section.length; j++) {
							team1SectionSum += Integer.parseInt(section[j]
									.substring(0, section[j].indexOf("-")));
							team2SectionSum += Integer.parseInt(section[j]
									.substring(section[j].indexOf("-") + 1));
						}

						if (match.getTeam1Score() != team1SectionSum
								|| match.getTeam2Score() != team2SectionSum) {
							elp.print(file.getName()
									+ " section sum is not equal to total score!");
							return false;
						} else {
							match.setSectionScores(Arrays.asList(section));
						}

					} else if (i == 3) {
						if (!match.getTeam1().equals(line.trim())) {
							elp.print(file.getName()
									+ " 's TEAM1_TEAM2 is incorrect!");
							return false;
						}
					} else if (line.trim().equals(match.getTeam2())) {
						isTeam1Player = false;
					} else {
						PlayerScoreTablePo t = new PlayerScoreTablePo();
						String[] a = line.split(";");
						if (a[0].equals("")) {
							elp.print(playerData[0] + error);
							return false;
						}
						t.set球员名(a[0].trim());
						t.set位置(a[1].trim());
						// 判断在场时间是否合法
						Matcher m = Pattern.compile("(\\d+):(\\d+)").matcher(
								a[2].trim());
						if (!m.matches()) {
							elp.print(file.getName() + " " + t.get球员名()
									+ playerData[2] + error);
							return false;
						} else {
							t.set在场时间(Integer.parseInt(m.group(1)) * 60
									+ Integer.parseInt(m.group(2)));
						}
						// 判断球员数据项是否合法
						for (int k = 3; k < 18; k++) {
							Matcher m2 = Pattern.compile("\\d+").matcher(
									a[k].trim());
							if (!m2.matches()) {
								elp.print(file.getName() + " " + t.get球员名()
										+ " " + playerData[k] + error);
								return false;
							}
						}
						t.set投篮命中数(Integer.parseInt(a[3].trim()));
						t.set投篮出手数(Integer.parseInt(a[4].trim()));
						t.set三分命中数(Integer.parseInt(a[5].trim()));
						t.set三分出手数(Integer.parseInt(a[6].trim()));
						t.set罚球命中数(Integer.parseInt(a[7].trim()));
						t.set罚球出手数(Integer.parseInt(a[8].trim()));
						t.set进攻篮板数(Integer.parseInt(a[9].trim()));
						t.set防守篮板数(Integer.parseInt(a[10].trim()));
						t.set总篮板数(Integer.parseInt(a[11].trim()));
						t.set助攻数(Integer.parseInt(a[12].trim()));
						t.set抢断数(Integer.parseInt(a[13].trim()));
						t.set盖帽数(Integer.parseInt(a[14].trim()));
						t.set失误数(Integer.parseInt(a[15].trim()));
						t.set犯规数(Integer.parseInt(a[16].trim()));
						t.set个人得分(Integer.parseInt(a[17].trim()));

						if (t.get三分出手数() < t.get三分命中数()) {
							elp.print(file.getName() + t.get球员名()
									+ " 三分出手数小于三分命中数!");
							return false;
						}
						if (t.get总篮板数() != t.get进攻篮板数() + t.get防守篮板数()) {
							elp.print(file.getName() + t.get球员名() + " 篮板数不正确!");
							return false;
						}
						if (t.get罚球出手数() < t.get罚球命中数()) {
							elp.print(file.getName() + t.get球员名()
									+ " 罚球出手数小于罚球命中数!");
							return false;
						}
						if (t.get犯规数() > 6) {
							elp.print(file.getName() + t.get球员名() + " 犯规数大于6!");
							return false;
						}
						if (t.get投篮命中数() > t.get投篮出手数()) {
							elp.print(file.getName() + t.get球员名()
									+ " 投篮出手数小于投篮命中数!");
							return false;
						}
						if (t.get投篮命中数() < t.get三分命中数()) {
							elp.print(file.getName() + t.get球员名()
									+ " 投篮命中数小于三分命中数!");
							return false;
						}

						if (t.get个人得分() != (t.get投篮命中数() - t.get三分命中数()) * 2
								+ t.get三分命中数() * 3 + t.get罚球命中数() * 1) {
							elp.print(file.getName() + " " + t.get球员名()
									+ " 个人得分计算错误!");
							return false;
						}
						if (isTeam1Player == true) {
							team1PlayersScore.add(t);
						} else {
							team2PlayersScore.add(t);
						}
					}
					++i;
				}
				line = bf.readLine();
			}
			// 遍历set
			int team1ScoreSum = 0;
			int team2ScoreSum = 0;
			for (PlayerScoreTablePo t : team1PlayersScore) {
				team1ScoreSum += t.get个人得分();
			}
			for (PlayerScoreTablePo t : team2PlayersScore) {
				team2ScoreSum += t.get个人得分();
			}
			if (team1ScoreSum != match.getTeam1Score()) {
				elp.print(file.getName() + " team1 球员得分和不等于总得分!");
				return false;
			}
			if (team2ScoreSum != match.getTeam2Score()) {
				elp.print(file.getName() + " team2 球员得分和不等于总得分!");
				return false;
			}
			bf.close();
			match.setTeam1PlayersScore(team1PlayersScore);
			match.setTeam2PlayersScore(team2PlayersScore);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return true;
	}

}
