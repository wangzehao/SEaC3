package bhwz.seac3.bl.import_;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

import bhwz.seac3.RMIClientBLServiceManager;
import bhwz.seac3.bl.datacomputer.DataComputer;
import bhwz.seac3.blservice.ReDisplayControllerBLService;
import bhwz.seac3.blservice.import_.ErrorLogPrinter;
import bhwz.seac3.blservice.query.MatchQueryBLService;
import bhwz.seac3.db.nbadata.PlayerMatchesEntity;
import bhwz.seac3.db.nbadata.SaveQueryDB;
import bhwz.seac3.db.nbadata.PlayerMatchDB;
import bhwz.seac3.dbservice.nbadata.PlayerMatchDBService;
import bhwz.seac3.dbservice.nbadata.SaveQueryDBService;
import bhwz.seac3.po.MatchPo;
import bhwz.seac3.po.PlayerPo;
import bhwz.seac3.po.PlayerScoreTablePo;
import bhwz.seac3.vo.MatchVo;
import bhwz.seac3.vo.PlayerScoreTableVo;

public class MatchImportBL {
	static SaveQueryDBService<MatchPo> db = new SaveQueryDB<>();
	private ReDisplayControllerBLService autoDisplayController;

	public MatchImportBL(ReDisplayControllerBLService autoDisplayController) {
		this.autoDisplayController=autoDisplayController;
	}

	public void importMatch(String path, ErrorLogPrinter elp) {
		if (Data.isValid(path)) {
			Match.withAllData(autoDisplayController,Data.getFiles(path), new MatchesDataCheck(),
					po -> {
						saveMatch(po);
					}, elp);

		} else {
			elp.print("文件路径不合法!");
		}
	}

	public void importOneMatch(File file, ErrorLogPrinter elp) {
		MatchPo po = new MatchPo();
		boolean isValid = new MatchesDataCheck().matches(file, po, elp);
		if (isValid) {
			saveMatch(po);
		}
	}
	
	public void importOneMatch(MatchVo vo){
		saveMatch(_po(vo));
	}

	private MatchVo _vo(MatchPo po) {
		MatchVo vo = new MatchVo();
		vo.setDate(po.getDate());
		vo.setSeason(po.getSeason());
		vo.setSectionScores(po.getSectionScores());
		vo.setTeam1(po.getTeam1());
		vo.setTeam1Score(po.getTeam1Score());
		vo.setTeam2(po.getTeam2());
		vo.setTeam2Score(po.getTeam2Score());
		Set<PlayerScoreTablePo> team1Po = po.getTeam1PlayersScore();
		Set<PlayerScoreTableVo> team1Vo = new HashSet<>();
		for (PlayerScoreTablePo p : team1Po) {
			PlayerScoreTableVo v = new PlayerScoreTableVo();
			v.set三分出手数(p.get三分出手数());
			v.set三分命中数(p.get三分命中数());
			v.set个人得分(p.get个人得分());
			v.set位置(p.get位置());
			v.set助攻数(p.get助攻数());
			v.set在场时间(p.get在场时间());
			v.set失误数(p.get失误数());
			v.set总篮板数(p.get总篮板数());
			v.set投篮出手数(p.get投篮出手数());
			v.set投篮命中数(p.get投篮命中数());
			v.set抢断数(p.get抢断数());
			v.set犯规数(p.get犯规数());
			v.set犯规数(p.get犯规数());
			v.set球员名(p.get球员名());
			v.set盖帽数(p.get盖帽数());
			v.set罚球出手数(p.get罚球出手数());
			v.set罚球命中数(p.get罚球命中数());
			v.set进攻篮板数(p.get进攻篮板数());
			v.set防守篮板数(p.get防守篮板数());
			team1Vo.add(v);
		}
		Set<PlayerScoreTablePo> team2Po = po.getTeam2PlayersScore();
		Set<PlayerScoreTableVo> team2Vo = new HashSet<>();
		for (PlayerScoreTablePo p : team2Po) {
			PlayerScoreTableVo v = new PlayerScoreTableVo();
			v.set三分出手数(p.get三分出手数());
			v.set三分命中数(p.get三分命中数());
			v.set个人得分(p.get个人得分());
			v.set位置(p.get位置());
			v.set助攻数(p.get助攻数());
			v.set在场时间(p.get在场时间());
			v.set失误数(p.get失误数());
			v.set总篮板数(p.get总篮板数());
			v.set投篮出手数(p.get投篮出手数());
			v.set投篮命中数(p.get投篮命中数());
			v.set抢断数(p.get抢断数());
			v.set犯规数(p.get犯规数());
			v.set犯规数(p.get犯规数());
			v.set球员名(p.get球员名());
			v.set盖帽数(p.get盖帽数());
			v.set罚球出手数(p.get罚球出手数());
			v.set罚球命中数(p.get罚球命中数());
			v.set进攻篮板数(p.get进攻篮板数());
			v.set防守篮板数(p.get防守篮板数());
			team2Vo.add(v);
		}

		vo.setTeam1PlayersScore(team1Vo);
		vo.setTeam2PlayersScore(team2Vo);

		return vo;
	}

	private MatchPo _po(MatchVo vo) {
		MatchPo po = new MatchPo();
		po.setDate(vo.getDate());
		po.setSeason(vo.getSeason());
		po.setSectionScores(vo.getSectionScores());
		po.setTeam1(vo.getTeam1());
		po.setTeam1Score(vo.getTeam1Score());
		po.setTeam2(vo.getTeam2());
		po.setTeam2Score(vo.getTeam2Score());
		Set<PlayerScoreTableVo> team1Vo = vo.getTeam1PlayersScore();
		Set<PlayerScoreTablePo> team1Po = new HashSet<>();
		for (PlayerScoreTableVo p : team1Vo) {
			PlayerScoreTablePo v = new PlayerScoreTablePo();
			v.set三分出手数(p.get三分出手数());
			v.set三分命中数(p.get三分命中数());
			v.set个人得分(p.get个人得分());
			v.set位置(p.get位置());
			v.set助攻数(p.get助攻数());
			v.set在场时间(p.get在场时间());
			v.set失误数(p.get失误数());
			v.set总篮板数(p.get总篮板数());
			v.set投篮出手数(p.get投篮出手数());
			v.set投篮命中数(p.get投篮命中数());
			v.set抢断数(p.get抢断数());
			v.set犯规数(p.get犯规数());
			v.set犯规数(p.get犯规数());
			v.set球员名(p.get球员名());
			v.set盖帽数(p.get盖帽数());
			v.set罚球出手数(p.get罚球出手数());
			v.set罚球命中数(p.get罚球命中数());
			v.set进攻篮板数(p.get进攻篮板数());
			v.set防守篮板数(p.get防守篮板数());
			team1Po.add(v);
		}
		Set<PlayerScoreTableVo> team2Vo = vo.getTeam2PlayersScore();
		Set<PlayerScoreTablePo> team2Po = new HashSet<>();
		for (PlayerScoreTableVo p : team2Vo) {
			PlayerScoreTablePo v = new PlayerScoreTablePo();
			v.set三分出手数(p.get三分出手数());
			v.set三分命中数(p.get三分命中数());
			v.set个人得分(p.get个人得分());
			v.set位置(p.get位置());
			v.set助攻数(p.get助攻数());
			v.set在场时间(p.get在场时间());
			v.set失误数(p.get失误数());
			v.set总篮板数(p.get总篮板数());
			v.set投篮出手数(p.get投篮出手数());
			v.set投篮命中数(p.get投篮命中数());
			v.set抢断数(p.get抢断数());
			v.set犯规数(p.get犯规数());
			v.set犯规数(p.get犯规数());
			v.set球员名(p.get球员名());
			v.set盖帽数(p.get盖帽数());
			v.set罚球出手数(p.get罚球出手数());
			v.set罚球命中数(p.get罚球命中数());
			v.set进攻篮板数(p.get进攻篮板数());
			v.set防守篮板数(p.get防守篮板数());
			team2Po.add(v);
		}

		po.setTeam1PlayersScore(team1Po);
		po.setTeam2PlayersScore(team2Po);

		return po;
	}
	
	private void saveMatch(MatchPo po) {
		try {
			 MatchVo match = ((MatchQueryBLService) RMIClientBLServiceManager
					.getBLService(MatchQueryBLService.class)).queryById(po.getId());
			if(match==null){
				db.save(po);
			// 更新球员数据接口
			DataComputer data = new DataComputer();
			data.AppendMatchDataToPlayerAndTeam(_vo(po));
			// 添加比赛到球员中
			SaveQueryDBService<PlayerMatchesEntity> mapdb = new SaveQueryDB<>();
			SaveQueryDBService<MatchPo> matchdb = new SaveQueryDB<>();
			PlayerMatchDBService pm = new PlayerMatchDB(mapdb, matchdb);
			Set<PlayerScoreTablePo> team1Po = po.getTeam1PlayersScore();
			for (PlayerScoreTablePo pst : team1Po) {
				PlayerPo player = new PlayerPo();
				player.setName(pst.get球员名());
				pm.addMatchToPlayer(po, player);
			}
			Set<PlayerScoreTablePo> team2Po = po.getTeam2PlayersScore();
			for (PlayerScoreTablePo pst : team2Po) {
				PlayerPo player = new PlayerPo();
				player.setName(pst.get球员名());
				pm.addMatchToPlayer(po, player);
			}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
