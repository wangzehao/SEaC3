package bhwz.seac3.bl.import_;

import java.io.File;

public interface DataFilter {
     public boolean matches(File file);
}
