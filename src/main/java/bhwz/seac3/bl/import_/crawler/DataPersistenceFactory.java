package bhwz.seac3.bl.import_.crawler;

public class DataPersistenceFactory {
	public static DataPersistence create(String className){
		DataPersistence dp=null;
		try {
				dp=(DataPersistence) Class.forName(DataPersistence.class.getPackage().getName()+"."+className).newInstance();
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		return dp;
	}
}
