package bhwz.seac3.bl.import_.crawler;

import bhwz.seac3.bl.RedisplayControllerBL;
import bhwz.seac3.bl.import_.MatchImportBL;
import bhwz.seac3.vo.MatchVo;

public class DataPersistenceInDatabase extends DataPersistence {

	static MatchImportBL bl = new MatchImportBL(new RedisplayControllerBL());

	@Override
	public void save(MatchVo vo) {
		bl.importOneMatch(vo);
	}

}
