package bhwz.seac3.bl.import_.crawler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public class Downloader {
	public String download(String url) {
		String html = "";
		try {
			URL Url = new URL(url);
			URLConnection c = Url.openConnection();
			html = convertStreamToString(c.getInputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("download  "+url);
		return html;
	}

	private String convertStreamToString(InputStream is) {
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();

		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}
	
}
