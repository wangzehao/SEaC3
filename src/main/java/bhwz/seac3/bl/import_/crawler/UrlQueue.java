package bhwz.seac3.bl.import_.crawler;

import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;

public class UrlQueue {
//	private static Set<String> visitedUrls = new HashSet<>();
	private static Queue<String> unVisitedUrls = new ConcurrentLinkedQueue<>();

/*	public static Set<String> getVisitedUrl() {
		return visitedUrls;
	}
*/
	public static Queue<String> getUnVisitedUrl() {
		return unVisitedUrls;
	}

	public static  void addUnvisitedUrl(String url) {
		if(url!=null&&!unVisitedUrls.contains(url)){
			unVisitedUrls.add(url);
		}
	}

	public static void addUnvisitedUrls(Set<String> urls) {
		for(String url:urls){
			if(url!=null&&!unVisitedUrls.contains(url)&&!unVisitedUrls.contains(url)){
				addUnvisitedUrl(url);
			}
		}
	}
	
	public static void addVisitedUrl(String url){
		if(url!=null){
//			visitedUrls.add(url);
		}
	}
	
	public static String unVisitedUrlsDequeue(){
		return unVisitedUrls.poll();
	}
	
	public static boolean isUnVisitedUrlsEmpty(){
		return unVisitedUrls.isEmpty();
	}
}
