package bhwz.seac3.bl.import_.crawler;

import java.util.function.BiConsumer;

import bhwz.seac3.vo.MatchVo;

public class DataPersistence implements BiConsumer<String,String>{
	protected MatchVo vo;
	
	@Override
	public void accept(String htmlPerGame,String baseUrl) {
		HtmlParser parser=new HtmlParser();
		vo=parser.encapsulateMatchVo(htmlPerGame,baseUrl);
		save(vo);
	}
	
	public void save(MatchVo vo){
		
	}
}
