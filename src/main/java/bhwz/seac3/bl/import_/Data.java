package bhwz.seac3.bl.import_;

import java.io.File;
import java.util.function.Consumer;

import bhwz.seac3.blservice.import_.ErrorLogPrinter;

public class Data {
	public static void withAllData(File[] files, DataFilter filter, Consumer<File> consumer,ErrorLogPrinter elp) {
		if(files.length==0){
			elp.print("没有文件!");
			return;
		}
		for (File file : files) {
			if (filter.matches(file)) {
				consumer.accept(file);
			}
		}
	}
	public static File[] getFiles(String path) {
		return new File(path).listFiles();
	}
	public static boolean isValid(String path){
		File file=new File(path);
		if(path.equals("")||path==null||!file.exists()||!file.isDirectory()){
			return false;	
		}
		return true;
	}
	
}
