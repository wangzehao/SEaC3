package bhwz.seac3.bl.import_;

import java.io.File;

import bhwz.seac3.blservice.import_.ErrorLogPrinter;
import bhwz.seac3.po.MatchPo;

public interface MatchFilter {
     public boolean matches(File file, MatchPo po,ErrorLogPrinter elp);
}
