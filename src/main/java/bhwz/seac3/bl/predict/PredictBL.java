package bhwz.seac3.bl.predict;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import bhwz.seac3.blservice.predict.PredictBLService;
import bhwz.seac3.blservice.query.SeasonQueryBLService;
import bhwz.seac3.blservice.query.TeamQueryBLService;
import bhwz.seac3.vo.PredictVo;
import bhwz.seac3.vo.TeamVo;

public class PredictBL implements PredictBLService {
	TeamQueryBLService tbl;
	SeasonQueryBLService sbl;

	public PredictBL(TeamQueryBLService tbl, SeasonQueryBLService sbl) {
		this.tbl = tbl;
		this.sbl = sbl;
	}

	@Override
	public PredictVo predict(TeamVo team1, TeamVo team2) throws Exception {
		PredictVo result = new PredictVo();
		List<Double> team1得分率 = get近100左右场得分率(team1);
		List<Double> team2得分率 = get近100左右场得分率(team2);
		result.team1.得分率=(team1得分率);
		result.team2.得分率=(team2得分率);
		result.team1.teamname=team1.get球队简称();
		result.team2.teamname=team2.get球队简称();
		return result;
	}

	private List<Double> get近100左右场得分率(TeamVo team) throws Exception {
		final int maxSize=100;
		List<String> seasons = sbl.queryJoinedSeasons(team);
		List<TeamVo> allMatches = new ArrayList<>();
		for (int i=0;i<seasons.size();i++) {
			String season=seasons.get(i);
			try {
				allMatches.addAll(tbl.query每场比赛表现(season, team));
				if(allMatches.size()>=maxSize){
					break;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return allMatches
				.stream()
				.map(teamVo -> get得分率(teamVo)).collect(Collectors.toList());
	}
	
	private Double get得分率(TeamVo teamVo){
		double get比赛得分 = teamVo.get比赛得分();
		int get对手得分 = teamVo
				.get对手得分();
		return (get比赛得分 / (get比赛得分 + get对手得分));
	}

}
