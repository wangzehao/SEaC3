package bhwz.seac3.bl.datacomputer;

import bhwz.seac3.util.Formula;
import bhwz.seac3.vo.TeamVo;

public class TeamComputer {
	
    private Formula f;
	
	private int 胜利场数;	
	private int 对手得分;
	private double 进攻回合;
	private double 防守回合;
	private int 对手进攻篮板数;
	private int 对方防守篮板数;

	private int 比赛场数;
	private double 投篮数;
	private double 罚球数;
	private double 失误数;	
	private double 断球数;
	private double 助攻数;
	private double 盖帽数;
	private double 犯规数;
	private double 比赛得分;
	private double 投篮命中数;
	private double 三分命中数;
	private double 三分出手数;
	private double 罚球命中数;
	private double 前场篮板数;
	private double 后场篮板数;	

	private double 命中率;
	private double 三分命中率;
	private double 罚球命中率;
	private double 篮板数;
	private double 胜率;
	private double 进攻效率;
	private double 防守效率;
	private double 进攻篮板效率;
	private double 防守篮板效率;
	private double 抢断效率;
	private double 助攻率;
	
	public TeamComputer(){
		f=new Formula();
	}
	
	public TeamVo computeTeamDataAllGame(TeamVo allGameData){
		胜利场数=allGameData.get胜利场数();	
		对手得分=allGameData.get对手得分();
		进攻回合=allGameData.get进攻回合();
		防守回合=allGameData.get防守回合();
		对手进攻篮板数=allGameData.get对手进攻篮板数();
		对方防守篮板数=allGameData.get对方防守篮板数();

		比赛场数=allGameData.get比赛场数();
		投篮数=allGameData.get投篮数();
		罚球数=allGameData.get罚球数();
		断球数=allGameData.get断球数();
		助攻数=allGameData.get助攻数();
		比赛得分=allGameData.get比赛得分();
		投篮命中数=allGameData.get投篮命中数();
		三分命中数=allGameData.get三分命中数();
		三分出手数=allGameData.get三分出手数();
		罚球命中数=allGameData.get罚球命中数();
		前场篮板数=allGameData.get前场篮板数();
		后场篮板数=allGameData.get后场篮板数();	

		命中率=f.teamFieldGoalPer(投篮命中数, 投篮数);
		三分命中率=f.teamFieldGoalPer(三分命中数, 三分出手数);
		罚球命中率=f.teamFieldGoalPer(罚球命中数, 罚球数);
		篮板数=前场篮板数+后场篮板数;
		胜率=f.teamWinPer(胜利场数, 比赛场数);
		进攻效率=f.teamATKEffPer(进攻回合, 比赛得分);
		防守效率=f.teamDEFEffPer(防守回合, 对手得分);
		进攻篮板效率=f.teamATKReboundEff(前场篮板数, 对方防守篮板数);
		防守篮板效率=f.teamDEFReboundEff(后场篮板数, 对手进攻篮板数);
		抢断效率=f.teamStealEffPer(防守回合, 断球数);
		助攻率=f.teamAssistPer(进攻回合, 助攻数);
		
		allGameData.set命中率(命中率);
		allGameData.set三分命中率(三分命中率);
		allGameData.set罚球命中率(罚球命中率);
		allGameData.set篮板数(篮板数);
		allGameData.set胜率(胜率);
		allGameData.set进攻效率(进攻效率);
		allGameData.set防守效率(防守效率);
		allGameData.set进攻篮板效率(进攻篮板效率);
		allGameData.set防守篮板效率(防守篮板效率);
		allGameData.set抢断效率(抢断效率);
		allGameData.set助攻率(助攻率);
		
		return allGameData;
	}

	public TeamVo computeTeamDataPerGame(TeamVo allGameData){
		胜利场数=allGameData.get胜利场数();	
		对手得分=allGameData.get对手得分();
		进攻回合=allGameData.get进攻回合();
		防守回合=allGameData.get防守回合();
		对手进攻篮板数=allGameData.get对手进攻篮板数();
		对方防守篮板数=allGameData.get对方防守篮板数();

		比赛场数=allGameData.get比赛场数();
		投篮数=allGameData.get投篮数();
		罚球数=allGameData.get罚球数();
		断球数=allGameData.get断球数();
		助攻数=allGameData.get助攻数();
		比赛得分=allGameData.get比赛得分();
		投篮命中数=allGameData.get投篮命中数();
		三分命中数=allGameData.get三分命中数();
		三分出手数=allGameData.get三分出手数();
		罚球命中数=allGameData.get罚球命中数();
		前场篮板数=allGameData.get前场篮板数();
		后场篮板数=allGameData.get后场篮板数();	
		失误数=allGameData.get失误数();
		盖帽数=allGameData.get盖帽数();
		犯规数=allGameData.get犯规数();

		命中率=f.teamFieldGoalPer(投篮命中数, 投篮数);
		三分命中率=f.teamFieldGoalPer(三分命中数, 三分出手数);
		罚球命中率=f.teamFieldGoalPer(罚球命中数, 罚球数);
		篮板数=前场篮板数+后场篮板数;
		胜率=f.teamWinPer(胜利场数, 比赛场数);
		进攻效率=f.teamATKEffPer(进攻回合, 比赛得分);
		防守效率=f.teamDEFEffPer(防守回合, 对手得分);
		进攻篮板效率=f.teamATKReboundEff(前场篮板数, 对方防守篮板数);
		防守篮板效率=f.teamDEFReboundEff(后场篮板数, 对手进攻篮板数);
		抢断效率=f.teamStealEffPer(防守回合, 断球数);
		助攻率=f.teamAssistPer(进攻回合, 助攻数);
		
		allGameData.set命中率(命中率);
		allGameData.set三分命中率(三分命中率);
		allGameData.set罚球命中率(罚球命中率);
		allGameData.set胜率(胜率);
		allGameData.set进攻效率(进攻效率);
		allGameData.set防守效率(防守效率);
		allGameData.set进攻篮板效率(进攻篮板效率);
		allGameData.set防守篮板效率(防守篮板效率);
		allGameData.set抢断效率(抢断效率);
		allGameData.set助攻率(助攻率);
		
		篮板数=f.average(篮板数, 比赛场数);
		进攻回合=f.average(进攻回合, 比赛场数);
		防守回合=f.average(防守回合, 比赛场数);
		投篮数=f.average(投篮数,比赛场数);
		罚球数=f.average(罚球数, 比赛场数);
		断球数=f.average(罚球数, 比赛场数);
		助攻数=f.average(助攻数, 比赛场数);
		比赛得分=f.average(比赛得分, 比赛场数);
		投篮命中数=f.average(投篮命中数, 比赛场数);
		三分命中数=f.average(三分命中数, 比赛场数);
		三分出手数=f.average(三分出手数, 比赛场数);
		罚球命中数=f.average(罚球命中数, 比赛场数);
		前场篮板数=f.average(前场篮板数, 比赛场数);
		后场篮板数=f.average(后场篮板数, 比赛场数);
		失误数=f.average(失误数, 比赛场数);	
		盖帽数=f.average(盖帽数, 比赛场数);
		犯规数=f.average(犯规数, 比赛场数);
		对手得分=(int)f.average(对手得分, 比赛场数);
		
		allGameData.set篮板数(篮板数);
		allGameData.set进攻回合(进攻回合);
		allGameData.set防守回合(防守回合);
		allGameData.set投篮数(投篮数);
		allGameData.set罚球数(罚球数);
		allGameData.set断球数(断球数);
		allGameData.set助攻数(助攻数);
		allGameData.set比赛得分(比赛得分);
		allGameData.set投篮命中数(投篮命中数);
		allGameData.set三分命中数(三分命中数);
		allGameData.set三分出手数(三分出手数);
		allGameData.set罚球命中数(罚球命中数);
		allGameData.set前场篮板数(前场篮板数);
		allGameData.set后场篮板数(后场篮板数);
		allGameData.set失误数(失误数);
		allGameData.set盖帽数(盖帽数);
		allGameData.set犯规数(犯规数);
		allGameData.set对手得分(对手得分);
		
		return allGameData;
	}
	
}
