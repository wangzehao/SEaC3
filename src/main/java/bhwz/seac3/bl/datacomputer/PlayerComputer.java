package bhwz.seac3.bl.datacomputer;

import bhwz.seac3.util.Formula;
import bhwz.seac3.vo.PlayerVo;

public class PlayerComputer {
	
	private Formula f;
	
	//from GameDataPo
	private int 比赛场数;
	private double 投篮数;
	private double 罚球数;
	private double 失误数;	
	private double 断球数;
	private double 助攻数;
	private double 盖帽数;
	private double 犯规数;
	private double 比赛得分;
	private double 投篮命中数;
	private double 三分命中数;
	private double 三分出手数;
	private double 罚球命中数;
	private double 前场篮板数;
	private double 后场篮板数;
	//from DataComputer
	private double 命中率;
	private double 效率;
	private double GmSc效率值;
	private double 真实投篮命中率;
	private double 投篮效率;
	private double 篮板数;
	private double 罚球命中率;
	private double 三分命中率;
	private double 投篮命中率;

	public PlayerComputer(){
		f=new Formula();
	}
	
	public PlayerVo computePlayerDataAllGame(PlayerVo allGameData) {
		
		投篮数=allGameData.get投篮数();
		罚球数=allGameData.get罚球数();
		失误数=allGameData.get失误数();	
		断球数=allGameData.get断球数();
		助攻数=allGameData.get助攻数();
		盖帽数=allGameData.get盖帽数();
		犯规数=allGameData.get犯规数();
		比赛得分=allGameData.get比赛得分();
		投篮命中数=allGameData.get投篮命中数();
		三分命中数=allGameData.get三分命中数();
		三分出手数=allGameData.get三分出手数();
		罚球命中数=allGameData.get罚球命中数();
		前场篮板数=allGameData.get前场篮板数();
		后场篮板数=allGameData.get后场篮板数();
		
		篮板数=前场篮板数+后场篮板数;
		命中率=f.playerFieldGoalPer(投篮命中数, 投篮数);
		效率=f.playerEffPer(比赛得分, 篮板数, 助攻数, 断球数, 盖帽数, 投篮数, 投篮命中数, 罚球数, 罚球命中数, 失误数);
		GmSc效率值=f.playerGmScPer(比赛得分, 投篮命中数, 投篮数, 罚球数, 罚球命中数, 前场篮板数, 后场篮板数, 断球数, 助攻数, 盖帽数, 犯规数, 失误数);
		真实投篮命中率=f.playerRealFieldGoalPer(比赛得分, 投篮数, 罚球数);
		投篮效率=f.playerShotEffPer(投篮命中数, 三分命中数, 投篮数);
		罚球命中率=f.playerFieldGoalPer(罚球命中数, 罚球数);
		三分命中率=f.playerFieldGoalPer(三分命中数, 三分出手数);
		投篮命中率=f.playerFieldGoalPer(投篮命中数, 投篮数);
		
	    allGameData.set篮板数(篮板数);
	    allGameData.set命中率(命中率);
	    allGameData.set效率(效率);
	    allGameData.setGmSc效率值(GmSc效率值);
		allGameData.set真实投篮命中率(真实投篮命中率);
		allGameData.set投篮效率(投篮效率);
		allGameData.set罚球命中率(罚球命中率);
		allGameData.set三分命中率(三分命中率);
		allGameData.set投篮命中率(投篮命中率);
	    
		return allGameData;
	}
	
	public PlayerVo computePlayerDataPerGame(PlayerVo allGameData) {
		
		比赛场数=allGameData.get比赛场数();
		投篮数=allGameData.get投篮数();
		罚球数=allGameData.get罚球数();
		失误数=allGameData.get失误数();	
		断球数=allGameData.get断球数();
		助攻数=allGameData.get助攻数();
		盖帽数=allGameData.get盖帽数();
		犯规数=allGameData.get犯规数();
		比赛得分=allGameData.get比赛得分();
		投篮命中数=allGameData.get投篮命中数();
		三分命中数=allGameData.get三分命中数();
		三分出手数=allGameData.get三分出手数();
		罚球命中数=allGameData.get罚球命中数();
		前场篮板数=allGameData.get前场篮板数();
		后场篮板数=allGameData.get后场篮板数();
		
		篮板数=前场篮板数+后场篮板数;
		篮板数=f.average(篮板数, 比赛场数);
		命中率=f.playerFieldGoalPer(投篮命中数, 投篮数);
		效率=f.playerEffPer(比赛得分, 篮板数, 助攻数, 断球数, 盖帽数, 投篮数, 投篮命中数, 罚球数, 罚球命中数, 失误数);
		效率=f.average(效率, 比赛场数);
		GmSc效率值=f.playerGmScPer(比赛得分, 投篮命中数, 投篮数, 罚球数, 罚球命中数, 前场篮板数, 后场篮板数, 断球数, 助攻数, 盖帽数, 犯规数, 失误数);
		GmSc效率值=f.average(GmSc效率值, 比赛场数);
		真实投篮命中率=f.playerRealFieldGoalPer(比赛得分, 投篮数, 罚球数);
		真实投篮命中率=f.average(真实投篮命中率, 比赛场数);
		投篮效率=f.playerShotEffPer(投篮命中数, 三分命中数, 投篮数);
		投篮效率=f.average(投篮效率, 比赛场数);
		罚球命中率=f.playerFieldGoalPer(罚球命中数, 罚球数);
		三分命中率=f.playerFieldGoalPer(三分命中数, 三分出手数);
		投篮命中率=f.playerFieldGoalPer(投篮命中数, 投篮数);
		
		投篮数=f.average(投篮数, 比赛场数);
		罚球数=f.average(罚球数, 比赛场数);
		失误数=f.average(失误数, 比赛场数);
		断球数=f.average(断球数, 比赛场数);
		助攻数=f.average(助攻数, 比赛场数);
		盖帽数=f.average(盖帽数, 比赛场数);
		犯规数=f.average(犯规数, 比赛场数);
		比赛得分=f.average(比赛得分, 比赛场数);
		投篮命中数=f.average(投篮命中数, 比赛场数);
		三分命中数=f.average(三分命中数, 比赛场数);
		三分出手数=f.average(三分出手数, 比赛场数);
		罚球命中数=f.average(罚球命中数, 比赛场数);
		前场篮板数=f.average(前场篮板数, 比赛场数);
		后场篮板数=f.average(后场篮板数, 比赛场数);
		
		allGameData.set篮板数(篮板数);
		allGameData.set命中率(命中率);
		allGameData.set效率(效率);
		allGameData.setGmSc效率值(GmSc效率值);
		allGameData.set真实投篮命中率(真实投篮命中率);
		allGameData.set投篮效率(投篮效率);
		allGameData.set罚球命中率(罚球命中率);
		allGameData.set三分命中率(三分命中率);
		allGameData.set投篮命中率(投篮命中率);
		
		allGameData.set投篮数(投篮数);
		allGameData.set罚球数(罚球数);
		allGameData.set失误数(失误数);
		allGameData.set断球数(断球数);
		allGameData.set助攻数(助攻数);
		allGameData.set盖帽数(盖帽数);
		allGameData.set犯规数(犯规数);
		allGameData.set比赛得分(比赛得分);
		allGameData.set投篮命中数(投篮命中数);
		allGameData.set三分命中数(三分命中数);
		allGameData.set三分出手数(三分出手数);
		allGameData.set罚球命中数(罚球命中数);
		allGameData.set前场篮板数(前场篮板数);
		allGameData.set后场篮板数(后场篮板数);
		
		return allGameData;
	}

}
