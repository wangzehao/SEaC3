package bhwz.seac3.bl.datacomputer;

import bhwz.seac3.blservice.datacomputer.MatchDataAppendBLService;
import bhwz.seac3.blservice.datacomputer.PerGamePlayerDataComputeBLService;
import bhwz.seac3.blservice.datacomputer.PerGameTeamDataComputeBLService;
import bhwz.seac3.vo.MatchVo;
import bhwz.seac3.vo.PlayerVo;
import bhwz.seac3.vo.TeamVo;

public class DataComputer implements PerGamePlayerDataComputeBLService,
		PerGameTeamDataComputeBLService, MatchDataAppendBLService {

	@Override
	public void AppendMatchDataToPlayerAndTeam(MatchVo match) throws Exception {
		// TODO Auto-generated method stub
		MatchComputer m=new MatchComputer();
		m.AppendMatchDataToPlayerAndTeam(match);
	}

	@Override
	public TeamVo computeTeamDataPerGame(TeamVo allGameData) {
		// TODO Auto-generated method stub
		TeamComputer t=new TeamComputer();
		TeamVo vo=t.computeTeamDataPerGame(allGameData);
		return vo;
	}

	@Override
	public TeamVo computeTeamDataAllGame(TeamVo allGameData) {
		// TODO Auto-generated method stub
		TeamComputer t=new TeamComputer();
		TeamVo vo=t.computeTeamDataAllGame(allGameData);
		return vo;
	}

	@Override
	public PlayerVo computePlayerDataPerGame(PlayerVo allGameData) {
		// TODO Auto-generated method stub
		PlayerComputer p=new PlayerComputer();
		PlayerVo vo=p.computePlayerDataPerGame(allGameData);
		return vo;
	}

	@Override
	public PlayerVo computePlayerDataAllGame(PlayerVo allGameData) {
		// TODO Auto-generated method stub
		PlayerComputer p=new PlayerComputer();
		PlayerVo vo=p.computePlayerDataAllGame(allGameData);
		return vo;
	}

	
	
	

}
