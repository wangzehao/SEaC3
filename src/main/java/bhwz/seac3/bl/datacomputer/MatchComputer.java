package bhwz.seac3.bl.datacomputer;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import bhwz.seac3.DBServiceManager;
import bhwz.seac3.dbservice.nbadata.PlayerMatchDBService;
import bhwz.seac3.dbservice.nbadata.SaveQueryDBService;
import bhwz.seac3.po.MatchPo;
import bhwz.seac3.po.PlayerCaculationDataPo;
import bhwz.seac3.po.PlayerGameDataPo;
import bhwz.seac3.po.PlayerPo;
import bhwz.seac3.po.PlayerScoreTablePo;
import bhwz.seac3.po.TeamGameDataPo;
import bhwz.seac3.po.TeamPo;
import bhwz.seac3.util.Formula;
import bhwz.seac3.vo.MatchVo;
import bhwz.seac3.vo.PlayerScoreTableVo;

public class MatchComputer {
	
	private SaveQueryDBService<TeamGameDataPo> teamService;
	private SaveQueryDBService<PlayerGameDataPo> playerService;
	private SaveQueryDBService<PlayerCaculationDataPo> playerCService;
	private SaveQueryDBService<PlayerPo> playerPoService;
	private SaveQueryDBService<TeamPo> tService;
	private PlayerMatchDBService pmdb;
	private Formula f;
	
	private String season;
	private String team1;
	private String team2;
	private int team1Score;
	private int team2Score;
	private Set<PlayerScoreTableVo> team1PlayersScore=new HashSet<>();
	private Set<PlayerScoreTableVo> team2PlayersScore=new HashSet<>();
	
	private TeamGameDataPo t_po1;
	private TeamGameDataPo t_po2;
	private PlayerGameDataPo pg_po1;
	private PlayerGameDataPo pg_po2;
	private PlayerCaculationDataPo pc_po1;
	private PlayerCaculationDataPo pc_po2;
	private PlayerPo p_po1;
	private PlayerPo p_po2;
	
	private Iterator<PlayerScoreTableVo> itor1;
	private Iterator<PlayerScoreTableVo> itor2;
	
	private boolean t1_save=false;
	private boolean t2_save=false;
	private boolean p1_save=false;
	private boolean p2_save=false;
	private boolean pc1_save=false;
	private boolean pc2_save=false;
	
	private PlayerScoreTableVo temp;

	private long 球队比赛时间;
	
	public void AppendMatchDataToPlayerAndTeam(MatchVo match){
	
		season=match.getSeason();
		team1=match.getTeam1();
		team2=match.getTeam2();
		team1Score=match.getTeam1Score();
		team2Score=match.getTeam2Score();
		team1PlayersScore=match.getTeam1PlayersScore();
		team2PlayersScore=match.getTeam2PlayersScore();
		
		teamService=DBServiceManager.getTeamGameDataDBService();
		playerService=DBServiceManager.getPlayerGameDataDBService();
		playerCService=DBServiceManager.getPlayerCaculationDataDBService();
		playerPoService=DBServiceManager.getPlayerDBService();
		pmdb=DBServiceManager.getPlayerMatchDBService();
		tService=DBServiceManager.getTeamDBService();
		f=new Formula();
		
	    t_po1=new TeamGameDataPo();
	    t_po2=new TeamGameDataPo();
		pg_po1=new PlayerGameDataPo();
		pg_po2=new PlayerGameDataPo();
		pc_po1=new PlayerCaculationDataPo();
		pc_po2=new PlayerCaculationDataPo();
		p_po1=new PlayerPo();
		p_po2=new PlayerPo();
		
		t_po1.set赛季(season);
		t_po2.set赛季(season);
		t_po1.set球队简称(team1);
		t_po2.set球队简称(team2);
		
		try {
			t_po1=teamService.queryById(t_po1);
			t_po2=teamService.queryById(t_po2);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(t_po1==null){
			t_po1=new TeamGameDataPo();
			t_po1.set赛季(season);
			t_po1.set球队简称(team1);
			t1_save=true;
		}else{
			t1_save=false;
		}
		
		if(t_po2==null){
			t_po2=new TeamGameDataPo();
			t_po2.set赛季(season);
			t_po2.set球队简称(team2);
			t2_save=true;
		}else{
			t2_save=false;
		}
		
        pg_po1.set赛季(season);
        pg_po2.set赛季(season);
        
        pc_po1.set赛季(season);
        pc_po2.set赛季(season);
        
        t_po1.set比赛得分(team1Score+t_po1.get比赛得分());
        t_po1.set对手得分(team2Score+t_po1.get对手得分());
        t_po2.set比赛得分(team2Score+t_po2.get比赛得分());
        t_po2.set对手得分(team1Score+t_po2.get对手得分());
        
        t_po1.set比赛场数(t_po1.get比赛场数()+1);
        t_po2.set比赛场数(t_po2.get比赛场数()+1);
        if(team1Score>team2Score){
        	t_po1.set胜利场数(t_po1.get胜利场数()+1);
        }else{
        	t_po2.set胜利场数(t_po2.get胜利场数()+1);
        }
        
        itor1=team1PlayersScore.iterator();
        itor2=team2PlayersScore.iterator();
        
        int 球队1投篮数=0;
   	    int 球队1罚球数=0;
   	    int 球队1失误数=0;	
     	int 球队1断球数=0;
   	    int 球队1助攻数=0;
   	    int 球队1盖帽数=0;
   	    int 球队1犯规数=0;
   	    int 球队1投篮命中数=0;
   	    int 球队1三分命中数=0;
   	    int 球队1三分出手数=0;
   	    int 球队1罚球命中数=0;
   	    int 球队1前场篮板数=0;
   	    int 球队1后场篮板数=0;
   	    int 对方1防守篮板数=0;
   	    double 球队1进攻回合=0;
   	    double 球队1防守回合=0;
   	    int 对手1进攻篮板数=0;
   	    
   	    int 球队2投篮数=0;
	    int 球队2罚球数=0;
	    int 球队2失误数=0;	
  	    int 球队2断球数=0;
	    int 球队2助攻数=0;
	    int 球队2盖帽数=0;
	    int 球队2犯规数=0;
	    int 球队2投篮命中数=0;
	    int 球队2三分命中数=0;
	    int 球队2三分出手数=0;
	    int 球队2罚球命中数=0;
	    int 球队2前场篮板数=0;
	    int 球队2后场篮板数=0;
	    int 对方2防守篮板数=0;
	    double 球队2进攻回合=0;
	    double 球队2防守回合=0;
	    int 对手2进攻篮板数=0;
        
        while(itor1.hasNext()){
        	 temp=itor1.next();
        	 球队1投篮数+=temp.get投篮出手数();
        	 球队1罚球数+=temp.get罚球出手数();
        	 球队1失误数+=temp.get失误数();	
          	 球队1断球数+=temp.get抢断数();
        	 球队1助攻数+=temp.get助攻数();
        	 球队1盖帽数+=temp.get盖帽数();
        	 球队1犯规数+=temp.get犯规数();
        	 球队1投篮命中数+=temp.get投篮命中数();
        	 球队1三分命中数+=temp.get三分命中数();
        	 球队1三分出手数+=temp.get三分出手数();
        	 球队1罚球命中数+=temp.get罚球命中数();
        	 球队1前场篮板数+=temp.get进攻篮板数();
        	 球队1后场篮板数+=temp.get防守篮板数();
        	 球队比赛时间+=temp.get在场时间();
        }
        
        while(itor2.hasNext()){
        	 temp=itor2.next();
        	 球队2投篮数+=temp.get投篮出手数();
        	 球队2罚球数+=temp.get罚球出手数();
        	 球队2失误数+=temp.get失误数();	
          	 球队2断球数+=temp.get抢断数();
        	 球队2助攻数+=temp.get助攻数();
        	 球队2盖帽数+=temp.get盖帽数();
        	 球队2犯规数+=temp.get犯规数();
        	 球队2投篮命中数+=temp.get投篮命中数();
        	 球队2三分命中数+=temp.get三分命中数();
        	 球队2三分出手数+=temp.get三分出手数();
        	 球队2罚球命中数+=temp.get罚球命中数();
        	 球队2前场篮板数+=temp.get进攻篮板数();
        	 球队2后场篮板数+=temp.get防守篮板数();
        }
		
        对手1进攻篮板数=球队2前场篮板数;
        对方1防守篮板数=球队2后场篮板数;
   	    球队1进攻回合=f.teamATKRoundNum(球队1投篮数, 球队1罚球数, 球队1前场篮板数, 对方1防守篮板数, 球队1投篮命中数, 球队1失误数);
   	    
   	    对手2进攻篮板数=球队1前场篮板数;
        对方2防守篮板数=球队1后场篮板数;
	    球队2进攻回合=f.teamATKRoundNum(球队2投篮数, 球队2罚球数, 球队2前场篮板数, 对方2防守篮板数, 球队2投篮命中数, 球队2失误数);
   	    
   	    球队1防守回合=球队2进攻回合;
   	    球队2防守回合=球队1进攻回合;
   	    
   	    t_po1.set投篮数(球队1投篮数+t_po1.get投篮数());
	    t_po1.set罚球数(球队1罚球数+t_po1.get罚球数());
	    t_po1.set失误数(球队1失误数+t_po1.get失误数());	
  	    t_po1.set断球数(球队1断球数+t_po1.get断球数());
	    t_po1.set助攻数(球队1助攻数+t_po1.get助攻数());
	    t_po1.set盖帽数(球队1盖帽数+t_po1.get盖帽数());
	    t_po1.set犯规数(球队1犯规数+t_po1.get犯规数());
	    t_po1.set投篮命中数(球队1投篮命中数+t_po1.get投篮命中数());
	    t_po1.set三分命中数(球队1三分命中数+t_po1.get三分命中数());
	    t_po1.set三分出手数(球队1三分出手数+t_po1.get三分出手数());
	    t_po1.set罚球命中数(球队1罚球命中数+t_po1.get罚球命中数());
	    t_po1.set前场篮板数(球队1前场篮板数+t_po1.get前场篮板数());
	    t_po1.set后场篮板数(球队1后场篮板数+t_po1.get后场篮板数());
	    t_po1.set对手进攻篮板数(对手1进攻篮板数+t_po1.get对手进攻篮板数());
	    t_po1.set对方防守篮板数(对方1防守篮板数+t_po1.get对方防守篮板数());
	    t_po1.set进攻回合(球队1进攻回合+t_po1.get进攻回合());
		t_po1.set防守回合(球队1防守回合+t_po1.get防守回合());
		
		t_po2.set投篮数(球队2投篮数+t_po2.get投篮数());
	    t_po2.set罚球数(球队2罚球数+t_po2.get罚球数());
	    t_po2.set失误数(球队2失误数+t_po2.get失误数());	
  	    t_po2.set断球数(球队2断球数+t_po2.get断球数());
	    t_po2.set助攻数(球队2助攻数+t_po2.get助攻数());
	    t_po2.set盖帽数(球队2盖帽数+t_po2.get盖帽数());
	    t_po2.set犯规数(球队2犯规数+t_po2.get犯规数());
	    t_po2.set投篮命中数(球队2投篮命中数+t_po2.get投篮命中数());
	    t_po2.set三分命中数(球队2三分命中数+t_po2.get三分命中数());
	    t_po2.set三分出手数(球队2三分出手数+t_po2.get三分出手数());
	    t_po2.set罚球命中数(球队2罚球命中数+t_po2.get罚球命中数());
	    t_po2.set前场篮板数(球队2前场篮板数+t_po2.get前场篮板数());
	    t_po2.set后场篮板数(球队2后场篮板数+t_po2.get后场篮板数());
	    t_po2.set对手进攻篮板数(对手2进攻篮板数+t_po2.get对手进攻篮板数());
	    t_po2.set对方防守篮板数(对方2防守篮板数+t_po2.get对方防守篮板数());
	    t_po2.set进攻回合(球队2进攻回合+t_po2.get进攻回合());
		t_po2.set防守回合(球队2防守回合+t_po2.get防守回合());
		
		if(t1_save==true){
			try {
				teamService.save(t_po1);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally{
				t1_save=false;
			}
		}else{
			try {
				teamService.updateById(t_po1);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally{
				t1_save=false;
			}
		}
		
		if(t2_save==true){
			try {
				teamService.save(t_po2);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally{
				t2_save=false;
			}
		}else{
			try {
				teamService.updateById(t_po2);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally{
				t2_save=false;
			}
		}
		
		itor1=team1PlayersScore.iterator();
		itor2=team2PlayersScore.iterator();
		
		while(itor1.hasNext()){
            temp=itor1.next();
            p_po1=new PlayerPo();
            p_po1.setName(temp.get球员名());
            try {
				p_po1=playerPoService.queryById(p_po1);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            if(p_po1==null){
            	continue;
            }

            pg_po1.set球员姓名(temp.get球员名());
            try {
				pg_po1=playerService.queryById(pg_po1);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            
            if(pg_po1==null){
            	p1_save=true;
            	pg_po1=new PlayerGameDataPo();
            	pg_po1.set赛季(season);
            	pg_po1.set球员姓名(temp.get球员名());
            }
            
            if(temp.get位置()==null||temp.get位置().equals("")){
				
			}else{
				pg_po1.set先发场数(pg_po1.get先发场数()+1);
			}
            
            pg_po1.set比赛场数(pg_po1.get比赛场数() + 1);
			pg_po1.set投篮数(pg_po1.get投篮数() + temp.get投篮出手数());
			pg_po1.set罚球数(pg_po1.get罚球数() + temp.get罚球出手数());
			pg_po1.set断球数(pg_po1.get断球数() + temp.get抢断数());
			pg_po1.set助攻数(pg_po1.get助攻数() + temp.get助攻数());
			pg_po1.set盖帽数(pg_po1.get盖帽数() + temp.get盖帽数());
			pg_po1.set犯规数(pg_po1.get犯规数() + temp.get犯规数());
			pg_po1.set比赛得分(pg_po1.get比赛得分() + temp.get个人得分());
			pg_po1.set投篮命中数(pg_po1.get投篮命中数() + temp.get投篮命中数());
			pg_po1.set三分出手数(pg_po1.get三分出手数() + temp.get三分出手数());
			pg_po1.set三分命中数(pg_po1.get三分命中数() + temp.get三分命中数());
			pg_po1.set罚球命中数(pg_po1.get罚球命中数() + temp.get罚球命中数());
			pg_po1.set前场篮板数(pg_po1.get前场篮板数() + temp.get进攻篮板数());
			pg_po1.set后场篮板数(pg_po1.get后场篮板数() + temp.get防守篮板数());
			pg_po1.set失误数(pg_po1.get失误数()+temp.get失误数());
			pg_po1.set所属球队(team1);
			pg_po1.set在场时间(pg_po1.get在场时间() + temp.get在场时间());
			TeamPo t_po1=new TeamPo();
			t_po1.set缩写(team1);
			String 联盟=null;
			try {
				联盟=tService.queryById(t_po1).get分区();
			} catch (Exception e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			pg_po1.set所属联盟(联盟);
			
            if(p1_save==true){
            	try {
					playerService.save(pg_po1);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}finally{
					p1_save=false;
				}
            }else{
            	try {
					playerService.updateById(pg_po1);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}finally{
					p1_save=false;
				}
            } 
            
            pc_po1.setName(temp.get球员名());
            
            try {
				pc_po1=playerCService.queryById(pc_po1);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            if(pc_po1==null){
            	pc1_save=true;
            	pc_po1=new PlayerCaculationDataPo();
            	pc_po1.set赛季(season);
            	pc_po1.setName(temp.get球员名());
            }
            
            double reboundpb = pc_po1.get篮板率();
			double ATKReboundpb = pc_po1.get进攻篮板率();
			double DEFReboundpb = pc_po1.get防守篮板率();
			double assistpb = pc_po1.get助攻率();
			double stealpb = pc_po1.get抢断率();
			double blockpb = pc_po1.get盖帽率();
			double turnoverpb = pc_po1.get失误率();
			double workpb = pc_po1.get使用率();
            
            double reboundp=f.playerReboundPer(temp.get进攻篮板数()+temp.get防守篮板数(),球队比赛时间 , temp.get在场时间(), 球队1前场篮板数+球队1后场篮板数, 对手1进攻篮板数+对方1防守篮板数);
            double ATKReboundp=f.playerATKReboundPer(temp.get进攻篮板数(), 球队比赛时间, temp.get在场时间(), 球队1前场篮板数, 对手1进攻篮板数);
            double DEFReboundp=f.playerDEFReboundPer(temp.get防守篮板数(), 球队比赛时间, temp.get在场时间(), 球队1后场篮板数, 对方1防守篮板数);
            double assistp=f.playerAssistPer(temp.get助攻数(), temp.get在场时间(), 球队比赛时间, 球队1投篮命中数, temp.get投篮命中数());
            double stealp=f.playerStealPer(temp.get抢断数(), temp.get在场时间(), 球队比赛时间, 球队1防守回合);
			double blockp=f.playerBlockPer(temp.get盖帽数(), temp.get在场时间(), 球队比赛时间, 球队2投篮数-球队2三分出手数);
			double turnoverp=f.playerTurnoverPer(temp.get失误数(), temp.get投篮出手数()-temp.get三分出手数(), temp.get罚球出手数());
            double workp=f.playerWorkPer(temp.get投篮出手数(), temp.get罚球出手数(), temp.get失误数(), 球队比赛时间, temp.get在场时间(), 球队1投篮数, 球队1罚球数, 球队1失误数);
            
            reboundpb=((pg_po1.get比赛场数()-1)*reboundpb+reboundp)/pg_po1.get比赛场数();
            ATKReboundpb=((pg_po1.get比赛场数()-1)*ATKReboundpb+ATKReboundp)/pg_po1.get比赛场数();
            DEFReboundpb=((pg_po1.get比赛场数()-1)*DEFReboundpb+DEFReboundp)/pg_po1.get比赛场数();
            assistpb=((pg_po1.get比赛场数()-1)*assistpb+assistp)/pg_po1.get比赛场数();
            stealpb=((pg_po1.get比赛场数()-1)*stealpb+stealp)/pg_po1.get比赛场数();
            blockpb=((pg_po1.get比赛场数()-1)*blockpb+blockp)/pg_po1.get比赛场数();
            turnoverpb=((pg_po1.get比赛场数()-1)*turnoverpb+turnoverp)/pg_po1.get比赛场数();
            workpb=((pg_po1.get比赛场数()-1)*workpb+workp)/pg_po1.get比赛场数();
            
            pc_po1.set篮板率(reboundpb);
            pc_po1.set进攻篮板率(ATKReboundpb);
            pc_po1.set防守篮板率(DEFReboundpb);
            pc_po1.set助攻率(assistpb);
            pc_po1.set抢断率(stealpb);
            pc_po1.set盖帽率(blockpb);
            pc_po1.set失误率(turnoverpb);
            pc_po1.set使用率(workpb);
            
            List<MatchPo> lastFive=null;
            try {
				lastFive=pmdb.getPlayerLatestFiveMatches(temp.get球员名(), season);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
            
            if(lastFive==null||lastFive.size()!=5){
            	pc_po1.set助攻近五场提升率(0);
            	pc_po1.set得分近五场提升率(0);
            	pc_po1.set篮板近五场提升率(0);
            }else{
            	int score=pg_po1.get比赛得分();
            	int assist=pg_po1.get助攻数();
            	int rebound=pg_po1.get前场篮板数()+pg_po1.get后场篮板数();
            	int matches=pg_po1.get比赛场数();
            	int l_score=0;
            	int l_assist=0;
            	int l_rebound=0;
                Iterator<MatchPo> itor=lastFive.iterator();
            	while(itor.hasNext()){
            		Set<PlayerScoreTablePo> table=itor.next().getTeam1PlayersScore();
            		Iterator<PlayerScoreTablePo> p_itor=table.iterator();
            		while(p_itor.hasNext()){
            			PlayerScoreTablePo player=p_itor.next();
            			if(player.get球员名().equals(temp.get球员名())){
            				l_score+=player.get个人得分();
            				l_assist+=player.get助攻数();
            				l_rebound+=player.get总篮板数();
            			}else{
            				continue;
            			}
            		}
                    double a_l_score=l_score/5.0;
                    double a_l_assist=l_assist/5.0;
                    double a_l_rebound=l_rebound/5.0;
                    double a_score=(score-l_score)*1.0/(matches-5);
                    double a_assist=(assist-l_assist)*1.0/(matches-5);
                    double a_rebound=(rebound-l_rebound)*1.0/(matches-5);
                    
                    if(matches==5){
                    	a_score=0;
                    	a_assist=0;
                    	a_rebound=0;
                    }
                    
                    pc_po1.set得分近五场提升率(f.playerProgress(a_l_score, a_score));
            		pc_po1.set助攻近五场提升率(f.playerProgress(a_l_assist, a_assist));
            		pc_po1.set篮板近五场提升率(f.playerProgress(a_l_rebound, a_rebound));
            	}
            	
            }
            
            
            
            if(pc1_save==true){
            	try {
					playerCService.save(pc_po1);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}finally{
					pc1_save=false;
				}
            }else{
                try {
					playerCService.updateById(pc_po1);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}finally{
					pc1_save=false;
				}           	
            }
		}
		
		
		while(itor2.hasNext()){
            temp=itor2.next();
            p_po2=new PlayerPo();
            p_po2.setName(temp.get球员名());
            try {
				p_po2=playerPoService.queryById(p_po2);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            if(p_po2==null){
            	continue;
            }

            pg_po2.set球员姓名(temp.get球员名());
            try {
				pg_po2=playerService.queryById(pg_po2);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            
            if(pg_po2==null){
            	p2_save=true;
            	pg_po2=new PlayerGameDataPo();
            	pg_po2.set赛季(season);
            	pg_po2.set球员姓名(temp.get球员名());
            }
            
            if(temp.get位置()==null||temp.get位置().equals("")){
				
			}else{
				pg_po2.set先发场数(pg_po2.get先发场数()+1);
			}
            
            pg_po2.set比赛场数(pg_po2.get比赛场数() + 1);
			pg_po2.set投篮数(pg_po2.get投篮数() + temp.get投篮出手数());
			pg_po2.set罚球数(pg_po2.get罚球数() + temp.get罚球出手数());
			pg_po2.set断球数(pg_po2.get断球数() + temp.get抢断数());
			pg_po2.set助攻数(pg_po2.get助攻数() + temp.get助攻数());
			pg_po2.set盖帽数(pg_po2.get盖帽数() + temp.get盖帽数());
			pg_po2.set犯规数(pg_po2.get犯规数() + temp.get犯规数());
			pg_po2.set比赛得分(pg_po2.get比赛得分() + temp.get个人得分());
			pg_po2.set投篮命中数(pg_po2.get投篮命中数() + temp.get投篮命中数());
			pg_po2.set三分出手数(pg_po2.get三分出手数() + temp.get三分出手数());
			pg_po2.set三分命中数(pg_po2.get三分命中数() + temp.get三分命中数());
			pg_po2.set罚球命中数(pg_po2.get罚球命中数() + temp.get罚球命中数());
			pg_po2.set前场篮板数(pg_po2.get前场篮板数() + temp.get进攻篮板数());
			pg_po2.set后场篮板数(pg_po2.get后场篮板数() + temp.get防守篮板数());
			pg_po2.set失误数(pg_po2.get失误数()+temp.get失误数());
			pg_po2.set所属球队(team2);
			pg_po2.set在场时间(pg_po2.get在场时间() + temp.get在场时间());
			TeamPo t_po2=new TeamPo();
			t_po2.set缩写(team1);
			String 联盟=null;
			try {
				联盟=tService.queryById(t_po2).get分区();
			} catch (Exception e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			pg_po2.set所属联盟(联盟);
			
            if(p2_save==true){
            	try {
					playerService.save(pg_po2);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}finally{
					p2_save=false;
				}
            }else{
            	try {
					playerService.updateById(pg_po2);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}finally{
					p2_save=false;
				}
            } 
            
            pc_po2.setName(temp.get球员名());
            
            try {
				pc_po2=playerCService.queryById(pc_po2);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            if(pc_po2==null){
            	pc2_save=true;
            	pc_po2=new PlayerCaculationDataPo();
            	pc_po2.set赛季(season);
            	pc_po2.setName(temp.get球员名());
            }
            
            double reboundpb = pc_po2.get篮板率();
			double ATKReboundpb = pc_po2.get进攻篮板率();
			double DEFReboundpb = pc_po2.get防守篮板率();
			double assistpb = pc_po2.get助攻率();
			double stealpb = pc_po2.get抢断率();
			double blockpb = pc_po2.get盖帽率();
			double turnoverpb = pc_po2.get失误率();
			double workpb = pc_po2.get使用率();
            
            double reboundp=f.playerReboundPer(temp.get进攻篮板数()+temp.get防守篮板数(),球队比赛时间 , temp.get在场时间(), 球队2前场篮板数+球队2后场篮板数, 对手2进攻篮板数+对方2防守篮板数);
            double ATKReboundp=f.playerATKReboundPer(temp.get进攻篮板数(), 球队比赛时间, temp.get在场时间(), 球队2前场篮板数, 对手2进攻篮板数);
            double DEFReboundp=f.playerDEFReboundPer(temp.get防守篮板数(), 球队比赛时间, temp.get在场时间(), 球队2后场篮板数, 对方2防守篮板数);
            double assistp=f.playerAssistPer(temp.get助攻数(), temp.get在场时间(), 球队比赛时间, 球队2投篮命中数, temp.get投篮命中数());
            double stealp=f.playerStealPer(temp.get抢断数(), temp.get在场时间(), 球队比赛时间, 球队2防守回合);
			double blockp=f.playerBlockPer(temp.get盖帽数(), temp.get在场时间(), 球队比赛时间, 球队1投篮数-球队1三分出手数);
			double turnoverp=f.playerTurnoverPer(temp.get失误数(), temp.get投篮出手数()-temp.get三分出手数(), temp.get罚球出手数());
            double workp=f.playerWorkPer(temp.get投篮出手数(), temp.get罚球出手数(), temp.get失误数(), 球队比赛时间, temp.get在场时间(), 球队2投篮数, 球队2罚球数, 球队2失误数);
            
            reboundpb=((pg_po2.get比赛场数()-1)*reboundpb+reboundp)/pg_po2.get比赛场数();
            ATKReboundpb=((pg_po2.get比赛场数()-1)*ATKReboundpb+ATKReboundp)/pg_po2.get比赛场数();
            DEFReboundpb=((pg_po2.get比赛场数()-1)*DEFReboundpb+DEFReboundp)/pg_po2.get比赛场数();
            assistpb=((pg_po2.get比赛场数()-1)*assistpb+assistp)/pg_po2.get比赛场数();
            stealpb=((pg_po2.get比赛场数()-1)*stealpb+stealp)/pg_po2.get比赛场数();
            blockpb=((pg_po2.get比赛场数()-1)*blockpb+blockp)/pg_po2.get比赛场数();
            turnoverpb=((pg_po2.get比赛场数()-1)*turnoverpb+turnoverp)/pg_po2.get比赛场数();
            workpb=((pg_po2.get比赛场数()-1)*workpb+workp)/pg_po2.get比赛场数();
            
            pc_po2.set篮板率(reboundpb);
            pc_po2.set进攻篮板率(ATKReboundpb);
            pc_po2.set防守篮板率(DEFReboundpb);
            pc_po2.set助攻率(assistpb);
            pc_po2.set抢断率(stealpb);
            pc_po2.set盖帽率(blockpb);
            pc_po2.set失误率(turnoverpb);
            pc_po2.set使用率(workpb);
            
            List<MatchPo> lastFive=null;
            try {
				lastFive=pmdb.getPlayerLatestFiveMatches(temp.get球员名(), season);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
            
            if(lastFive==null||lastFive.size()!=5){
            	pc_po2.set助攻近五场提升率(0);
            	pc_po2.set得分近五场提升率(0);
            	pc_po2.set篮板近五场提升率(0);
            }else{
            	int score=pg_po2.get比赛得分();
            	int assist=pg_po2.get助攻数();
            	int rebound=pg_po2.get前场篮板数()+pg_po2.get后场篮板数();
            	int matches=pg_po2.get比赛场数();
            	int l_score=0;
            	int l_assist=0;
            	int l_rebound=0;
                Iterator<MatchPo> itor=lastFive.iterator();
            	while(itor.hasNext()){
            		Set<PlayerScoreTablePo> table=itor.next().getTeam2PlayersScore();
            		Iterator<PlayerScoreTablePo> p_itor=table.iterator();
            		while(p_itor.hasNext()){
            			PlayerScoreTablePo player=p_itor.next();
            			if(player.get球员名().equals(temp.get球员名())){
            				l_score+=player.get个人得分();
            				l_assist+=player.get助攻数();
            				l_rebound+=player.get总篮板数();
            			}else{
            				continue;
            			}
            		}
                    double a_l_score=l_score/5.0;
                    double a_l_assist=l_assist/5.0;
                    double a_l_rebound=l_rebound/5.0;
                    double a_score=(score-l_score)*1.0/(matches-5);
                    double a_assist=(assist-l_assist)*1.0/(matches-5);
                    double a_rebound=(rebound-l_rebound)*1.0/(matches-5);
                    
                    if(matches==5){
                    	a_score=0;
                    	a_assist=0;
                    	a_rebound=0;
                    }
                    
                    pc_po2.set得分近五场提升率(f.playerProgress(a_l_score, a_score));
            		pc_po2.set助攻近五场提升率(f.playerProgress(a_l_assist, a_assist));
            		pc_po2.set篮板近五场提升率(f.playerProgress(a_l_rebound, a_rebound));
            	}
            	
            }
            
            
            if(pc2_save==true){
            	try {
					playerCService.save(pc_po2);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}finally{
					pc2_save=false;
				}
            }else{
                try {
					playerCService.updateById(pc_po2);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}finally{
					pc2_save=false;
				}           	
            }
		}
		
		
		
	    
	}
	
	

}
