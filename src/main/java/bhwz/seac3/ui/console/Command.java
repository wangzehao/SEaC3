package bhwz.seac3.ui.console;

import java.util.*;

public class Command {
	protected List<String> cmd;

	public Command(List<String> cmd) {
		this.cmd = cmd;
	}

	public String getValue(String key) {
		int index = cmd.indexOf(key);
		if (index + 1 < cmd.size()) {
			String next = cmd.get(index + 1);
			if (next.startsWith("-"))
				return "";
			else
				return next;
		} else {
			return "";
		}
	}

	public boolean hasKey(String key) {
		return cmd.contains(key);
	}
}
