package bhwz.seac3.ui.user;

import java.awt.Font;
import java.awt.Graphics;
import javax.swing.*;

/**
 * 作者 :byc
 * 创建时间：2015年6月8日 下午9:03:03
 * 类说明:
 */
public class AnalyseChoicePanel extends JPanel{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3275872773982050267L;
	public AnalyseChoicePanel(){
		this.setLayout(null);
		JLabel lblVs = new JLabel("VS");
		lblVs.setFont(new Font("微软雅黑", Font.BOLD, 30));
		lblVs.setBounds(352, 26, 52, 30);
		this.add(lblVs);
		
		JLabel label = new JLabel("主场");
		label.setFont(new Font("微软雅黑", Font.PLAIN, 27));
		label.setBounds(10, 26, 54, 30);
		this.add(label);
		
		JLabel label_1 = new JLabel("客场");
		label_1.setFont(new Font("微软雅黑", Font.PLAIN, 27));
		label_1.setBounds(686, 26, 54, 30);
		this.add(label_1);
		
		JComboBox<String> comboBox = new JComboBox<String>();
		comboBox.setBounds(99, 26, 216, 35);
		comboBox.setFont(new Font("微软雅黑", Font.PLAIN, 20));
		comboBox.setModel(new DefaultComboBoxModel<String>(new String[] {"ATL","CHA","MIA","ORL","WAS","CHI","CLE","IND","DET","MIL","BOS","NYK","PHI","BKN","TOR"}));
		this.add(comboBox);
		this.repaint();
		
		JComboBox<String> comboBox_1 = new JComboBox<String>();
		comboBox_1.setFont(new Font("微软雅黑", Font.PLAIN, 20));
		comboBox_1.setModel(new DefaultComboBoxModel<String>(new String[] {"ATL","CHA","MIA","ORL","WAS","CHI","CLE","IND","DET","MIL","BOS","NYK","PHI","BKN","TOR"}));
		comboBox_1.setBounds(424, 26, 216, 35);
		comboBox_1.setSelectedIndex(1);
		this.add(comboBox_1);
	}
	 @Override
	public void paintComponent(Graphics g)
     {
        java.net.URL imgURL=getClass().getResource("/picture/back3.png");
        ImageIcon icon=new ImageIcon(imgURL);
        g.drawImage(icon.getImage(),0,0,this.getWidth(),this.getHeight(),this);
     }
}
