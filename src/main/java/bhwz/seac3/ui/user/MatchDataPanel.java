package bhwz.seac3.ui.user;

import java.awt.Font;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import bhwz.seac3.LocalBLServiceManager;
import bhwz.seac3.RMIClientBLServiceManager;
import bhwz.seac3.blservice.AutoRedisplay;
import bhwz.seac3.blservice.query.MatchQueryBLService;
import bhwz.seac3.util.SortableTable;
import bhwz.seac3.util.SortableTableModel;
import bhwz.seac3.vo.MatchVo;

import java.awt.BorderLayout;

public class MatchDataPanel extends JPanel implements AutoRedisplay{

	private static final long serialVersionUID = 4510250461136407446L;
	private SortableTable table;
	private Object[][] info;
	private SeasonSelectComboBox timeComboBox;
	private final Object columns[] = { "比赛时间", "比赛队伍", "比分", "小节比分" };
	private JScrollPane tablePane;
	private DateFilterPanel dateFilter;

	public MatchDataPanel() {
		super();
		setLayout(new BorderLayout(0, 0));

		JPanel panel = new JPanel();
		add(panel, BorderLayout.NORTH);
		JLabel playerRankLabel = new JLabel("比赛数据");
		panel.add(playerRankLabel);
		playerRankLabel.setFont(new Font("华文楷体", Font.BOLD, 18));

		timeComboBox = new SeasonSelectComboBox();
		panel.add(timeComboBox);
		timeComboBox.setFont(new Font("微软雅黑", Font.PLAIN, 15));
		timeComboBox.addActionListener(e->redisplay());

		JLabel timeLabel = new JLabel("赛季");
		panel.add(timeLabel);
		timeLabel.setFont(new Font("华文楷体", Font.BOLD, 17));
		
		dateFilter = new DateFilterPanel();
		dateFilter.addActionListener(e->redisplay());
		panel.add(dateFilter);
		
		try {
			info = matchToArray(((MatchQueryBLService) RMIClientBLServiceManager
					.getBLService(MatchQueryBLService.class))
					.queryAllMatches(timeComboBox.getSeason()));
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, "服务器连接发生异常\n" + e.getMessage());
			e.printStackTrace();
		}
		tablePane = new JScrollPane(table);
		tablePane.setBounds(10, 50, 745, 320);
		this.add(tablePane);
		
		display数据(timeComboBox.getSeason());
		
		LocalBLServiceManager.getRedisplayControllerBLService().register(this);
	}

	public void display数据(String season) {
		try {
			List<MatchVo> matches=((MatchQueryBLService) RMIClientBLServiceManager
					.getBLService(MatchQueryBLService.class))
					.queryAllMatches(season);
			List<MatchVo> filtedMatches=matches.stream().filter(match->dateFilter.filte(match.getDate())).collect(Collectors.toList());
			info=matchToArray(filtedMatches);
			table = new SortableTable(new SortableTableModel(info, columns));
			table.addDoubleClickOnDataListener(DetailsListenerFactory.getMatchDetailListener(timeComboBox.getSeason()),0,1);
			tablePane.setViewportView(table);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, "服务器连接发生异常\n" + e.getMessage());
			e.printStackTrace();
		}
	}

	private Object[][] matchToArray(List<MatchVo> matches) {
		int length = matches.size();
		Object[][] info = new Object[length][4];
		for (int n = 0; n < length; n++) {
			MatchVo match = matches.get(n);
			info[n][0] = new SimpleDateFormat("yyyy-MM-dd").format(new Date(
					match.getDate()));
			info[n][1] = match.getTeam1() + "-" + match.getTeam2();
			info[n][2] = match.getTeam1Score() + ":" + match.getTeam2Score();
			StringBuffer sectionScore = new StringBuffer("");
			match.getSectionScores().forEach(s -> {
				sectionScore.append(s + ";");
			});
			info[n][3] = sectionScore;
		}
		return info;

	}

	@Override
	public void redisplay() {
		display数据(timeComboBox.getSeason());
	}
}