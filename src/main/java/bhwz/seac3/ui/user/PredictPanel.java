package bhwz.seac3.ui.user;

import javax.swing.JPanel;

import java.awt.BorderLayout;
import java.awt.Graphics;

import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import bhwz.seac3.RMIClientBLServiceManager;
import bhwz.seac3.blservice.predict.PredictBLService;
import bhwz.seac3.vo.PredictVo;
import bhwz.seac3.vo.TeamVo;

import javax.swing.BoxLayout;

public class PredictPanel extends JPanel{
	private String team1;
	private String team2;
	private static final long serialVersionUID = 1L;
	private JPanel panel;
	
	public PredictPanel(String name1,String name2) {
		team1=name1;
		team2=name2;
		setLayout(new BorderLayout(0, 0));
		
		JPanel bar_1 = new JPanel();
		add(bar_1, BorderLayout.NORTH);
		
		JLabel label = new JLabel("\u7403\u961F1\uFF1A");
		bar_1.add(label);
		
		/*JComboBox<String> comboBox = new TeamSelectComboBox();
		comboBox.addActionListener(e->team1=(String) comboBox.getSelectedItem());
		bar_1.add(comboBox);
		
		JLabel label_1 = new JLabel("\u7403\u961F2\uFF1A");
		bar_1.add(label_1);
		
		JComboBox<String> comboBox_1 = new TeamSelectComboBox();
		comboBox_1.addActionListener(e->team2=(String) comboBox_1.getSelectedItem());
		bar_1.add(comboBox_1);
		
		JButton doPredict = new JButton("\u9884\u6D4B");
		doPredict.addActionListener(e->predict());
		bar_1.add(doPredict);*/
		
		panel = new JPanel();
		add(panel, BorderLayout.CENTER);
		panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
	}
	
	void predict(){
		panel.removeAll();
		PredictBLService bl=RMIClientBLServiceManager.getBLService(PredictBLService.class);
		TeamVo t1id=new TeamVo();
		t1id.set球队简称(team1);
		TeamVo t2id=new TeamVo();
		t2id.set球队简称(team2);
		try {
			PredictVo vo = bl.predict(t1id, t2id);
			PredictSubteamPanel sub1=new PredictSubteamPanel(vo.team1){
				private static final long serialVersionUID = 1L;

				@Override
				public void paint(Graphics g) {
					this.setBounds(0, 0, panel.getWidth()/2, panel.getHeight());
					super.paint(g);
				}
			};
			PredictSubteamPanel sub2=new PredictSubteamPanel(vo.team2){
				private static final long serialVersionUID = 1L;

				@Override
				public void paint(Graphics g) {
					this.setBounds(panel.getWidth()/2, 0, panel.getWidth()/2, panel.getHeight());
					super.paint(g);
				}
			};
			panel.add(sub1);
			panel.add(sub2);
			sub1.predict(sub2);
			sub2.predict(sub1);
		} catch (Exception e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null,"比赛场数不足，无法显示");
		}
		
	}

}
