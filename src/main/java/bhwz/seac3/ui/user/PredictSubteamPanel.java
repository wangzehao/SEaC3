package bhwz.seac3.ui.user;

import javax.swing.JPanel;

import bhwz.seac3.ui.user.graphics.Graphics;
import bhwz.seac3.vo.PredictItemVo;

import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JLabel;

public class PredictSubteamPanel extends JPanel{
	private static final long serialVersionUID = 1L;
	private JPanel chartPanel;
	private double exp;
	private double var;
	List<Double> chartData;
	private JPanel chart;
	private JLabel bar;
	private JLabel underBar;

	public PredictSubteamPanel(PredictItemVo vo) {
		setLayout(new BorderLayout(0, 0));
		
		chartPanel = new JPanel();
		add(chartPanel, BorderLayout.CENTER);
		chartPanel.setLayout(new BorderLayout(0, 0));

		bar = new JLabel("期望:"+exp+"\t方差"+var);
		chartPanel.add(bar, BorderLayout.NORTH);
		
		underBar = new JLabel();
		chartPanel.add(underBar,BorderLayout.SOUTH);
		display(vo);
	}
	
	@SuppressWarnings("unchecked")
	public void display(PredictItemVo vo){
		chartData=vo.得分率;
		Object[] result=Graphics.getHistogram(vo.得分率);
		if(chart!=null&&result[0]!=null&&result[1]!=null)
		chartPanel.remove(chart);
		chart = (JPanel) result[0];
		exp=Double.parseDouble(((List<String>)result[1]).get(0));
		var=Double.parseDouble(((List<String>)result[1]).get(1));
		chartPanel.add(chart,BorderLayout.CENTER);
		bar.setText("期望:"+exp+"\t方差"+var);
		
		}

	List<Double> get1List(int size){
		ArrayList<Double> result=new ArrayList<Double>();
		for(int i=0;i<size;i++){
			result.add(1.0);
		}
		return result;
	}
	
	public void predict(PredictSubteamPanel p2){
		List<String> l=Graphics.exec("python pysrc/Predict.py "+exp+" "+var+" "+p2.exp+" "+p2.var);
		underBar.setText("胜率:"+l.get(0));
		winRate=Double.parseDouble(l.get(0));
	}
	double winRate;
	public double getWinRate(){
		return winRate;
	}
}
