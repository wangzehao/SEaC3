package bhwz.seac3.ui.user;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;

import bhwz.seac3.RMIClientBLServiceManager;
import bhwz.seac3.blservice.query.HotspotQueryBLService;
import bhwz.seac3.util.ChooseButton;
import bhwz.seac3.vo.TeamVo;

/**
 * 作者 :byc
 * 创建时间：2015年4月2日 下午7:15:34
 * 类说明:容纳热点筛选按钮的容器
 */
public class TeamHotspotChoicePanel extends JPanel{
	private String 筛选条件;
	private int i;
	private int buttonNumber;
	private List<TeamVo> list;
	private static final long serialVersionUID = -5104977669487284998L;
	public TeamHotspotChoicePanel(TeamHotspotPanel panel){
		this.setSize(752, 35);
		this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		this.setBackground(Color.GRAY);
		String [] buttonName={"场均得分","场均篮板","场均助攻","场均盖帽","场均抢断","三分%","投篮%","罚球%"};
		buttonNumber=buttonName.length;
		ChooseButton [] chooseButton=new ChooseButton[buttonNumber];
		for(i=0;i<buttonNumber;i++){
			chooseButton[i]=new ChooseButton(buttonName[i]);
			chooseButton[i].addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					筛选条件=((JButton) (arg0.getSource())).getText();
					HotspotQueryBLService hq=(HotspotQueryBLService)RMIClientBLServiceManager.getBLService(HotspotQueryBLService.class);
					try {
						list=hq.QueryTeamsRankedBy条件(筛选条件.replaceAll(" ",""));
					} catch (Exception e) {
						e.printStackTrace();
					}
					panel.showInfo(list);
				}
			});
			this.add(chooseButton[i]);	
			this.repaint();
		}
		
	}
	
	public List<TeamVo> getRankedInfo(){
		return list;
	}
	
	public void setRankedInfo(String 条件) {
		 筛选条件=条件;
		 HotspotQueryBLService
		 hq=(HotspotQueryBLService)RMIClientBLServiceManager
		 .getBLService(HotspotQueryBLService.class);
		 try {
			list=hq.QueryTeamsRankedBy条件(筛选条件.replaceAll(" ",""));
		} catch (Exception e) {
			e.printStackTrace();
		}
	
	}
}
