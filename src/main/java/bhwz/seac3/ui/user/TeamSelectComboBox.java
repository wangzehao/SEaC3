package bhwz.seac3.ui.user;

import java.util.HashSet;
import java.util.Set;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;

import bhwz.seac3.RMIClientBLServiceManager;
import bhwz.seac3.blservice.query.SeasonQueryBLService;
import bhwz.seac3.blservice.query.TeamQueryBLService;

public class TeamSelectComboBox extends JComboBox<String> {

	private static final long serialVersionUID = 374955416341068014L;

	public TeamSelectComboBox() {
		super();
		TeamQueryBLService tbl = RMIClientBLServiceManager
				.getBLService(TeamQueryBLService.class);
		SeasonQueryBLService sbl = RMIClientBLServiceManager
				.getBLService(SeasonQueryBLService.class);
		Set<String> allTeams = new HashSet<String>();
		try {
			sbl.queryAllSeasons().forEach(
					season -> {
						try {
							tbl.query赛季总数据(season).forEach(
									teamVo -> allTeams.add(teamVo.get球队简称()));
						} catch (Exception e) {
							e.printStackTrace();
						}
					});
		} catch (Exception e) {
			e.printStackTrace();
		}
		this.setModel(new DefaultComboBoxModel<String>(allTeams.toArray(new String[0])));
	}

}
