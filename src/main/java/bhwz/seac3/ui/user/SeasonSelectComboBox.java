package bhwz.seac3.ui.user;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;

import bhwz.seac3.LocalBLServiceManager;
import bhwz.seac3.RMIClientBLServiceManager;
import bhwz.seac3.blservice.AutoRedisplay;
import bhwz.seac3.blservice.ReDisplayControllerBLService;
import bhwz.seac3.blservice.query.SeasonQueryBLService;

public class SeasonSelectComboBox extends JComboBox<String> implements AutoRedisplay{
	
	private static final long serialVersionUID = -5141427189730626483L;
	private static String[] seasons;
	private static SeasonQueryBLService sqbl;
	private static ReDisplayControllerBLService rdbl;
	
	private void updateSeasons(){		
		if(sqbl==null)
			sqbl=RMIClientBLServiceManager.getBLService(SeasonQueryBLService.class);
		try {
			seasons=sqbl.queryAllSeasons().toArray(new String[0]);
			this.setModel(new DefaultComboBoxModel<String>(seasons));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public SeasonSelectComboBox() {
		super();
		updateSeasons();
		rdbl=LocalBLServiceManager.getRedisplayControllerBLService();
		rdbl.register(this);
	}
	
	public String getSeason(){
		return (String) this.getSelectedItem();
	}

	@Override
	public void redisplay() {
		updateSeasons();
	}
}
