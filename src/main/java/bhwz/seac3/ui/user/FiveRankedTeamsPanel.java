package bhwz.seac3.ui.user;

import java.util.List;
import javax.swing.BoxLayout;
import javax.swing.JPanel;

import bhwz.seac3.vo.TeamVo;

/**
 * 作者 :byc 创建时间：2015年4月2日 上午10:36:35 类说明:装载前五数据信息的容器
 */
public class FiveRankedTeamsPanel extends JPanel {
	private TeamVo team;
	private String info;
	private static final long serialVersionUID = -4386915775724143165L;

	public FiveRankedTeamsPanel(List<TeamVo> data) {
		this.setSize(700, 285);
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.setVisible(true);
		TeamRankInfoPanel[] teams = new TeamRankInfoPanel[5];
		if (data != null) {
			for (int i = 0; i < teams.length; i++) {
				team = data.get(i);
				info = team.get比赛信息() + "";
				teams[i] = new TeamRankInfoPanel(i, team, info);
				this.add(teams[i]);
			}
		}
		this.repaint();

	}

}
