package bhwz.seac3.ui.user;

import java.awt.Font;
import java.awt.Graphics;
import javax.swing.*;

/**
 * 作者 :byc
 * 创建时间：2015年6月9日 下午3:05:23
 * 类说明:
 */
public class AnalyseResultPanel extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7613440880129193966L;
	private double rate1;
	private double rate2;
	public AnalyseResultPanel(){
	}
	
	public AnalyseResultPanel(String name1,String name2){
		this.setLayout(null);
		
		JLabel team1Label = new JLabel(name1);
		team1Label.setFont(new Font("微软雅黑", Font.BOLD, 27));
		team1Label.setBounds(80, 10, 82, 34);
		this.add(team1Label);
		
		JLabel team2Label = new JLabel(name2);
		team2Label.setFont(new Font("微软雅黑", Font.BOLD, 27));
		team2Label.setBounds(630, 10, 82, 34);
		this.add(team2Label);
		
		AnalyseDetailPanel adp=new AnalyseDetailPanel(name1,name2);
		adp.setLocation(10,40);
		rate1=adp.getRate1();
		rate2=adp.getRate2();
		this.add(adp);
		
		WinRatePanel wrp =new WinRatePanel(rate1,rate2);
		wrp.setLocation((750-wrp.getWidth())/2,10);
		this.add(wrp);
		this.repaint();
	}
	@Override
	public void paintComponent(Graphics g)
    {
       java.net.URL imgURL=getClass().getResource("/picture/back3.png");
       ImageIcon icon=new ImageIcon(imgURL);
       g.drawImage(icon.getImage(),0,0,800,500,this);
    }
}
