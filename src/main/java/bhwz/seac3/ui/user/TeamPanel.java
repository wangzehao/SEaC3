package bhwz.seac3.ui.user;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.SystemColor;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.JComboBox;
import java.awt.BorderLayout;

/**
 * 作者 :byc 
 * 创建时间：2015年4月19日 下午7:05:58 
 * 类说明:
 */
public class TeamPanel extends JPanel {
	private static final long serialVersionUID = 5926749438241201723L;
	private JScrollPane scrollPane;
	private JPanel eastpanel;
	private JPanel westpanel;
	private SeasonSelectComboBox season;
	public TeamPanel() {
		eastpanel=new JPanel();
		eastpanel.setPreferredSize(new Dimension(780, 1300));
		eastpanel.setLayout(null);
		eastpanel.setBackground(Color.WHITE);
		

		westpanel=new JPanel();
		westpanel.setPreferredSize(new Dimension(780, 1300));
		westpanel.setLayout(null);
		westpanel.setBackground(Color.WHITE);
		
		setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		add(panel,BorderLayout.NORTH);
		
		JLabel label_6 = new JLabel("球队数据");
		label_6.setFont(new Font("华文楷体", Font.BOLD, 18));
		panel.add(label_6);
		
		season = new SeasonSelectComboBox();
		season.setFont(new Font("微软雅黑", Font.PLAIN, 15));
		panel.add(season);
		
		JLabel label_7 = new JLabel("\u8D5B\u5B63");
		label_7.setFont(new Font("华文楷体", Font.BOLD, 18));
		panel.add(label_7);
		
		String[] areas=new String[]{"东部","西部"};
		JComboBox<String> areaComboBox = new JComboBox<String>(areas);
		areaComboBox.setFont(new Font("微软雅黑", Font.PLAIN, 15));
		
		areaComboBox.addActionListener(e->{
			if(areaComboBox.getSelectedItem().equals("东部")){
				scrollPane.setViewportView(eastpanel);
			}else{
				scrollPane.setViewportView(westpanel);
			}
		});
		panel.add(areaComboBox);
		
		JLabel label_8 = new JLabel("\u8054\u76DF");
		label_8.setFont(new Font("华文楷体", Font.BOLD, 18));
		panel.add(label_8);
		scrollPane = new JScrollPane();
		scrollPane.setViewportView(eastpanel);
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		this.add(scrollPane,BorderLayout.CENTER);
		
		JPanel titlePanel = new JPanel();
		titlePanel.setBounds(0, 5, 492, 44);
		titlePanel.setBackground(SystemColor.textHighlight);
		titlePanel.setLayout(null);
		eastpanel.add(titlePanel);
		
		JLabel eastLabel = new JLabel("东部联盟");
		eastLabel.setForeground(Color.WHITE);
		eastLabel.setFont(new Font("华文楷体", Font.BOLD, 20));
		eastLabel.setBounds(61, 3, 96, 38);
		titlePanel.add(eastLabel);
		
		JLabel eastLogoLabel = new JLabel("");
		eastLogoLabel.setIcon(new ImageIcon(TeamPanel.class.getResource("/picture/eastlogo.jpg")));
		eastLogoLabel.setBounds(0, 1, 41, 41);
		titlePanel.add(eastLogoLabel);
		
		JPanel areaPanel = new JPanel();
		areaPanel.setBounds(0, 49, 492, 29);
		areaPanel.setBackground(SystemColor.controlHighlight);
		eastpanel.add(areaPanel);
		areaPanel.setLayout(new BoxLayout(areaPanel, BoxLayout.X_AXIS));
		
		JLabel label = new JLabel("         东南分区         ");
		label.setFont(new Font("华文楷体", Font.BOLD, 16));
		areaPanel.add(label);
		
		JLabel label_1 = new JLabel("         中央分区         ");
		label_1.setFont(new Font("华文楷体", Font.BOLD, 16));
		areaPanel.add(label_1);
		
		JLabel label_2 = new JLabel("         大西洋分区         ");
		label_2.setFont(new Font("华文楷体", Font.BOLD, 16));
		areaPanel.add(label_2);
		
		JPanel teamPanel1=new JPanel();
		teamPanel1.setBounds(0, 78, 492, 550);
		teamPanel1.setLayout(new BoxLayout(teamPanel1, BoxLayout.X_AXIS));
		teamPanel1.setBackground(Color.WHITE);
		eastpanel.add(teamPanel1);
		
		TeamDividedByAreaPanel t1=new TeamDividedByAreaPanel("东南分区",season.getSeason());
		TeamDividedByAreaPanel t2=new TeamDividedByAreaPanel("中央分区",season.getSeason());
		TeamDividedByAreaPanel t3=new TeamDividedByAreaPanel("大西洋分区",season.getSeason());
		teamPanel1.add(t1);
		teamPanel1.add(t2);
		teamPanel1.add(t3);
		
		JPanel titlePanel2 = new JPanel();
		titlePanel2.setBounds(0, 5, 492, 44);
		titlePanel2.setBackground(SystemColor.textHighlight);
		titlePanel2.setLayout(null);
		westpanel.add(titlePanel2);
		
		JLabel westLabel = new JLabel("西部联盟");
		westLabel.setForeground(Color.WHITE);
		westLabel.setFont(new Font("华文楷体", Font.BOLD, 20));
		westLabel.setBounds(61, 3, 96, 38);
		titlePanel2.add(westLabel);
		
		JLabel westLogoLabel = new JLabel("");
		westLogoLabel.setIcon(new ImageIcon(TeamPanel.class.getResource("/picture/westlogo.jpg")));
		westLogoLabel.setBounds(0, 1, 41, 41);
		titlePanel2.add(westLogoLabel);
		
		JPanel areaPanel2 = new JPanel();
		areaPanel2.setBounds(0, 49, 492, 29);
		areaPanel2.setBackground(SystemColor.controlHighlight);
		westpanel.add(areaPanel2);
		areaPanel2.setLayout(new BoxLayout(areaPanel2, BoxLayout.X_AXIS));
		
		JLabel label_3 = new JLabel("         太平洋分区         ");
		label_3.setFont(new Font("华文楷体", Font.BOLD, 16));
		areaPanel2.add(label_3);
		
		JLabel label_4 = new JLabel("         西北分区         ");
		label_4.setFont(new Font("华文楷体", Font.BOLD, 16));
		areaPanel2.add(label_4);
		
		JLabel label_5 = new JLabel("         西南分区         ");
		label_5.setFont(new Font("华文楷体", Font.BOLD, 16));
		areaPanel2.add(label_5);
		
		JPanel teamPanel2=new JPanel();
		teamPanel2.setBounds(0, 78, 492, 550);
		teamPanel2.setLayout(new BoxLayout(teamPanel2, BoxLayout.X_AXIS));
		teamPanel2.setBackground(Color.WHITE);
		westpanel.add(teamPanel2);
		
		TeamDividedByAreaPanel t4=new TeamDividedByAreaPanel("太平洋分区",season.getSeason());
		TeamDividedByAreaPanel t5=new TeamDividedByAreaPanel("西北分区",season.getSeason());
		TeamDividedByAreaPanel t6=new TeamDividedByAreaPanel("西南分区",season.getSeason());
		teamPanel2.add(t4);
		teamPanel2.add(t5);
		teamPanel2.add(t6);
		
		JPanel eastrankPanel =new JPanel();
		eastrankPanel.setBounds(494,5,257,600);
		eastrankPanel.setLayout(new BoxLayout(eastrankPanel, BoxLayout.Y_AXIS));
		eastpanel.add(eastrankPanel);
		
		TeamRankedPanel trp1=new TeamRankedPanel("东部联盟",season.getSeason());
		eastrankPanel.add(trp1);
		

		JPanel westrankPanel =new JPanel();
		westrankPanel.setBounds(494,5,257,600);
		westrankPanel.setLayout(new BoxLayout(westrankPanel, BoxLayout.Y_AXIS));
		westpanel.add(westrankPanel);
		
		TeamRankedPanel trp2=new TeamRankedPanel("西部联盟",season.getSeason());
		westrankPanel.add(trp2);
		
		season.addActionListener(e->{
			trp1.setSeason(season.getSeason());
			trp2.setSeason(season.getSeason());
		});
		
	}

}
