package bhwz.seac3.ui.user;

import java.text.SimpleDateFormat;

import javax.swing.JOptionPane;

import bhwz.seac3.RMIClientBLServiceManager;
import bhwz.seac3.blservice.query.MatchQueryBLService;
import bhwz.seac3.blservice.query.PlayerQueryBLService;
import bhwz.seac3.blservice.query.TeamQueryBLService;
import bhwz.seac3.util.SortableTable.DoubleClickOnRowDataListener;

public class DetailsListenerFactory {
	public static enum Type {
		ALL, AVG
	};

	public static DoubleClickOnRowDataListener getTeamDetailListener(Type type,
			String season) {
		return o -> {
			if(o==null)
				return;
			switch (type) {
			case ALL:
				try {
					new TeamDetailDialog(null,
							((TeamQueryBLService) RMIClientBLServiceManager
									.getBLService(TeamQueryBLService.class))
									.query场均数据ByName((String) o[0], season),season);
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(null,
							"服务器连接发生异常\n" + e1.getMessage());
					e1.printStackTrace();
				}
				break;
			case AVG:
				try {
					new TeamDetailDialog(null,
							((TeamQueryBLService) RMIClientBLServiceManager
									.getBLService(TeamQueryBLService.class))
									.query场均数据ByName((String) o[0], season),season);
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(null,
							"服务器连接发生异常\n" + e1.getMessage());
					e1.printStackTrace();
				}
				break;
			}
		};
	}

	public static DoubleClickOnRowDataListener getPlayerDetailListener(Type type,
			String season) {
		return o -> {
			if(o==null)
				return;
			switch (type) {
			case ALL:
				try {
					new PlayerDetailDialog(
							null,
							((PlayerQueryBLService) RMIClientBLServiceManager
									.getBLService(PlayerQueryBLService.class))
									.query场均数据ByName((String)o[0],season),season);
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(null, "服务器连接发生异常\n"
							+ e1.getMessage());
					e1.printStackTrace();
				}
				break;
			case AVG:
				try {
					new PlayerDetailDialog(
							null,
							((PlayerQueryBLService) RMIClientBLServiceManager
									.getBLService(PlayerQueryBLService.class))
									.query场均数据ByName((String)o[0],season),season);
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(null, "服务器连接发生异常\n"
							+ e1.getMessage());
					e1.printStackTrace();
				}
				break;
			}
		};
	}

	public static DoubleClickOnRowDataListener getMatchDetailListener(
			String season) {
		return o -> {
			if(o==null)
				return;
			try {
				new MatchDetailDialog(
						null,
						((MatchQueryBLService) RMIClientBLServiceManager
								.getBLService(MatchQueryBLService.class)).queryById(new SimpleDateFormat("yyyy-MM-dd").parse((String) o[0]).getTime()+";"+((String)o[1]).replace("-", ";")));
			} catch (Exception e1) {
				JOptionPane.showMessageDialog(null, "服务器连接发生异常\n"
						+ e1.getMessage());
				e1.printStackTrace();
			}
		};
	}
}
