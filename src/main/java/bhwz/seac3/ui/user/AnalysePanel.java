package bhwz.seac3.ui.user;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.*;

/**
 * 作者 :byc
 * 创建时间：2015年6月8日 下午5:56:22
 * 类说明:显示数据分析结果的界面
 */
public class AnalysePanel extends JPanel{
	/**
	 * 
	 */
	private static final long serialVersionUID = 152312578869360710L;
	private static String team_name1;
	private static String team_name2;
	static AnalyseResultPanel arp;
	public AnalysePanel(){
		this.setBounds(0, 0, 800, 500);
		this.setLayout(null);
		
		AnalyseChoicePanel acp=new AnalyseChoicePanel();
		acp.setBounds(25, 0, 750, 80);
		this.add(acp);
		
		JButton button=new JButton("进行数据分析");
		button.setFont(new Font("微软雅黑",Font.BOLD,20));
		button.setBounds(320,82,160,40);
		button.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				List<JComboBox<String>> list=new ArrayList<JComboBox<String>>();
				int count = acp.getComponentCount();
				for (int i = 0; i < count; i++) {
					Object obj = acp.getComponent(i);
					if (obj instanceof JComboBox) {
						@SuppressWarnings("unchecked")
						JComboBox<String> jcb =  (JComboBox<String>) obj;
						list.add(jcb);
					}
				}
			team_name1=list.get(0).getSelectedItem().toString();
			team_name2=list.get(1).getSelectedItem().toString();
			AnalysePanel.this.remove(arp);
			arp=new AnalyseResultPanel(team_name1,team_name2);
			arp.setBounds(25, 124, 750, 300);
			AnalysePanel.this.add(arp);
			AnalysePanel.this.repaint();
				
			}
		});
		this.add(button);
		
		arp=new AnalyseResultPanel();
		arp.setBounds(25, 124, 750, 300);
		this.add(arp);
		
		this.repaint();
	}
	 @Override
	public void paintComponent(Graphics g)
     {
        java.net.URL imgURL=getClass().getResource("/picture/back2.jpg");
        ImageIcon icon=new ImageIcon(imgURL);
        g.drawImage(icon.getImage(),0,0,800,500,this);
     }
	 
	 
}