package bhwz.seac3.ui.user;

import java.awt.Font;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.TableModel;

import bhwz.seac3.RMIClientBLServiceManager;
import bhwz.seac3.blservice.query.TeamQueryBLService;
import bhwz.seac3.ui.user.DetailsListenerFactory.Type;
import bhwz.seac3.util.SortableTable;
import bhwz.seac3.util.SortableTableModel;
import bhwz.seac3.vo.TeamVo;

import java.awt.BorderLayout;

public class TeamDataPanel extends JPanel {

	private static final long serialVersionUID = 4510250461136407446L;
	private SortableTable table;
	private Object[][] info;
	private SeasonSelectComboBox timeComboBox;
	private final Object columns[] = { "队名", "场数", "投篮命中", "投篮次数", "三分命中",
			"三分次数", "罚球命中", "罚球次数", "攻篮板", "防篮板", "总篮板", "助攻", "抢断", "盖帽",
			"失误", "犯规", "得分", "投篮%", "三分%", "罚球%", "胜率", "进攻回合", "进攻%", "防守%",
			"进攻篮板%", "防守篮板%", "抢断%", "助攻%" };

	public TeamDataPanel() {
		super();
		setLayout(new BorderLayout(0, 0));

		JPanel panel = new JPanel();
		add(panel, BorderLayout.NORTH);
		JLabel teamRankLabel = new JLabel("球队数据");
		panel.add(teamRankLabel);
		teamRankLabel.setFont(new Font("华文楷体", Font.BOLD, 18));
		teamRankLabel.setBounds(0, 13, 84, 24);

		timeComboBox = new SeasonSelectComboBox();
		panel.add(timeComboBox);
		timeComboBox.setFont(new Font("微软雅黑", Font.PLAIN, 15));
		timeComboBox.setBounds(133, 13, 101, 21);

		JLabel timeLabel = new JLabel("赛季");
		panel.add(timeLabel);
		timeLabel.setFont(new Font("华文楷体", Font.BOLD, 17));
		timeLabel.setBounds(244, 13, 54, 20);

		JButton averageButton = new JButton("场均");
		panel.add(averageButton);
		averageButton.setBounds(670, 13, 84, 24);
		averageButton.setFocusPainted(false);
		averageButton.setFont(new Font("华文楷体", Font.BOLD, 16));
		averageButton.addActionListener(e->display场均数据(timeComboBox.getSelectedItem().toString()));

		JButton totalButton = new JButton("赛季");
		panel.add(totalButton);
		totalButton.setBounds(560, 13, 104, 24);
		totalButton.setFocusPainted(false);
		totalButton.setFont(new Font("华文楷体", Font.BOLD, 16));		
		totalButton.addActionListener(e->display总数据(timeComboBox.getSelectedItem().toString()));
		
		try {
			info = teamToArray(((TeamQueryBLService)RMIClientBLServiceManager
					.getBLService(TeamQueryBLService.class)).query赛季总数据(timeComboBox.getSelectedItem().toString()));
		} catch (Exception e1) {
			JOptionPane.showMessageDialog(this, "服务器连接发生异常\n"+e1.getMessage());
			e1.printStackTrace();
		}
		TableModel model = new SortableTableModel(info, columns);
		table = new SortableTable(model);
		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		switch(显示内容){
		case "总数据":
			table.addDoubleClickOnDataListener(DetailsListenerFactory.getTeamDetailListener(Type.ALL, timeComboBox.getSeason()),0);
			break;
		case "场均数据":
			table.addDoubleClickOnDataListener(DetailsListenerFactory.getTeamDetailListener(Type.AVG, timeComboBox.getSeason()),0);
			break;
		}
		JScrollPane pane = new JScrollPane(table);
		pane.setBounds(10, 50, 745, 320);
		this.add(pane);
	}

	private String 显示内容 = "总数据";
	
	public void display场均数据(String season){
		try {
			info = teamToArray(((TeamQueryBLService)RMIClientBLServiceManager
					.getBLService(TeamQueryBLService.class)).query场均数据(season));
			table.setModel(new SortableTableModel(info, columns));
			显示内容 = "场均数据";
		} catch (Exception e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(this, "服务器连接发生异常\n"+e.getMessage());
		}
	}

	public void display总数据(String season){
		try {
			info = teamToArray(((TeamQueryBLService)RMIClientBLServiceManager
					.getBLService(TeamQueryBLService.class)).query赛季总数据(season));
			table.setModel(new SortableTableModel(info, columns));
			显示内容 = "总数据";
		} catch (Exception e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(this, "服务器连接发生异常\n"+e.getMessage());
		}		
	}

	public Object[][] teamToArray(List<TeamVo> lt) {
		int length = lt.size();
		Object[][] info = new Object[length][28];
		for (int n = 0; n < length; n++) {
			info[n][0] = lt.get(n).get球队简称();
			info[n][1] = lt.get(n).get比赛场数();
			info[n][2] = lt.get(n).get投篮命中数();
			info[n][3] = lt.get(n).get投篮数();
			info[n][4] = lt.get(n).get三分命中数();
			info[n][5] = lt.get(n).get三分出手数();
			info[n][6] = lt.get(n).get罚球命中数();
			info[n][7] = lt.get(n).get罚球数();
			info[n][8] = lt.get(n).get前场篮板数();
			info[n][9] = lt.get(n).get后场篮板数();
			info[n][10] = lt.get(n).get篮板数();
			info[n][11] = lt.get(n).get助攻数();
			info[n][12] = lt.get(n).get断球数();
			info[n][13] = lt.get(n).get盖帽数();
			info[n][14] = lt.get(n).get失误数();
			info[n][15] = lt.get(n).get犯规数();
			info[n][16] = lt.get(n).get比赛得分();
			info[n][17] = lt.get(n).get命中率();
			info[n][18] = lt.get(n).get三分命中率();
			info[n][19] = lt.get(n).get罚球命中率();
			info[n][20] = lt.get(n).get胜率();
			info[n][21] = lt.get(n).get进攻回合();
			info[n][22] = lt.get(n).get进攻效率();
			info[n][23] = lt.get(n).get防守效率();
			info[n][24] = lt.get(n).get进攻篮板效率();
			info[n][25] = lt.get(n).get防守篮板效率();
			info[n][26] = lt.get(n).get抢断效率();
			info[n][27] = lt.get(n).get助攻率();

		}
		return info;

	}
}
