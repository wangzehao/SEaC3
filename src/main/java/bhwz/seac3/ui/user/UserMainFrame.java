package bhwz.seac3.ui.user;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import java.net.URL;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.ImageIcon;
import javax.swing.JTabbedPane;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;

public class UserMainFrame extends JFrame {
	
	private static final long serialVersionUID = -3186465281630510060L;
	private JPanel contentPane;
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				UserMainFrame frame = new UserMainFrame();
				frame.setVisible(true);
				frame.setLocationRelativeTo(null);
			}
		});
	}

	public UserMainFrame() {
		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
			UIManager.put("TabbedPane.font",new Font("华文楷体",Font.BOLD,20));
	    } catch (Exception e) {
			e.printStackTrace();
		}
		
		URL url = this.getClass().getResource("/picture/LOGO.jpg");
		Image img = Toolkit.getDefaultToolkit().getImage(url);
		this.setIconImage(img);
		
		setBounds(100, 100, 800, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JTabbedPane tabbedPane = new JTabbedPane(SwingConstants.TOP);
		contentPane.add(tabbedPane, BorderLayout.CENTER);
		
		JPanel firstPanel = new HomePagePanel();
		tabbedPane.addTab("    首页     ", null, firstPanel, null);
		
		JPanel hotspotPanel = new HotspotPanel();
		tabbedPane.addTab("     热点      ", null, hotspotPanel, null);
		
		JPanel playerPanel = new PlayerDataPanel();
		tabbedPane.addTab("    球员数据    ", null, playerPanel, null);
		
		JPanel teamPanel = new TeamPanel();
		tabbedPane.addTab("    球队数据    ", null, teamPanel, null);
		
		JPanel matchPanel = new MatchDataPanel();
		tabbedPane.addTab("     比赛数据     ", null, matchPanel, null);
		
		JPanel analysePanel = new AnalysePanel();
		tabbedPane.addTab("    数据分析    ", null, analysePanel, null);
		
		//JPanel predictPanel=new PredictPanel();
		//tabbedPane.addTab("     胜率预测     ", null, predictPanel, null);
		
		JPanel nbaLive=new NBALivePanel();
		tabbedPane.addTab("     文字直播     ", null, nbaLive, null);
		
		JLabel logoLabel = new JLabel();
		logoLabel.setIcon(new ImageIcon(UserMainFrame.class
				.getResource("/picture/pic.png")));
		contentPane.add(logoLabel, BorderLayout.NORTH);
	}

}
