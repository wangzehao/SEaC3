package bhwz.seac3.ui.user;
import java.util.ArrayList;
import java.util.List;

import javax.swing.*;


/**
 * 作者 :byc
 * 创建时间：2015年6月9日 下午6:41:53
 * 类说明:
 */
public class AnalyseDetailPanel extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8583393870248867560L;
	PredictSubteamPanel sub1;
	PredictSubteamPanel sub2;
public AnalyseDetailPanel(String name1,String name2){
	this.setSize(730,280);
	this.setVisible(true);
	this.setLayout(null);
	this.removeAll();
	
	PredictPanel panel=new PredictPanel(name1, name2);
	List<PredictSubteamPanel> list=new ArrayList<PredictSubteamPanel>();
	int count = panel.getComponentCount();
	for (int i = 0; i < count; i++) {
		Object obj = panel.getComponent(i);
		if (obj instanceof PredictSubteamPanel) {
			PredictSubteamPanel jcb =  (PredictSubteamPanel) obj;
			list.add(jcb);
		}
	}
	sub1=list.get(0);
	sub2=list.get(1);
	panel.setBounds(0, 0, this.getWidth(),this.getHeight());
	panel.setLayout(null);
	this.add(panel);
	this.repaint();
}

public double getRate1(){
	return sub1.getWinRate();
}

public double getRate2(){
	return sub2.getWinRate();
}

@Override
public void paintComponent(java.awt.Graphics g)
{
   java.net.URL imgURL=getClass().getResource("/picture/noback.png");
   ImageIcon icon=new ImageIcon(imgURL);
   g.drawImage(icon.getImage(),0,0,800,500,this);
}




}
