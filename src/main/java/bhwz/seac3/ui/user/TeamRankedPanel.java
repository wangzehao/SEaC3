package bhwz.seac3.ui.user;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.SystemColor;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import bhwz.seac3.LocalBLServiceManager;
import bhwz.seac3.RMIClientBLServiceManager;
import bhwz.seac3.blservice.AutoRedisplay;
import bhwz.seac3.blservice.query.TeamQueryBLService;
import bhwz.seac3.ui.user.DetailsListenerFactory.Type;
import bhwz.seac3.util.SortableTable;
import bhwz.seac3.util.SortableTableModel;
import bhwz.seac3.util.ListSorter.SortOrder;
import bhwz.seac3.vo.TeamVo;

import java.awt.BorderLayout;

import javax.swing.BoxLayout;

import java.awt.CardLayout;

/**
 * 作者 :byc 创建时间：2015年4月19日 下午9:14:15 类说明:
 */
public class TeamRankedPanel extends JPanel implements AutoRedisplay {
	private static final long serialVersionUID = -2007153413636072494L;
	private String a;
	private String season = "13-14";
	private JScrollPane tablePane;
	private SortableTable table;
	private Object[][] data;
	private String[] tableColumns = new String[] { "球队", "胜-负", "胜率", "排序项" };

	private final Vector<String> sortColumns = new Vector<String>(
			Arrays.asList(new String[] { "球队全名", "比赛场数", "投篮命中数", "投篮数",
					"三分命中数", "三分出手数", "罚球命中数", "罚球数", "前场篮板数", "后场篮板数", "篮板数",
					"助攻数", "断球数", "盖帽数", "失误数", "犯规数", "比赛得分", "命中率", "三分命中率",
					"罚球命中率", "胜率", "进攻回合", "进攻效率", "防守回合", "防守效率", "进攻篮板效率",
					"防守篮板效率", "助攻率", "抢断效率", }));
	private final ArrayList<Function<TeamVo, Comparable<?>>> sortMethods = new ArrayList<Function<TeamVo, Comparable<?>>>() {

		private static final long serialVersionUID = 1L;

		{
			this.add(vo -> vo.get球队全名());
			this.add(vo -> vo.get比赛场数());
			this.add(vo -> vo.get投篮命中数());
			this.add(vo -> vo.get投篮数());
			this.add(vo -> vo.get三分命中数());
			this.add(vo -> vo.get三分出手数());
			this.add(vo -> vo.get罚球命中数());
			this.add(vo -> vo.get罚球数());
			this.add(vo -> vo.get前场篮板数());
			this.add(vo -> vo.get后场篮板数());
			this.add(vo -> vo.get篮板数());
			this.add(vo -> vo.get助攻数());
			this.add(vo -> vo.get断球数());
			this.add(vo -> vo.get盖帽数());
			this.add(vo -> vo.get失误数());
			this.add(vo -> vo.get犯规数());
			this.add(vo -> vo.get比赛得分());
			this.add(vo -> vo.get命中率());
			this.add(vo -> vo.get三分命中率());
			this.add(vo -> vo.get罚球命中率());
			this.add(vo -> vo.get胜率());
			this.add(vo -> vo.get进攻回合());
			this.add(vo -> vo.get进攻效率());
			this.add(vo -> vo.get防守回合());
			this.add(vo -> vo.get防守效率());
			this.add(vo -> vo.get进攻篮板效率());
			this.add(vo -> vo.get防守篮板效率());
			this.add(vo -> vo.get助攻率());
			this.add(vo -> vo.get抢断效率());
		}
	};
	private final String[] sortOrder = new String[] { "降序", "升序" };
	private JComboBox<String> comboBoxSort;
	private JComboBox<String> comboBoxSortOrder;

	public TeamRankedPanel(String area,String season) {
		if (area.equals("东部联盟")) {
			a = "东部排名";
		} else {
			a = "西部排名";
		}
		this.season=season;
		this.setBackground(Color.WHITE);
		setLayout(new BorderLayout(0, 0));

		JPanel panel = new JPanel();
		add(panel, BorderLayout.CENTER);
		panel.setLayout(new BorderLayout(0, 0));

		tablePane = new JScrollPane(table);
		panel.add(tablePane, BorderLayout.CENTER);

		JPanel sortPanel = new JPanel();
		panel.add(sortPanel, BorderLayout.NORTH);
		sortPanel.setBackground(SystemColor.controlHighlight);
		sortPanel.setBounds(0, 44, 257, 29);
		sortPanel.setLayout(new BoxLayout(sortPanel, BoxLayout.X_AXIS));

		JLabel rankLabel = new JLabel("排序条件");
		rankLabel.setFont(new Font("华文楷体", Font.BOLD, 16));
		sortPanel.add(rankLabel);

		comboBoxSort = new JComboBox<String>();
		comboBoxSort.setModel(new DefaultComboBoxModel<String>(sortColumns));
		comboBoxSort.setFont(new Font("微软雅黑", Font.PLAIN, 15));
		comboBoxSort.setSelectedItem("胜率");
		comboBoxSort.addActionListener(e -> redisplay());
		sortPanel.add(comboBoxSort);

		comboBoxSortOrder = new JComboBox<String>();
		comboBoxSortOrder.addActionListener(e -> redisplay());
		comboBoxSortOrder.setModel(new DefaultComboBoxModel<String>(sortOrder));
		comboBoxSortOrder.setFont(new Font("微软雅黑", Font.PLAIN, 15));
		comboBoxSortOrder.setBackground(SystemColor.controlHighlight);
		sortPanel.add(comboBoxSortOrder);

		JPanel panel_1 = new JPanel();
		panel_1.setBackground(SystemColor.textHighlight);
		add(panel_1, BorderLayout.NORTH);
		panel_1.setLayout(new CardLayout(0, 0));

		JLabel lblFgd = new JLabel(a);
		panel_1.add(lblFgd, "name_29462289419465");
		lblFgd.setBackground(SystemColor.textHighlight);
		lblFgd.setForeground(Color.WHITE);
		lblFgd.setFont(new Font("华文楷体", Font.BOLD, 20));
		lblFgd.setPreferredSize(new Dimension(100, 40));

		LocalBLServiceManager.getRedisplayControllerBLService().register(this);
		redisplay();
	}

	@Override
	public void redisplay() {
		try {
			List<TeamVo> queryResult = ((TeamQueryBLService) RMIClientBLServiceManager
					.getBLService(TeamQueryBLService.class)).query场均数据(season,
					getSortMethod(), getSortOrder());
			queryResult = queryResult
					.stream()
					.filter(vo -> {
						if (a.equals("东部排名")) {
							return "Southeast".equals(vo.get分区())
									|| "Central".equals(vo.get分区())
									|| "Atlantic".equals(vo.get分区());
						} else {
							return "Pacific".equals(vo.get分区())
									|| "Northwest".equals(vo.get分区())
									|| "Southwest".equals(vo.get分区());
						}
					}).collect(Collectors.toList());
			data = teamToArray(queryResult);

			displayData();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void  setSeason(String season){
		this.season=season;
		redisplay();
	}

	private void displayData() {
		setSortHead((String)comboBoxSort.getSelectedItem());
		table = new SortableTable(new SortableTableModel(data, tableColumns));
		table.addDoubleClickOnDataListener(DetailsListenerFactory
				.getTeamDetailListener(Type.AVG, season),0);
		tablePane.setViewportView(table);
	}

	private Comparable<?> getSortColumnData(TeamVo vo) {
		return getSortMethod().apply(vo);
	}

	private Function<TeamVo, Comparable<?>> getSortMethod() {
		return sortMethods.get(sortColumns.indexOf(comboBoxSort
				.getSelectedItem()));
	}

	private SortOrder getSortOrder() {
		if (comboBoxSortOrder.getSelectedItem().equals("升序"))
			return SortOrder.ESC;
		else
			return SortOrder.DESC;
	}

	public Object[][] teamToArray(List<TeamVo> lt) {
		int length = lt.size();
		Object[][] info = new Object[length][28];
		for (int n = 0; n < length; n++) {
			info[n][0] = lt.get(n).get球队简称();
			info[n][1] = "" + lt.get(n).get胜利场数() + "-"
					+ (lt.get(n).get比赛场数() - lt.get(n).get胜利场数());
			info[n][2] = lt.get(n).get胜率();
			info[n][3] = getSortColumnData(lt.get(n));
		}
		return info;

	}
	
	private void setSortHead(String name){
		tableColumns[3]=(String) comboBoxSort.getSelectedItem();
	}

}
