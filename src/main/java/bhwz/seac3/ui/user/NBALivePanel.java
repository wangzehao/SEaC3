package bhwz.seac3.ui.user;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;

public class NBALivePanel extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public NBALivePanel(){
		setLayout(new BorderLayout(0, 0));
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBorder(new EmptyBorder(0, 0, 0, 0));
		add(scrollPane, BorderLayout.CENTER);
		JTextArea textArea = new JTextArea();
		textArea.setEditable(false);
		scrollPane.setViewportView(textArea);
		
	/*	NBALive live=new NBALive();
		new Thread(){
			public void run(){
				live.broadCast(s->textArea.append(s+"\n"));
			}
		}.start();		
		*/
	}
}
