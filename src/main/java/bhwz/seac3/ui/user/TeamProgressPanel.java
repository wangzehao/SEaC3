package bhwz.seac3.ui.user;

import java.awt.BorderLayout;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import bhwz.seac3.RMIClientBLServiceManager;
import bhwz.seac3.blservice.query.TeamQueryBLService;
import bhwz.seac3.blservice.query.SeasonQueryBLService;
import bhwz.seac3.ui.user.graphics.Graphics;
import bhwz.seac3.vo.TeamVo;

public class TeamProgressPanel extends JPanel{
	
	private static final long serialVersionUID = 1L;
	private SeasonSelectComboBox seasonBox;
	private TeamVo team;
	private JPanel chart;
	boolean allSeason;
	private Map<String, Function<TeamVo, Double>> valueMap = new HashMap<String, Function<TeamVo, Double>>() {

		private static final long serialVersionUID = 1L;

		{
			Class<TeamVo> c = TeamVo.class;
			Method[] ms = c.getMethods();
			for (Method m : ms) {
				if (m.getReturnType().getName().toLowerCase()
						.endsWith("double")) {
					String mname = m.getName();
					if (mname.startsWith("get"))
						put(mname.substring(3), vo -> {
							try {
								return (Double) (m.invoke(vo));
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
								return 0.0;
							}
						});
				}
			}
		}

	};
	private JComboBox<String> valueBox;

	public TeamProgressPanel(TeamVo team) {
		this.team = team;
		setLayout(new BorderLayout(0, 0));

		JPanel bar = new JPanel();
		add(bar, BorderLayout.NORTH);

		JButton button = new JButton("\u5168\u90E8\u8D5B\u5B63");
		button.addActionListener(e -> showChart(true));
		bar.add(button);

		seasonBox = new SeasonSelectComboBox();
		seasonBox.addActionListener(e->showChart(false));

		JButton button_1 = new JButton("\u6307\u5B9A\u8D5B\u5B63");
		button_1.addActionListener(e -> showChart(false));
		bar.add(button_1);

		bar.add(seasonBox);

		JLabel label = new JLabel("\u8D5B\u5B63");
		bar.add(label);

		valueBox = new JComboBox<>();
		valueBox.setModel(new DefaultComboBoxModel<String>(valueMap.keySet()
				.toArray(new String[0])));
		bar.add(valueBox);
		valueBox.addActionListener(e->showChart(allSeason));
		
		showChart(false);
	}

	public void showChart(boolean allSeason) {
		this.allSeason=allSeason;
		TeamQueryBLService bl = RMIClientBLServiceManager
				.getBLService(TeamQueryBLService.class);
		SeasonQueryBLService sbl = RMIClientBLServiceManager
				.getBLService(SeasonQueryBLService.class);
		try {
			List<TeamVo> l;
			if (allSeason)
				l = bl.query各个赛季表现(team);
			else
				l = bl.query每场比赛表现(seasonBox.getSeason(), team);

			Function<TeamVo, Double> sortColumn = valueMap.get(valueBox
					.getSelectedItem());
			List<Double> values = l.stream().map(sortColumn)
					.collect(Collectors.toList());
			if(chart!=null)
				this.remove(chart);
			
			if (allSeason) {
				List<String> seasons = sbl
						.queryJoinedSeasons(team);
				

				chart=(Graphics.getLineChart("全部赛季表现",
						(String) valueBox.getSelectedItem(), seasons, values,
						quartiles(seasons,bl,sortColumn).map(list -> list.get(0)).map(sortColumn)
								.collect(Collectors.toList()),
						quartiles(seasons,bl,sortColumn).map(list -> list.get(1)).map(sortColumn)
								.collect(Collectors.toList()),
						quartiles(seasons,bl,sortColumn).map(list -> list.get(2)).map(sortColumn)
								.collect(Collectors.toList()),
						quartiles(seasons,bl,sortColumn).map(list -> list.get(3)).map(sortColumn)
								.collect(Collectors.toList())));
			}else{
				List<TeamVo> quartiles =bl.query所有球队赛季表现四分位数(seasonBox.getSeason(), vo->sortColumn.apply(vo));

				chart=(Graphics.getLineChart(seasonBox.getSeason()+"赛季表现",
						(String) valueBox.getSelectedItem(), l.stream().map(vo->new java.util.Date(vo.get比赛日期())).collect(Collectors.toList()), values,
						sortColumn.apply(quartiles.get(0)),
						sortColumn.apply(quartiles.get(1)),
						sortColumn.apply(quartiles.get(2)),
						sortColumn.apply(quartiles.get(3))));
				
			}
			this.add(chart,BorderLayout.CENTER);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private Stream<List<TeamVo>> quartiles(List<String> seasons,TeamQueryBLService bl,	Function<TeamVo, Double> sortColumn){
		return seasons
				.stream()
				.map(season -> {
					try {
					
						return bl.query所有球队赛季表现四分位数(season,
								vo -> sortColumn.apply(vo));
					} catch (Exception e) {
						return null;
					}
				});
	}
}
