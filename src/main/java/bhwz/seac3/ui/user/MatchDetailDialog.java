package bhwz.seac3.ui.user;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.WindowConstants;

import bhwz.seac3.RMIClientBLServiceManager;
import bhwz.seac3.util.SVGManager;
import bhwz.seac3.util.SortableTable;
import bhwz.seac3.util.SortableTableModel;
import bhwz.seac3.util.TextButton;
import bhwz.seac3.vo.MatchVo;
import bhwz.seac3.vo.PlayerScoreTableVo;
import bhwz.seac3.vo.TeamVo;

import javax.swing.JLabel;
import javax.swing.table.TableModel;

import org.apache.batik.swing.JSVGCanvas;
import org.w3c.dom.svg.SVGDocument;

import bhwz.seac3.blservice.query.TeamQueryBLService;


public class MatchDetailDialog extends JDialog {
	private static final long serialVersionUID = 7392616017799609332L;
	private JPanel contentPane;
	private JScrollPane scrollPane;
	private SortableTable table;
	private Object[][] data;
	private final Object[] playerScoreColumns = { "球员名", "位置", "在场时间","个人得分", "投篮命中数","投篮出手数","三分命中数","三分出手数","罚球命中数","罚球出手数","进攻篮板数","防守篮板数","总篮板数","助攻数","抢断数","盖帽数","失误数","犯规数" };
    private final int width=815,height=680;
	private  int div1X,div1Y,div2X,div2Y,div3X,div3Y,div4X,div4Y;
	
	
	public MatchDetailDialog(Frame owner, MatchVo vo) {
		super(owner);
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setVisible(true);
		setBounds(300, 0, width, height);
		setResizable(false);
		contentPane = new JPanel();
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		contentPane.setBackground(Color.white);
		
		
		
		
		JPanel panel = new JPanel();
		panel.setPreferredSize(new Dimension(700, 950));
	    panel.setBackground(Color.white);
        scrollPane = new JScrollPane(panel);
		panel.setLayout(null);
		
		int labelWidth=50,labelHeight=25;
		String[] team={vo.getTeam1(),vo.getTeam2()};
		
		//div1
		div1X=150;div1Y=0;int gap=300;
		
		String session="";
		String date=new SimpleDateFormat("yyyy-MM-dd").format(new Date(vo.getDate()));
		String temp=date.substring(2, 4);
		if(Integer.parseInt(date.substring(5, 7))>=7){
		 session=temp+"-"+String.valueOf(Integer.parseInt(temp)+1);
		}else{
			session=String.valueOf(Integer.parseInt(temp)-1)+"-"+temp;
		}
		for(int t=0;t<team.length;t++){
			JSVGCanvas canvas = new JSVGCanvas();
			canvas.setBounds(div1X, div1Y,200,200);
			panel.add(canvas);
			SVGDocument svgd;
			
			TeamVo teamVo=new TeamVo();
			try {
				teamVo = ((TeamQueryBLService) RMIClientBLServiceManager
						.getBLService(TeamQueryBLService.class)).query场均数据ByName(
						team[t], session);
			} catch (Exception e1) {
				JOptionPane
				.showMessageDialog(this, "服务器连接发生异常\n" + e1.getMessage());
				e1.printStackTrace();
			}
	        byte[] icon=teamVo.getTeamIcon();
			try {
				svgd = new SVGManager().decodeImage(icon);
				canvas.setSVGDocument(svgd);
			} catch (ClassNotFoundException | IOException e) {
				e.printStackTrace();
			}
			div1X+=gap;
		}
				
		
		//div2
		List<String> sectionScores=vo.getSectionScores();
		List<String> team1=new LinkedList<>();
		List<String> team2=new LinkedList<>();
		sectionScores.forEach(s->{
			team1.add(s.substring(0,s.indexOf("-")));
			team2.add(s.substring(s.indexOf("-")+1));
		});
		team1.add(vo.getTeam1Score()+"");
		team2.add(vo.getTeam2Score()+"");
		
		div2X=170;div2Y=200;
		int gapBetweenLableY=30;
		for(int t=0;t<team.length;t++){
			JLabel lblNewLabel_1 = new JLabel(team[t]);
			lblNewLabel_1.setBounds(div2X, div2Y, labelWidth, labelHeight);
			panel.add(lblNewLabel_1);
			int start1=div2X+20;int gapBetweenLableX=70;
			List<String> teamSectionScore=t==0?team1:team2;
			for(int i=0;i<teamSectionScore.size();i++ ){
				JLabel t1 = new JLabel(teamSectionScore.get(i));
				t1.setBounds(start1+=gapBetweenLableX, div2Y, labelWidth, labelHeight);
				panel.add(t1);
			}
	        div2Y+=gapBetweenLableY;
		}
		
		
		//div3
		div3X=10;div3Y=300;
		JLabel labelGuide=new JLabel("-------技术统计--------");
		//labelGuide.setFont(new Font("微软雅黑", Font.PLAIN, 15));
		labelGuide.setBounds(div3X+300, div3Y, labelWidth+200, labelHeight);
		panel.add(labelGuide);
		//div4
		div4X=10;div4Y=350;int gapYBetweenTable=50;
		int tableWidth=770,tableHeight=200;
		final String season=session;
		for(int t=0;t<team.length;t++){
			final String teamName=team[t];
			TextButton buttonTeam=new TextButton(team[t]);
			buttonTeam.setFont(new Font("微软雅黑", Font.PLAIN, 28));
			buttonTeam.setBounds(div4X+238, div4Y, labelWidth+200, labelHeight);
			buttonTeam.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					try {
						new TeamDetailDialog(
								null,
								((TeamQueryBLService) RMIClientBLServiceManager
										.getBLService(TeamQueryBLService.class))
										. query场均数据ByName(teamName,
												season),season);
					} catch (Exception e1) {
						e1.printStackTrace();
					}
				}
			});
			panel.add(buttonTeam);
		/*	JLabel labelTeam=new JLabel(team[t]);
			labelTeam.setFont(new Font("微软雅黑", Font.PLAIN, 18));
			labelTeam.setBounds(div4X, div4Y, labelWidth+200, labelHeight);
			panel.add(labelTeam);
		*/
			Set<PlayerScoreTableVo> pst=t==0?vo.getTeam1PlayersScore():vo.getTeam2PlayersScore();
			data=playerScoreToArray(pst);
			TableModel model = new SortableTableModel(data, playerScoreColumns);
			table = new SortableTable(model);
			table.addDoubleClickOnDataListener( DetailsListenerFactory.getPlayerDetailListener(DetailsListenerFactory.Type.AVG,session),0);
			table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);			
			JScrollPane js=new JScrollPane(table);
			tableHeight=data.length*21;
			js.setBounds(div4X, div4Y+labelHeight,tableWidth,tableHeight);
			panel.add(js);
			div4Y+=tableHeight+=labelHeight;
			div4Y+=gapYBetweenTable;
		}
		contentPane.add(scrollPane);
	}
	
	
	private Object[][] playerScoreToArray(Set<PlayerScoreTableVo> pst) {
		int length = pst.size();
		Object[][] info = new Object[length][18];
		int [] i={-1};
		pst.forEach(s->{
			i[0]++;
		    info[i[0]][0]=s.get球员名();
		    info[i[0]][1]=s.get位置();
		    info[i[0]][2]=s.get在场时间();
		    info[i[0]][3]=s.get个人得分();
		    info[i[0]][4]=s.get投篮命中数();
		    info[i[0]][5]=s.get投篮出手数();
		    info[i[0]][6]=s.get三分命中数();
		    info[i[0]][7]=s.get三分出手数();
		    info[i[0]][8]=s.get罚球命中数();
		    info[i[0]][9]=s.get罚球出手数();
		    info[i[0]][10]=s.get进攻篮板数();
		    info[i[0]][11]=s.get防守篮板数();
		    info[i[0]][12]=s.get总篮板数();
		    info[i[0]][13]=s.get助攻数();
		    info[i[0]][14]=s.get抢断数();
		    info[i[0]][15]=s.get盖帽数();
		    info[i[0]][16]=s.get失误数();
		    info[i[0]][17]=s.get失误数();
		});
		return info;
	}
	/*
	@Test
	public void test(){
		JFrame f=new JFrame();
		f.setSize(200, 200);
		f.setVisible(true);
		JButton b=new JButton("test");
		b.setSize(100,100);
		f.getContentPane().add(b);
		MatchVo vo=new MatchVo();
		vo.setTeam1("勇士");
		vo.setTeam2("火箭");
		vo.setTeam1Score(120);
		vo.setTeam2Score(100);
		List<String> section=new LinkedList<>();
		section.add("30-25");
		section.add("30-25");
		section.add("30-25");
		section.add("30-25");
		vo.setSectionScores(section);
		Set<PlayerScoreTableVo> ps=new HashSet<>();
		PlayerScoreTableVo p1=new PlayerScoreTableVo();
		p1.set球员名("kobe");
		p1.set位置("F");
		p1.set在场时间(20);
		p1.set三分出手数(1);
		p1.set三分命中数(1);
		PlayerScoreTableVo p2=new PlayerScoreTableVo();
		PlayerScoreTableVo p3=new PlayerScoreTableVo();
		PlayerScoreTableVo p4=new PlayerScoreTableVo();
		PlayerScoreTableVo p5=new PlayerScoreTableVo();
		PlayerScoreTableVo p6=new PlayerScoreTableVo();
		PlayerScoreTableVo p7=new PlayerScoreTableVo();
		PlayerScoreTableVo p8=new PlayerScoreTableVo();
		PlayerScoreTableVo p9=new PlayerScoreTableVo();
		PlayerScoreTableVo p10=new PlayerScoreTableVo();
		PlayerScoreTableVo p11=new PlayerScoreTableVo();
		PlayerScoreTableVo p12=new PlayerScoreTableVo();
		p2.set球员名("P2");
		p3.set球员名("P3");
		p4.set球员名("P4");
		p5.set球员名("P5");
		p6.set球员名("P6");
		p7.set球员名("P7");
		p8.set球员名("P8");
		p9.set球员名("P9");
		p10.set球员名("P10");
		p11.set球员名("P11");
		p12.set球员名("P12");
		ps.add(p1);
		ps.add(p2);
		ps.add(p3);
		ps.add(p4);
		ps.add(p5);
		ps.add(p6);
		ps.add(p7);
		ps.add(p8);
		ps.add(p9);
		ps.add(p10);
		ps.add(p11);
		ps.add(p12);
		Set<PlayerScoreTableVo> ps2=new HashSet<>();
		PlayerScoreTableVo p22=new PlayerScoreTableVo();
		p22.set球员名("hello");
		p22.set位置("F");
		p22.set在场时间(20);
		p22.set三分出手数(1);
		p22.set三分命中数(1);
		ps2.add(p22);
		vo.setTeam1PlayersScore(ps);
		vo.setTeam2PlayersScore(ps2);
		b.addActionListener(e->{
			new MatchDetailDialog(f,vo);
		});
		
	}
	*/
}
