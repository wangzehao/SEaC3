package bhwz.seac3.ui.user;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.rmi.RemoteException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.apache.batik.swing.JSVGCanvas;
import org.w3c.dom.svg.SVGDocument;

import bhwz.seac3.DBServiceManager;
import bhwz.seac3.LocalBLServiceManager;
import bhwz.seac3.RMIClientBLServiceManager;
import bhwz.seac3.bl.datacomputer.DataComputer;
import bhwz.seac3.bl.query.HotSpotQueryBL;
import bhwz.seac3.bl.query.MatchQueryBL;
import bhwz.seac3.bl.query.TeamQueryBL;
import bhwz.seac3.blservice.AutoRedisplay;
import bhwz.seac3.blservice.datacomputer.PerGameTeamDataComputeBLService;
import bhwz.seac3.blservice.query.HotspotQueryBLService;
import bhwz.seac3.blservice.query.MatchQueryBLService;
import bhwz.seac3.blservice.query.PlayerQueryBLService;
import bhwz.seac3.blservice.query.TeamQueryBLService;
import bhwz.seac3.dbservice.nbadata.HotSpotDBService;
import bhwz.seac3.dbservice.nbadata.PlayerMatchDBService;
import bhwz.seac3.dbservice.nbadata.SaveQueryDBService;
import bhwz.seac3.dbservice.nbadata.TeamMatchDBService;
import bhwz.seac3.po.MatchPo;
import bhwz.seac3.po.TeamGameDataPo;
import bhwz.seac3.po.TeamIconPo;
import bhwz.seac3.po.TeamPo;
import bhwz.seac3.util.PictureManager;
import bhwz.seac3.util.SVGManager;
import bhwz.seac3.util.TextButton;
import bhwz.seac3.vo.MatchVo;
import bhwz.seac3.vo.PlayerVo;
import bhwz.seac3.vo.TeamVo;

public class HomePagePanel extends JPanel implements AutoRedisplay{
	
	private static final long serialVersionUID = 1L;
	
	private int width;
	private int height;
	private int left_width=0;
	private int right_width=0;
	private int left_height=0;
	private int right_height=0;
	private LeftPanel left=null;
	private RightPanel right=null;
	private PictureManager manager=null;
	private SaveQueryDBService<TeamIconPo> teamPicManager=null;
	private SaveQueryDBService<MatchPo> matchManager=null;
	private MatchQueryBLService matchQuery=null;
	private PlayerMatchDBService pm=null;
	private TeamMatchDBService pmd=null;
	private HotSpotDBService hdb=null;
	private ButtonType type=ButtonType.MATCH;
	private String currentSeason=null;
	private TeamQueryBLService teambl=null;
	private PerGameTeamDataComputeBLService cbl=null;
	private SaveQueryDBService<TeamPo> tdb=null;
	private SaveQueryDBService<TeamGameDataPo> tddb;
	
	enum ButtonType{
		MATCH,
		PLAYER,
		TEAM
	}
	
	private class LeftPanel extends JPanel{
		
		private static final long serialVersionUID = 1L;
		
		LeftPanel(int width,int height){
			//调整左边panel的属性：尺寸，不透明，背景白，可见
			super();
			left_width=(int)(2.0/7*width);
			left_height=height;
			this.setSize(left_width, left_height);
			Dimension size=new Dimension(left_width-40,left_height);
			this.setMinimumSize(size);
			this.setPreferredSize(size);
			this.setMaximumSize(size);
			this.setBackground(Color.WHITE);
			//this.setOpaque(true);
			this.setOpaque(false);
			this.setVisible(true);
			
			//添加三个choicebutton
			ChoiceButton matchButton=new ChoiceButton(left_width,(int)(left_height*1.0/3),ButtonType.MATCH);
			ChoiceButton playerButton=new ChoiceButton(left_width,(int)(left_height*1.0/3),ButtonType.PLAYER);
			ChoiceButton teamButton=new ChoiceButton(left_width,(int)(left_height*1.0/3),ButtonType.TEAM);
			this.add(matchButton);
			this.add(playerButton);
			this.add(teamButton);
			
			
		       }
		 public void paintComponent(Graphics g)
	       {
	       int x=0,y=0;
	          java.net.URL imgURL=getClass().getResource("/picture/leftPanel.png");
	  
	          //test.jpg是测试图片，与Demo.java放在同一目录下
	          ImageIcon icon=new ImageIcon(imgURL);
	          g.drawImage(icon.getImage(),x,y,getSize().width,getSize().height,this);
	          while(true)
	          {
	            g.drawImage(icon.getImage(),x,y,this);
	            if(x>getSize().width && y>getSize().height)break;
              //这段代码是为了保证在窗口大于图片时，图片仍能覆盖整个窗口
	            if(x>getSize().width)
	            {
	               x=0;
	               y+=icon.getIconHeight();
	            }
	            else
	             x+=icon.getIconWidth();
	          }
		}
	    
		private class ChoiceButton extends JButton{
			
			private static final long serialVersionUID = 1L;
			
			private String org_pic=null;
			private String roll_pic=null;
			
			ChoiceButton(int b_width,int b_height,ButtonType type){
				//调整ChoiceButton的尺寸，背景白，透明
				super();
				this.setBackground(Color.WHITE);
				this.setSize(b_width,b_height);
				this.setBorder(null);
				Dimension size=new Dimension(b_width,b_height);
				this.setMinimumSize(size);
				this.setPreferredSize(size);
				this.setMaximumSize(size);
				//this.setOpaque(false);
				this.setContentAreaFilled(false);

				//根据类型设置图片路径
				switch(type){
				case MATCH:org_pic="/picture/matchButton.png";
				           roll_pic="/picture/lastMatch.png";
						   break;
				case PLAYER:org_pic="/picture/playerButton.png";
		                    roll_pic="/picture/scoreKing.png";
				            break;
				case TEAM:org_pic="/picture/teamButton.png";
		                   roll_pic="/picture/teamKing.png";
				          break;
				}
				
				//分别添加选择时和未选择时的图片
				this.setRolloverIcon(new ImageIcon(getClass().getResource(roll_pic)));
				this.setIcon(new ImageIcon(getClass().getResource(org_pic)));
				
				//添加时间,改变右panel的显示内容
				this.addActionListener(new ActionListener(){
					 public void actionPerformed(ActionEvent e){
						 switch(type){
						 case MATCH:
							 remove_all();//移除该面板所有组件
							 addLeft(width,height);//添加左panel
							 addRight(width,height,ButtonType.MATCH);//添加右panel
							 refresh();//刷新
							 setMatch();
							 break;
						 case PLAYER:
							 remove_all();
							 addLeft(width,height);
							 addRight(width,height,ButtonType.PLAYER);
							 refresh();
							 setPlayer();
							 break;
						 case TEAM:
							 remove_all();
							 addLeft(width,height);
							 addRight(width,height,ButtonType.TEAM);
							 refresh();
							 setTeam();
							 break;
						 }
					 }
				});
			}
		}
	}
	
	private class RightPanel extends JPanel{
		
		private static final long serialVersionUID = 1L;
		
		 public void paintComponent(Graphics g)
	       {
	       int x=0,y=0;
	          java.net.URL imgURL=getClass().getResource("/picture/leftPanel.png");
	  
	          //test.jpg是测试图片，与Demo.java放在同一目录下
	          ImageIcon icon=new ImageIcon(imgURL);
	          g.drawImage(icon.getImage(),x,y,getSize().width,getSize().height,this);
	          while(true)
	          {
	            g.drawImage(icon.getImage(),x,y,this);
	            if(x>getSize().width && y>getSize().height)break;
            //这段代码是为了保证在窗口大于图片时，图片仍能覆盖整个窗口
	            if(x>getSize().width)
	            {
	               x=0;
	               y+=icon.getIconHeight();
	            }
	            else
	             x+=icon.getIconWidth();
	          }
	       }
		
		RightPanel(int width,int height,ButtonType type){
			//设置尺寸，不透明，背景白，给view加上backPanel,backPanel设置为boxLayout
			super();
			right_width=(int)(5.0/7*width);
			right_height=height;
			Dimension size=new Dimension(right_width,right_height);
			this.setSize(size);
			this.setMinimumSize(size);
			this.setPreferredSize(size);
			this.setMaximumSize(size);
			this.setOpaque(false);
			//JPanel backPanel=new JPanel();
			//backPanel.setSize(size);
			//backPanel.setMinimumSize(size);
			//backPanel.setPreferredSize(size);
			//backPanel.setMaximumSize(size);
			//backPanel.setBackground(Color.WHITE);
			//backPanel.setOpaque(true);
			//backPanel.setLayout(new BoxLayout(backPanel,BoxLayout.Y_AXIS));
			this.setBackground(Color.WHITE);
			//this.setOpaque(true);
			this.setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
			//this.setViewportView(backPanel);
			
			//在bakcPanel上添加contentPanel
			switch(type){
			case MATCH:
				List<MatchVo> matches=null;
				try {
					matches=matchQuery.query当天比赛();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if(matches==null||matches.size()==0){
					break;
				}
				for(int i=0;i<matches.size();i++){
					//backPanel.add(new ContentPanel(matches.get(i)));
					//backPanel.validate();
					this.add(new ContentPanel(matches.get(i)));
					this.validate();
				}
				setCurrentSeason(matches.get(0).getSeason());
				break;
			case PLAYER:
				HotspotQueryBLService h=null;
				try {
					h=new HotSpotQueryBL();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				List<PlayerVo> list=null;
				try {
					 list=h.QueryPlayersRankedBy条件("场均得分");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if(list==null||list.size()==0){
					break;
				}
				for(int i=0;i<list.size();i++){
					//backPanel.add(new ContentPanel(list.get(i)));
					//backPanel.validate();
					this.add(new ContentPanel(list.get(i)));
					this.validate();
				}
				break;
			case TEAM:
				List<TeamVo> list2=null;
				try {
					 list2=teambl.query场均数据(getCurrentSeason()).subList(0, 5);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if(list2==null||list2.size()==0){
					break;
				}
				for(int i=0;i<list2.size();i++){
					//backPanel.add(new ContentPanel(list2.get(i)));
					//backPanel.validate();
					this.add(new ContentPanel(list2.get(i)));
					this.validate();
				}
				break;
			}
			
			
			
		}
		
		
		private class ContentPanel extends JPanel{
			
			private static final long serialVersionUID = 1L;
			
			private void init(){
				//设置contentPanel，设置尺寸,背景白,不透明和flowLayout
				this.setSize(right_width,(int)(0.19*right_height));
				Dimension s=new Dimension(right_width,(int)(0.17*right_height));
				this.setMinimumSize(s);
				this.setPreferredSize(s);
				this.setMaximumSize(s);
				this.setBackground(Color.WHITE);
				this.setOpaque(false);
				this.setLayout(new FlowLayout(FlowLayout.LEFT));
			}
			
			//type为match时
			ContentPanel(MatchVo vo){
				super();
				init();
				Dimension s=null;
				//String season=vo.getSeason();
				//long data=vo.getDate();
				String team1=vo.getTeam1();
				String team2=vo.getTeam2();
				int team1Score=vo.getTeam1Score();
				int team2Score=vo.getTeam2Score();
				
				JLabel time=new JLabel();
				String dateString=new SimpleDateFormat("yyyy-MM-dd").format(new Date(
						vo.getDate()));
				time.setText(dateString);
				time.setHorizontalAlignment(SwingConstants.CENTER);
				time.setFont(new Font("华文楷体", Font.BOLD, 18));
				
				JPanel team1_pic=new JPanel();//放置球队1图片
				team1_pic.setLayout(null);
				s=new Dimension((int)(0.1*right_width),(int)(0.15*right_height));
				team1_pic.setMinimumSize(s);
				team1_pic.setPreferredSize(s);
				team1_pic.setMaximumSize(s);
				
				JPanel team2_pic=new JPanel();//放置球队2图片
				team2_pic.setLayout(null);
				team2_pic.setMinimumSize(s);
				team2_pic.setPreferredSize(s);
				team2_pic.setMaximumSize(s);
				
				TeamIconPo team1Icon=new TeamIconPo();
				TeamIconPo team2Icon=new TeamIconPo();
				team1Icon.setKey(team1);
				team2Icon.setKey(team2);
				
				try {
					team1Icon=teamPicManager.queryById(team1Icon);
					team2Icon=teamPicManager.queryById(team2Icon);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				JSVGCanvas canvas = new JSVGCanvas();
				canvas.setLocation(0, 0);
				canvas.setSize(s);
				team1_pic.add(canvas);
				SVGDocument svgd;
				JSVGCanvas canvas2 = new JSVGCanvas();
				canvas2.setLocation(0, 0);
				canvas2.setSize(s);
				team2_pic.add(canvas2);
				SVGDocument svgd2;
				try {
					if(team1Icon!=null){
					svgd = new SVGManager().decodeImage(team1Icon.getPicture());
					canvas.setSVGDocument(svgd);
					}if(team2Icon!=null){
					svgd2 = new SVGManager().decodeImage(team2Icon.getPicture());
					canvas2.setSVGDocument(svgd2);
					}
				} catch (ClassNotFoundException | IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		
				JLabel team1_score=new JLabel();
				team1_score.setText(""+team1Score);
				team1_score.setHorizontalAlignment(SwingConstants.CENTER);
				team1_score.setFont(new Font("华文楷体", Font.BOLD, 18));
				
				JLabel team2_score=new JLabel();
				team2_score.setText(""+team2Score);
				team2_score.setHorizontalAlignment(SwingConstants.CENTER);
				team2_score.setFont(new Font("华文楷体", Font.BOLD, 18));
				
				s=new Dimension((int)(0.1*right_width),(int)(0.15*right_height));
				team1_score.setMinimumSize(s);
				team1_score.setPreferredSize(s);
				team1_score.setMaximumSize(s);
				team2_score.setMinimumSize(s);
				team2_score.setPreferredSize(s);
				team2_score.setMaximumSize(s);
				
				TextButton team1_name=new TextButton(team1);
				TextButton team2_name=new TextButton(team2);
				
				team1_name.setText(team1);
				team1_name.setHorizontalAlignment(SwingConstants.CENTER);
				team1_name.setFont(new Font("华文楷体", Font.BOLD, 18));
				team2_name.setText(team2);
				team2_name.setHorizontalAlignment(SwingConstants.CENTER);
				team2_name.setFont(new Font("华文楷体", Font.BOLD, 18));
				
				s=new Dimension((int)(0.1*right_width),(int)(0.15*right_height));
				team1_name.setMinimumSize(s);
				team1_name.setPreferredSize(s);
				team1_name.setMaximumSize(s);
				team2_name.setMinimumSize(s);
				team2_name.setPreferredSize(s);
				team2_name.setMaximumSize(s);

				TextButton vs=new TextButton("vs");
				vs.setText("VS");
				vs.setHorizontalAlignment(SwingConstants.CENTER);
				vs.setFont(new Font("华文楷体", Font.BOLD, 18));

				
				//JLabel data_lab=new JLabel();
				//JLabel season_lab=new JLabel();
				//Calendar c=Calendar.getInstance();
				//c.setTimeInMillis(data);
				//int year=c.get(Calendar.YEAR);
				//int month=c.get(Calendar.MONTH);
				//int day=c.get(Calendar.DAY_OF_MONTH);
				//data_lab.setText(year+"-"+month+"-"+day);
				//data_lab.setHorizontalAlignment(SwingConstants.CENTER);
				//data_lab.setFont(new Font("华文楷体", Font.BOLD, 18));
				//data_lab.setBounds(0,(int)0.5*right_width-20,40,20);
				//this.add(data_lab);
				//season_lab.setText(season);
				//season_lab.setHorizontalAlignment(SwingConstants.CENTER);
				//season_lab.setFont(new Font("华文楷体", Font.BOLD, 18));
				//season_lab.setBounds(0,0,0,0);
				//this.add(season_lab);
				this.add(time);
				this.add(team1_pic);
				this.add(team1_name);
				this.add(team1_score);
				this.add(vs);
				this.add(team2_score);
				this.add(team2_name);
				this.add(team2_pic);
				this.validate();
				
				team1_name.addActionListener(new ActionListener(){
					 public void actionPerformed(ActionEvent e){
						//TeamVo vo=new TeamVo();
						//vo.set球队简称(team1);
						//vo.set赛季(season);
						 try {
								new TeamDetailDialog(
										null,
										((TeamQueryBLService) RMIClientBLServiceManager
												.getBLService(TeamQueryBLService.class))
												.query赛季总数据ByName(vo.getTeam1(),
														getCurrentSeason()),getCurrentSeason());
							} catch (Exception e1) {
								e1.printStackTrace();
							}
					 }
				});
				
				team2_name.addActionListener(new ActionListener(){
					 public void actionPerformed(ActionEvent e){
						 //TeamVo vo=new TeamVo();
						 //vo.set球队简称(team2);
						 //vo.set赛季(season);
						 //new TeamDetailDialog(null,vo);
						 try {
								new TeamDetailDialog(
										null,
										((TeamQueryBLService) RMIClientBLServiceManager
												.getBLService(TeamQueryBLService.class))
												.query赛季总数据ByName(vo.getTeam2(),
														getCurrentSeason()),getCurrentSeason());
							} catch (Exception e1) {
								e1.printStackTrace();
							}
					 }
				});
				
				vs.addActionListener(new ActionListener(){
					 public void actionPerformed(ActionEvent e){
						 new MatchDetailDialog(null,vo);
					 }
				});
				
			}
			
			ContentPanel(PlayerVo vo){
				super();
				init();
				
				String name=vo.getName();
				String score=new DecimalFormat("#.00").format(vo.get比赛信息());
				byte[] pic=vo.getPortraitPicture();
				//String season=vo.get赛季();
				Dimension s=null;
				
				TextButton name_button=new TextButton(name);
				s=new Dimension((int)(0.4*right_width),(int)(0.18*right_height));
				name_button.setMinimumSize(s);
				name_button.setPreferredSize(s);
				name_button.setMaximumSize(s);
				
				JButton pic_button=new JButton();
				pic_button.setFocusPainted(false);
				pic_button.setBorderPainted(false);
				pic_button.setBorder(null);
				pic_button.setContentAreaFilled(false);
				pic_button.setFocusPainted(false);
				s=new Dimension((int)(0.18*right_height),(int)(0.18*right_height));
				pic_button.setMinimumSize(s);
				pic_button.setPreferredSize(s);
				pic_button.setMaximumSize(s);
				ImageIcon icon=null;
				try {
					icon = new ImageIcon(manager.decodeImage(pic));
				} catch (ClassNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				icon.setImage(icon.getImage().getScaledInstance((int)(0.18*right_width),(int)(0.18*right_height),Image.SCALE_DEFAULT));
				pic_button.setIcon(icon);
				
				JLabel score_label=new JLabel();
				s=new Dimension((int)(0.3*right_width),(int)(0.18*right_height));
				score_label.setMinimumSize(s);
				score_label.setPreferredSize(s);
				score_label.setMaximumSize(s);
				score_label.setText("场均得分:  "+score);
				score_label.setHorizontalAlignment(SwingConstants.CENTER);
				score_label.setFont(new Font("华文楷体", Font.BOLD, 18));
				
				this.add(pic_button);
				this.add(name_button);
				this.add(score_label);
				this.validate();
				
				name_button.addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent e){
						//new PlayerDetailDialog(null,vo);
						try {
							new PlayerDetailDialog(
									null,
									((PlayerQueryBLService) RMIClientBLServiceManager
											.getBLService(PlayerQueryBLService.class))
											.query赛季总数据ByName(vo.getName(),
													getCurrentSeason()),getCurrentSeason());
						} catch (Exception e1) {
							e1.printStackTrace();
						}
					}
				});
				
				pic_button.addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent e){
						try {
							new PlayerDetailDialog(
									null,
									((PlayerQueryBLService) RMIClientBLServiceManager
											.getBLService(PlayerQueryBLService.class))
											.query赛季总数据ByName(vo.getName(),
													getCurrentSeason()),getCurrentSeason());
						} catch (Exception e1) {
							e1.printStackTrace();
						}
					}
				});
			}
			
			ContentPanel(TeamVo vo){
				super();
				init();
				
				String name=vo.get球队全名();
				//String season=vo.get赛季();
				int win=vo.get胜利场数();
				int matches=vo.get比赛场数();
				int lose=matches-win;
				String rate=new DecimalFormat("#.00").format(win*1.0/matches);
				//byte[] pic=vo.getTeamIcon();
				Dimension s=null;

				TextButton name_button=new TextButton(name);
			    s=new Dimension((int)(0.25*right_width),(int)(0.17*right_height));
				name_button.setMinimumSize(s);
				name_button.setPreferredSize(s);
				name_button.setMaximumSize(s);
				
				JPanel pic_panel=new JPanel();
				pic_panel.setLayout(null);
				s=new Dimension((int)(0.12*right_width),(int)(0.12*right_height));
				pic_panel.setMinimumSize(s);
				pic_panel.setPreferredSize(s);
				pic_panel.setMaximumSize(s);
				JSVGCanvas canvas = new JSVGCanvas();
				canvas.setLocation(0, 0);
				canvas.setSize(s);
				pic_panel.add(canvas);
				SVGDocument svgd;
				TeamIconPo icon=new TeamIconPo();
				icon.setKey(vo.get球队简称());
				try {
					icon=teamPicManager.queryById(icon);
					svgd = new SVGManager().decodeImage(icon.getPicture());
					canvas.setSVGDocument(svgd);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
				JLabel win_lose_label=new JLabel();
				win_lose_label.setText("胜 / 负   "+win+" / "+lose);
				win_lose_label.setHorizontalAlignment(SwingConstants.CENTER);
				win_lose_label.setFont(new Font("华文楷体", Font.BOLD, 18));
				s=new Dimension((int)(right_width*0.35),(int)(0.17*right_height));
				win_lose_label.setMinimumSize(s);
				win_lose_label.setPreferredSize(s);
				win_lose_label.setMaximumSize(s);
				
				JLabel rate_label=new JLabel();
				rate_label.setText("胜率  "+rate);
				rate_label.setHorizontalAlignment(SwingConstants.CENTER);
				rate_label.setFont(new Font("华文楷体", Font.BOLD, 18));
				s=new Dimension((int)0.2*right_width,(int)(0.17*right_height));
				rate_label.setMinimumSize(s);
				rate_label.setPreferredSize(s);
				rate_label.setMaximumSize(s);
				
				this.add(pic_panel);
				this.add(name_button);
				this.add(win_lose_label);
				this.add(rate_label);
				this.validate();
				
				name_button.addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent e){
						 try {
								new TeamDetailDialog(
										null,
										((TeamQueryBLService) RMIClientBLServiceManager
												.getBLService(TeamQueryBLService.class))
												.query赛季总数据ByName(vo.get球队简称(),
														getCurrentSeason()),getCurrentSeason());
							} catch (Exception e1) {
								e1.printStackTrace();
							}
					}
				});
				
			}
		}
	} 
	
	public HomePagePanel(){
		this(745,380);
	}
	
	public HomePagePanel(int width,int height){
		super();
		this.setSize(width, height);
		int p_width=width;
		int p_height=height;
		this.setLocation(0, 0);
		this.setOpaque(true);
		this.setLayout(new FlowLayout());
		manager=new PictureManager("png");
		teamPicManager=DBServiceManager.getTeamIconDBService();
		matchManager=DBServiceManager.getMatchDBService();
	    pm=DBServiceManager.getPlayerMatchDBService();
	    pmd=DBServiceManager.geTeamMatchDBService();
	    hdb=DBServiceManager.getHotSpotDBService();
	    cbl=new DataComputer();
		tdb=DBServiceManager.getTeamDBService();
        tddb=DBServiceManager.getTeamGameDataDBService();
	    try {
			teambl=new TeamQueryBL(cbl,tdb,tddb,teamPicManager);
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			matchQuery=new MatchQueryBL(matchManager,pm,pmd,hdb);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		setCurrentSeason("13-14");
		
		this.width=width;
		this.height=height;
		
		addLeft(p_width,p_height);
		addRight(p_width,p_height,ButtonType.MATCH);
		
		this.add(left);
		this.add(right);
		
		LocalBLServiceManager.getRedisplayControllerBLService().register(this);
	}
	
	private void addRight(int width,int height,ButtonType type){
		right=new RightPanel(width,height,type);
		this.add(right);
	}
	
	private void addLeft(int width,int height){
        left=new LeftPanel(width,height);	
		left.setLayout(new BoxLayout(left,BoxLayout.Y_AXIS));
		this.add(left);
	}
	
	private void refresh(){
		this.repaint();
		this.validate();
	}
	
	private void remove_all(){
		this.removeAll();		
	}
	
	private void setMatch(){
		type=ButtonType.MATCH;
	}
	
	private void setPlayer(){
		type=ButtonType.PLAYER;
	}
	
	private void setTeam(){
		type=ButtonType.TEAM;
	}
	
	private void setCurrentSeason(String season){
		currentSeason=season;
	}
	
	private String getCurrentSeason(){
		return currentSeason;
	}
	
	@Override
	public void redisplay() {
		// TODO Auto-generated method stub
		remove_all();
		addLeft(width,height);
		switch(type){
		case MATCH:addRight(width,height,ButtonType.MATCH);break;
		case PLAYER:addRight(width,height,ButtonType.PLAYER);break;
		case TEAM:addRight(width,height,ButtonType.TEAM);break;
		}		
		refresh();
	}
	
	 public void paintComponent(Graphics g)
	       {
	       int x=0,y=0;
	          java.net.URL imgURL=getClass().getResource("/picture/background.jpg");
	  
	          //test.jpg是测试图片，与Demo.java放在同一目录下
	          ImageIcon icon=new ImageIcon(imgURL);
	          g.drawImage(icon.getImage(),x,y,getSize().width,getSize().height,this);
	          while(true)
	          {
	            g.drawImage(icon.getImage(),x,y,this);
	            if(x>getSize().width && y>getSize().height)break;
                //这段代码是为了保证在窗口大于图片时，图片仍能覆盖整个窗口
	            if(x>getSize().width)
	            {
	               x=0;
	               y+=icon.getIconHeight();
	            }
	            else
	             x+=icon.getIconWidth();
	          }
	       }

	
}
