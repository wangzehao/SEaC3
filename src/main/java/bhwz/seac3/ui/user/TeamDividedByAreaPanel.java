package bhwz.seac3.ui.user;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.BoxLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.apache.batik.swing.JSVGCanvas;
import org.w3c.dom.svg.SVGDocument;

import bhwz.seac3.RMIClientBLServiceManager;
import bhwz.seac3.blservice.query.TeamQueryBLService;
import bhwz.seac3.util.SVGManager;
import bhwz.seac3.util.TextButton;
import bhwz.seac3.vo.TeamVo;

/**
 * 作者 :byc
 * 创建时间：2015年4月19日 下午8:26:30
 * 类说明:
 */
public class TeamDividedByAreaPanel extends JPanel{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7854983455796965208L;
	String season;
	TeamDividedByAreaPanel pp;
	String [] team=new String [5];
    public TeamDividedByAreaPanel(String area,String season){
    	this.season=season;
    	List<TeamVo> list=getTeam(area);
		this.setSize(164, 550);
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		for(int i=0;i<5;i++){
			TeamDividedInfoPanel t=new TeamDividedInfoPanel(list.get(i));
			this.add(t);
		}
    	
    }
    public class TeamDividedInfoPanel extends JPanel{
    	private static final long serialVersionUID = -5050585949159190063L;
    	public TeamDividedInfoPanel(TeamVo team){
    	this.setBounds(0, 0, 164, 110);
    	this.setLayout(null);
    	this.setBackground(Color.WHITE);
    	
    	JSVGCanvas canvas = new JSVGCanvas();
    	canvas.setBounds(34, 5, 88, 79);
    	this.add(canvas);
    	SVGDocument svgd;

    	try {
    		svgd = new SVGManager().decodeImage(team.getTeamIcon());
    		canvas.setSVGDocument(svgd);
    	} catch (ClassNotFoundException | IOException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    	
    	TextButton nameLabel = new TextButton(team.get球队全名());
    	nameLabel.setHorizontalAlignment(SwingConstants.CENTER);
    	nameLabel.setFont(new Font("华文楷体", Font.BOLD, 16));
    	nameLabel.setBounds(0, 86, 164, 21);
    	nameLabel.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					new TeamDetailDialog(
							null,
							((TeamQueryBLService) RMIClientBLServiceManager
									.getBLService(TeamQueryBLService.class))
									.query赛季总数据ByName(team.get球队简称(),
											season),season);
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(pp, "服务器连接发生异常\n"
							+ e1.getMessage());
					e1.printStackTrace();
				}
			}
			
		});
    	this.add(nameLabel);
    }
    }

   
    
    public List<TeamVo> getTeam(String area){
    	List<TeamVo> teams = new ArrayList<TeamVo>();
    	TeamVo teamVo;
    	switch(area){
    	case"东南分区":
    		String [] temp={"ATL","CHA","MIA","ORL","WAS"};
    		team=temp;
    		break;
    	case"中央分区":
    		String [] temp1={"CHI","CLE","IND","DET","MIL"};
    		team=temp1;
    		break;
    	case"大西洋分区":
    		String [] temp2={"BOS","NYK","PHI","BKN","TOR"};
    		team=temp2;break;
    	case"西南分区":
    		String [] temp3={"SAS","MEM","HOU","DAL","NOP"};
    		team=temp3;
    		break;
    	case"西北分区":
    		String [] temp4={"MIN","DEN","UTA","POR","OKC"};
    		team=temp4;
    		break;
    	case"太平洋分区":
    		String [] temp5={"SAC","PHX","LAL","GSW","LAC"};
    		team=temp5;break;		
    	}
      for(int i=0;i<team.length;i++){
    	  String name=team[i];
    	  TeamQueryBLService hq=(TeamQueryBLService)RMIClientBLServiceManager.getBLService(TeamQueryBLService.class);
				try {
					teamVo=hq.query场均数据ByName(name, season);
					teams.add(teamVo);
				} catch (Exception e) {
					e.printStackTrace();
				}
				
      }
    	return teams;
    }
}
