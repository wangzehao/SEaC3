package bhwz.seac3.ui.user;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JPanel;

import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.util.Calendar;
import java.util.Date;

import javax.swing.JComboBox;
import javax.swing.JLabel;

public class DateFilterPanel extends JPanel {

	private static final long serialVersionUID = -7429591666501836802L;
	private static final String ALL = "- - -";
	private JComboBox<String> year;
	private JComboBox<String> month;
	private JComboBox<String> day;
	private JLabel label;
	private JLabel label_1;
	private JLabel label_2;
	private JLabel label_3;

	/**
	 * Create the panel.
	 */
	public DateFilterPanel() {
		setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		label = new JLabel("\u65E5\u671F");
		add(label);

		year = new JComboBox<String>();
		add(year);
		DefaultComboBoxModel<String> yearModel = new DefaultComboBoxModel<String>();
		yearModel.addElement(ALL);
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		int thisYear=cal.get(Calendar.YEAR);
		for (int i = thisYear; i >= 1946; i--) {
			yearModel.addElement(Integer.toString(i));
		}
		year.setModel(yearModel);

		label_1 = new JLabel("\u5E74");
		add(label_1);

		month = new JComboBox<String>();
		add(month);
		DefaultComboBoxModel<String> monthModel = new DefaultComboBoxModel<String>();
		monthModel.addElement(ALL);
		for (int i = 1; i <= 12; i++) {
			monthModel.addElement(Integer.toString(i));
		}
		month.setModel(monthModel);

		label_2 = new JLabel("\u6708");
		add(label_2);

		day = new JComboBox<String>();
		add(day);
		DefaultComboBoxModel<String> dayModel = new DefaultComboBoxModel<String>();
		dayModel.addElement(ALL);
		for (int i = 1; i <= 31; i++) {
			dayModel.addElement(Integer.toString(i));
		}
		day.setModel(dayModel);

		label_3 = new JLabel("\u65E5");
		add(label_3);
	}
	
	public boolean filte(long date){
		return filte(new Date(date));
	}

	public boolean filte(Date date) {
		try {
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			int dateYear=cal.get(Calendar.YEAR);
			int dateMonth=cal.get(Calendar.MONTH);
			int dateDay=cal.get(Calendar.DATE);
			if ((year.getSelectedItem().equals(ALL)
					||Integer.toString(dateYear).equals(year.getSelectedItem()))
					&& (month.getSelectedItem().equals(ALL)
							||Integer.toString(dateMonth+1).equals(
							month.getSelectedItem()))
					&& (day.getSelectedItem().equals(ALL)||Integer.toString(dateDay).equals(
							day.getSelectedItem()))) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}
	
	public void addActionListener(ActionListener l){
		year.addActionListener(l);
		month.addActionListener(l);
		day.addActionListener(l);
	}
}
