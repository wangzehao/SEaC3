package bhwz.seac3.ui.user;

import javax.swing.JPanel;

import java.awt.BorderLayout;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;

import bhwz.seac3.RMIClientBLServiceManager;
import bhwz.seac3.blservice.query.PlayerQueryBLService;
import bhwz.seac3.blservice.query.SeasonQueryBLService;
import bhwz.seac3.ui.user.graphics.Graphics;
import bhwz.seac3.vo.PlayerVo;

import java.lang.reflect.Method;
import java.util.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PlayerProgressPanel extends JPanel {
	
	private static final long serialVersionUID = 1L;
	private SeasonSelectComboBox seasonBox;
	private PlayerVo player;
	private JPanel chart;
	boolean allSeason;
	private Map<String, Function<PlayerVo, Double>> valueMap = new HashMap<String, Function<PlayerVo, Double>>() {

		private static final long serialVersionUID = 1L;

		{
			Class<PlayerVo> c = PlayerVo.class;
			Method[] ms = c.getMethods();
			for (Method m : ms) {
				if (m.getReturnType().getName().toLowerCase()
						.endsWith("double")) {
					String mname = m.getName();
					if (mname.startsWith("get"))
						put(mname.substring(3), vo -> {
							try {
								return (Double) (m.invoke(vo));
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
								return 0.0;
							}
						});
				}
			}
		}

	};
	private JComboBox<String> valueBox;

	public PlayerProgressPanel(PlayerVo player) {
		this.player = player;
		setLayout(new BorderLayout(0, 0));

		JPanel bar = new JPanel();
		add(bar, BorderLayout.NORTH);

		JButton button = new JButton("\u5168\u90E8\u8D5B\u5B63");
		button.addActionListener(e -> showChart(true));
		bar.add(button);

		seasonBox = new SeasonSelectComboBox();
		seasonBox.addActionListener(e->showChart(false));

		JButton button_1 = new JButton("\u6307\u5B9A\u8D5B\u5B63");
		button_1.addActionListener(e -> showChart(false));
		bar.add(button_1);

		bar.add(seasonBox);

		JLabel label = new JLabel("\u8D5B\u5B63");
		bar.add(label);

		valueBox = new JComboBox<>();
		valueBox.setModel(new DefaultComboBoxModel<String>(valueMap.keySet()
				.toArray(new String[0])));
		bar.add(valueBox);
		valueBox.addActionListener(e->showChart(allSeason));
		
		showChart(false);
	}

	public void showChart(boolean allSeason) {
		this.allSeason=allSeason;
		PlayerQueryBLService bl = RMIClientBLServiceManager
				.getBLService(PlayerQueryBLService.class);
		SeasonQueryBLService sbl = RMIClientBLServiceManager
				.getBLService(SeasonQueryBLService.class);
		try {
			List<PlayerVo> l;
			if (allSeason)
				l = bl.query各个赛季表现(player);
			else
				l = bl.query每场比赛表现(seasonBox.getSeason(), player);

			l.sort((p1,p2)->(p2.get比赛日期()-p1.get比赛日期())>0?1:-1);
			
			Function<PlayerVo, Double> sortColumn = valueMap.get(valueBox
					.getSelectedItem());
			List<Double> values = l.stream().map(sortColumn)
					.collect(Collectors.toList());
			if(chart!=null)
				this.remove(chart);
			
			if (allSeason) {
				List<String> seasons = sbl
						.queryJoinedSeasons(player);
				

				chart=(Graphics.getLineChart("全部赛季表现",
						(String) valueBox.getSelectedItem(), seasons, values,
						quartiles(seasons,bl,sortColumn).map(list -> list.get(0)).map(sortColumn)
								.collect(Collectors.toList()),
						quartiles(seasons,bl,sortColumn).map(list -> list.get(1)).map(sortColumn)
								.collect(Collectors.toList()),
						quartiles(seasons,bl,sortColumn).map(list -> list.get(2)).map(sortColumn)
								.collect(Collectors.toList()),
						quartiles(seasons,bl,sortColumn).map(list -> list.get(3)).map(sortColumn)
								.collect(Collectors.toList())));
			}else{
				List<PlayerVo> quartiles =bl.query所有球员赛季表现四分位数(seasonBox.getSeason(), vo->sortColumn.apply(vo));
				List<Date> dates=new ArrayList<Date>();
				Iterator<PlayerVo> iterator=l.iterator();
				while(iterator.hasNext()){
					dates.add(new Date(iterator.next().get比赛日期()));
				}
				chart=(Graphics.getLineChart(seasonBox.getSeason()+"赛季表现",
						(String) valueBox.getSelectedItem(), dates, values,
						sortColumn.apply(quartiles.get(0)),
						sortColumn.apply(quartiles.get(1)),
						sortColumn.apply(quartiles.get(2)),
						sortColumn.apply(quartiles.get(3))));
				
			}
			this.add(chart,BorderLayout.CENTER);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private Stream<List<PlayerVo>> quartiles(List<String> seasons,PlayerQueryBLService bl,	Function<PlayerVo, Double> sortColumn){
		return seasons
				.stream()
				.map(season -> {
					try {
					
						return bl.query所有球员赛季表现四分位数(season,
								vo -> sortColumn.apply(vo));
					} catch (Exception e) {
						return null;
					}
				});
	}
}
