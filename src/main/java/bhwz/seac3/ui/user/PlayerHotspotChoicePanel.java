package bhwz.seac3.ui.user;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import bhwz.seac3.RMIClientBLServiceManager;
import bhwz.seac3.blservice.query.HotspotQueryBLService;
import bhwz.seac3.util.ChooseButton;
import bhwz.seac3.vo.PlayerVo;

/**
 * 作者 :byc 
 * 创建时间：2015年4月2日 下午7:15:34 
 * 类说明:容纳热点筛选按钮的容器
 */
public class PlayerHotspotChoicePanel extends JPanel {
	private String 筛选条件;
	private int i;
	private String[] buttonName;
	private int buttonNumber;
	private List<PlayerVo> list;
	private PlayerHotspotPanel panel;
	private static final long serialVersionUID = -5104977669487284998L;

	public PlayerHotspotChoicePanel(String type, PlayerHotspotPanel p) {
		panel = p;
		this.setSize(752, 35);
		this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		this.setBackground(Color.GRAY);
		String[] buttonName1 = { "         得分         ", "        篮板         ",
				"        助攻        ", "        盖帽        ",
				"        抢断        " };
		String[] buttonName2 = { "场均得分", "场均篮板", "场均助攻", "场均盖帽", "场均抢断", "三分%",
				"投篮%", "罚球%" };
		String[] buttonName3 = { "          场均得分进步          ",
				"          场均篮板进步          ", "          场均助攻进步          " };
		if (type.equals("每日")) {
			buttonName = buttonName1;
		}
		if (type.equals("赛季")) {
			buttonName = buttonName2;
		}
		if (type.equals("进步")) {
			buttonName = buttonName3;
		}
		buttonNumber = buttonName.length;
		ChooseButton[] chooseButton = new ChooseButton[buttonNumber];
		for (i = 0; i < buttonNumber; i++) {
			chooseButton[i] = new ChooseButton(buttonName[i]);
			chooseButton[i].addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					
					   筛选条件=((JButton) (arg0.getSource())).getText();
					   try {
						   HotspotQueryBLService
						   hq=(HotspotQueryBLService)RMIClientBLServiceManager
						   .getBLService(HotspotQueryBLService.class);
						   list=hq.QueryPlayersRankedBy条件(筛选条件.replaceAll(" ",""));
						   panel.showInfo(list);		
						} catch (Exception e1) {
							JOptionPane.showMessageDialog(PlayerHotspotChoicePanel.this, "服务器连接发生异常\n"
									+ e1.getMessage());
							e1.printStackTrace();
						}
					  
					 
					/*System.out.println(((JButton) (arg0.getSource())).getText());
					if (((JButton) (arg0.getSource())).getText().equals(
							"         得分         ")) {
						TestPlayerHotspotPanel t = new TestPlayerHotspotPanel();
						list = t.getInfo1();
						PlayerHotspotPanel panel = new PlayerHotspotPanel();
						panel.showInfo(list);
					}
					if (((JButton) (arg0.getSource())).getText().equals(
							"        篮板         ")) {
						TestPlayerHotspotPanel t2 = new TestPlayerHotspotPanel();
						list = t2.getInfo2();
						panel.showInfo(list);
					}*/
				}
			});
			this.add(chooseButton[i]);
			this.repaint();
		}

	}

	public List<PlayerVo> getRankedInfo() {
		return list;
	}

	public void setRankedInfo(String 条件) {
		
		 筛选条件=条件;
	try {
		 HotspotQueryBLService
		 hq=(HotspotQueryBLService)RMIClientBLServiceManager
		 .getBLService(HotspotQueryBLService.class);
		 list=hq.QueryPlayersRankedBy条件(筛选条件.replaceAll(" ",""));
	} catch (Exception e1) {
		JOptionPane.showMessageDialog(PlayerHotspotChoicePanel.this, "服务器连接发生异常\n"
				+ e1.getMessage());
		e1.printStackTrace();
	}
		 
		/*if (条件.equals("         得分         ")) {
			TestPlayerHotspotPanel t = new TestPlayerHotspotPanel();
			list = t.getInfo1();

		}*/
	}

}
