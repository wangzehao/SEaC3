package bhwz.seac3.ui.user;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.SystemColor;
import javax.swing.*;

/**
 * 作者 :byc
 * 创建时间：2015年6月9日 下午3:14:31
 * 类说明:胜率条
 */
public class WinRatePanel extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1545441305208022364L;
	private int  rate1;
	private int rate2;
	public WinRatePanel(double r1,double r2){
		
		rate1=(int) (100*(r1/(r1+r2)));
		rate2=100-rate1;
		this.setSize( 340, 34);
		this.setLayout(null);
		this.setVisible(true);
		
		JLabel rate1Label = new JLabel(rate1+"%");
		rate1Label.setFont(new Font("微软雅黑", Font.PLAIN, 15));
		rate1Label.setBounds(10, 11, 35, 15);
		this.add(rate1Label);
		
		JLabel rate2Label = new JLabel(rate2+"%");
		rate2Label.setFont(new Font("微软雅黑", Font.PLAIN, 15));
		rate2Label.setBounds(299, 11, 35, 15);
		this.add(rate2Label);
		
		JPanel bluePanel = new JPanel();
		bluePanel.setBounds(61, 9, (int) (222*(r1/(r1+r2))), 17);
		this.add(bluePanel);
		bluePanel.setBackground(SystemColor.textHighlight);
		
		JPanel redPanel = new JPanel();
		redPanel.setBounds(61+(int) (222*(r1/(r1+r2))), 9, (int) (222*(1-r1/(r1+r2))), 17);
		this.add(redPanel);
		redPanel.setBackground(Color.RED);
	}
	
	@Override
	public void paintComponent(Graphics g)
    {
       java.net.URL imgURL=getClass().getResource("/picture/noback.png");
       ImageIcon icon=new ImageIcon(imgURL);
       g.drawImage(icon.getImage(),0,0,800,500,this);
    }
}
