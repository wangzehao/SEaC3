package bhwz.seac3.ui.user;


import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.text.DecimalFormat;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.apache.batik.swing.JSVGCanvas;
import org.w3c.dom.svg.SVGDocument;

import bhwz.seac3.RMIClientBLServiceManager;
import bhwz.seac3.blservice.query.TeamQueryBLService;
import bhwz.seac3.util.SVGManager;
import bhwz.seac3.util.TextButton;
import bhwz.seac3.vo.TeamVo;

/**
 * 作者 :byc
 * 创建时间：2015年4月2日 上午12:08:22
 * 类说明:排名信息条目
 */
public class TeamRankInfoPanel extends JPanel{
	String season="14-15";
	TeamRankInfoPanel pp;
	private static final long serialVersionUID = 3711145339557566298L;
    public TeamRankInfoPanel(int i,TeamVo team,String info){
		this.setSize(700, 57);
		this.setLayout(null);
		this.setBackground(Color.WHITE);
		JLabel rankLabel = new JLabel(i+1+" ");
		rankLabel.setHorizontalAlignment(SwingConstants.CENTER);
		rankLabel.setFont(new Font("幼圆", Font.BOLD, 28));
		rankLabel.setBounds(20, 10, 50, 37);
		this.add(rankLabel);
		
		JSVGCanvas canvas = new JSVGCanvas();
		canvas.setBounds(88, 8, 46, 40);
		this.add(canvas);
		SVGDocument svgd;

		try {
			svgd = new SVGManager().decodeImage(team.getTeamIcon());
			canvas.setSVGDocument(svgd);
		} catch (ClassNotFoundException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		TextButton teamNamelabel = new TextButton(team.get球队全名());
		teamNamelabel.setHorizontalAlignment(SwingConstants.CENTER);
		teamNamelabel.setFont(new Font("华文楷体", Font.BOLD, 18));
		teamNamelabel.setBounds(200, 13, 200, 30);
		this.add(teamNamelabel);
		teamNamelabel.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					new TeamDetailDialog(
							null,
							((TeamQueryBLService) RMIClientBLServiceManager
									.getBLService(TeamQueryBLService.class))
									.query赛季总数据ByName(team.get球队简称(),
											season),season);
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(pp, "服务器连接发生异常\n"
							+ e1.getMessage());
					e1.printStackTrace();
				}
			}
			
		});
		
		JLabel 赛区Label = new JLabel(team.get赛区());
		赛区Label.setFont(new Font("华文楷体", Font.BOLD, 18));
		赛区Label.setBounds(420, 12, 50, 30);
		this.add(赛区Label);
		
		JLabel 分区Label = new JLabel(team.get分区());
		分区Label.setFont(new Font("华文楷体", Font.BOLD, 18));
		分区Label.setBounds(500, 12, 100, 30);
		this.add(分区Label);
		
		info=format(Double.valueOf(info));
		JLabel infoLabel = new JLabel(info);
		infoLabel.setFont(new Font("幼圆", Font.BOLD, 22));
		infoLabel.setBounds(620, 10, 80, 37);
		this.add(infoLabel);
    }
    
    public String format(double d){
    	DecimalFormat df = new DecimalFormat("0.00");
    	return df.format(d);
    }
}
