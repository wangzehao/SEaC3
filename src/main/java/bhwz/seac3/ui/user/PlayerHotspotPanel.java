package bhwz.seac3.ui.user;

import java.awt.Color;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import bhwz.seac3.vo.PlayerVo;

/**
 * 作者 :byc 
 * 创建时间：2015年4月1日 下午5:45:44 
 * 类说明:
 */
public class PlayerHotspotPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5141207854348021454L;
	private JLabel titleLabel;
	private JPanel infoPanel;
	private String type;
	private PlayerHotspotChoicePanel p;
	private FiveRankedPlayersPanel f;

	public PlayerHotspotPanel() {
		this.setBackground(Color.WHITE);
		this.setSize(775,500);
		type = "每日";
		this.setLayout(null);
		infoPanel = new JPanel();
		infoPanel.setBackground(SystemColor.textHighlight);
		infoPanel.setBounds(5, 10, 752, 42);
		this.add(infoPanel);
		infoPanel.setLayout(new BoxLayout(infoPanel, BoxLayout.X_AXIS));

		titleLabel = new JLabel("  每日  ");
		titleLabel.setForeground(SystemColor.text);
		titleLabel.setFont(new Font("华文楷体", Font.BOLD, 22));
		infoPanel.add(titleLabel);

		JLabel label = new JLabel("热点球员");
		label.setForeground(SystemColor.text);
		label.setFont(new Font("华文楷体", Font.BOLD, 22));
		infoPanel.add(label);

		JLabel emptyLabel = new JLabel("                           ");
		emptyLabel.setFont(new Font("宋体", Font.PLAIN, 25));
		infoPanel.add(emptyLabel);

		JButton dayButton = new JButton("每日");
		dayButton.setFocusPainted(false);
		dayButton.setFont(new Font("微软雅黑", Font.PLAIN, 15));
		infoPanel.add(dayButton);
		dayButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				dayShow();
			}
		});

		JButton seasonButton = new JButton("赛季");
		seasonButton.setFocusPainted(false);
		seasonButton.setFont(new Font("微软雅黑", Font.PLAIN, 15));
		infoPanel.add(seasonButton);
		seasonButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				seasonShow();
			}
		});

		JButton improveButton = new JButton("进步最快");
		improveButton.setFocusPainted(false);
		improveButton.setFont(new Font("微软雅黑", Font.PLAIN, 15));
		infoPanel.add(improveButton);
		improveButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				improveShow();
			}
		});

		p = new PlayerHotspotChoicePanel(type, this);
		p.setLocation(5, 55);
		this.add(p);
		p.setRankedInfo("         得分         ");
		f = new FiveRankedPlayersPanel(p.getRankedInfo());
		f.setLocation(0, 93);
		this.add(f);
		this.repaint();

	}

	public void dayShow() {
		titleLabel.setText("  每日  ");
		type = "每日";
		p.setVisible(false);
		p = new PlayerHotspotChoicePanel(type, this);
		p.setLocation(5, 55);
		p.setVisible(true);
		p.setRankedInfo("         得分         ");
		f.setVisible(false);
		f = new FiveRankedPlayersPanel(p.getRankedInfo());
		f.setVisible(true);
		f.setLocation(0, 93);
		this.add(f);
		this.add(p);
		repaint();
	}

	public void seasonShow() {
		titleLabel.setText("  赛季  ");
		type = "赛季";
		p.setVisible(false);
		p = new PlayerHotspotChoicePanel(type, this);
		p.setLocation(5, 55);
		p.setVisible(true);
		p.setRankedInfo("场均得分");
		f.setVisible(false);
		f = new FiveRankedPlayersPanel(p.getRankedInfo());
		f.setVisible(true);
		f.setLocation(0, 93);
		this.add(f);
		this.add(p);
		repaint();
	}

	public void improveShow() {
		titleLabel.setText("  进步  ");
		type = "进步";
		p.setVisible(false);
		p = new PlayerHotspotChoicePanel(type, this);
		p.setLocation(5, 55);
		p.setVisible(true);
		p.setRankedInfo("          场均得分进步          ");
		f.setVisible(false);
		f = new FiveRankedPlayersPanel(p.getRankedInfo());
		f.setVisible(true);
		f.setLocation(0, 93);
		this.add(f);
		this.add(p);
		repaint();
	}

	public void showInfo(List<PlayerVo> m) {
		f.setVisible(false);
		f = new FiveRankedPlayersPanel(m);
		f.setLocation(0, 93);
		f.setVisible(true);
		this.add(f);
		repaint();
	}

}
