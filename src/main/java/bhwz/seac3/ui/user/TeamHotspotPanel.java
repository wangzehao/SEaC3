package bhwz.seac3.ui.user;

import java.awt.Color;
import java.awt.Font;
import java.awt.SystemColor;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

import bhwz.seac3.vo.TeamVo;

/**
 * 作者 :byc
 * 创建时间：2015年4月1日 下午5:46:09
 * 类说明:
 */
public class TeamHotspotPanel extends JPanel{
	private FiveRankedTeamsPanel f;
     private JPanel infoPanel;
	private static final long serialVersionUID = -6550265797584027413L;

	public TeamHotspotPanel(){
		this.setSize(780,500);
		this.setLayout(null);
		this.setBackground(Color.WHITE);
		infoPanel = new JPanel();
		infoPanel.setBackground(SystemColor.textHighlight);
		infoPanel.setBounds(5, 10, 752, 42);
		this.add(infoPanel);
		infoPanel.setLayout(new BoxLayout(infoPanel, BoxLayout.X_AXIS));
		
		JLabel titleLabel = new JLabel("  赛季  热点球队");
		titleLabel.setForeground(SystemColor.text);
		titleLabel.setFont(new Font("华文楷体", Font.BOLD, 22));
		infoPanel.add(titleLabel);
		
		TeamHotspotChoicePanel p=new TeamHotspotChoicePanel(this);
    	p.setLocation(5, 55);
    	p.setVisible(true);
    	p.setRankedInfo("场均得分");
		f = new FiveRankedTeamsPanel(p.getRankedInfo());
		f.setLocation(0, 93);
		this.add(f);
    	this.add(p);
		repaint();

     }
	
	 public List<TeamVo> getInfo(){
	    	TeamHotspotChoicePanel p=new TeamHotspotChoicePanel(this);
	    	return p.getRankedInfo();
	    }
	    public void showInfo(List<TeamVo> m){
	    	f.setVisible(false);
			f = new FiveRankedTeamsPanel(m);
			f.setLocation(0, 93);
			f.setVisible(true);
			this.add(f);
			repaint();
	    }
}
