package bhwz.seac3.ui.user;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

/**
 * 作者 :byc 
 * 创建时间：2015年4月19日 下午2:20:14 
 * 类说明:热点界面
 */
public class HotspotPanel extends JPanel {
	private static final long serialVersionUID = 5758602719429268487L;
	private JScrollPane scrollPane;
	private JPanel panel;

	public HotspotPanel() {
		this.setLayout(null);
		panel=new JPanel();
		panel.setPreferredSize(new Dimension(780, 800));
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		panel.setBackground(Color.WHITE);
		scrollPane = new JScrollPane();
		scrollPane.setViewportView(panel);
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		this.add(scrollPane);
		scrollPane.setBounds(0, 0, 780, 430);
		
		JPanel pp = new PlayerHotspotPanel();
		panel.add(pp);
	    JPanel tp = new TeamHotspotPanel();
		panel.add(tp);
		this.repaint();
	}
}
