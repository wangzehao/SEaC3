package bhwz.seac3.ui.user;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;

import javax.swing.*;

import org.apache.batik.swing.JSVGCanvas;

/**
 * 作者 :byc
 * 创建时间：2015年6月9日 上午10:32:59
 * 类说明:自定义显示图片加文字的下拉框
 */
@SuppressWarnings("rawtypes")
class ComplexCellRenderer implements ListCellRenderer {
	  protected DefaultListCellRenderer defaultRenderer = new DefaultListCellRenderer();

	  @Override
	public Component getListCellRendererComponent(JList list, Object value, int index,
	      boolean isSelected, boolean cellHasFocus) {
		 Font theFont=new Font("微软雅黑", Font.PLAIN, 20);
	    JSVGCanvas s = null;
	    String theText = null;

	    JPanel renderer = (JPanel) defaultRenderer.getListCellRendererComponent(list, value, index,
	        isSelected, cellHasFocus);
	    renderer.setLayout(null);
	    if (value instanceof Object[]) {
	      Object values[] = (Object[]) value;
	      s = (JSVGCanvas) values[0];
	      theText = (String) values[1];
	    } else {
	      theText = "";
	    }
	    if (!isSelected) {
	      renderer.setForeground(Color.gray);
	    }
	    if (s != null) {
	    	s.setBounds(10, 2, 33, 30);
	      renderer.add(s);
	    }
	    JLabel label=new JLabel();
	    label.setText(theText);
	    label.setFont(theFont);
	    renderer.add(label);
	    return renderer;
	  }
}