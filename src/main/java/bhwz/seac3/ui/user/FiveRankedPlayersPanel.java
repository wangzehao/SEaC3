package bhwz.seac3.ui.user;

import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JPanel;

import bhwz.seac3.vo.PlayerVo;

/**
 * 作者 :byc 创建时间：2015年4月2日 上午10:36:35 类说明:装载前五数据信息的容器
 */
public class FiveRankedPlayersPanel extends JPanel {
	private PlayerVo player;
	private String info;
	private List<PlayerVo> d;
	private static final long serialVersionUID = -4386915775724143165L;

	public FiveRankedPlayersPanel(List<PlayerVo> data) {
		d=data;
		this.setSize(700, 285);
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.setVisible(true);
		if (d != null) {
			PlayerRankInfoPanel[] players = new PlayerRankInfoPanel[d.size()];
			for (int i = 0; i < players.length; i++) {
				player = d.get(i);
				info = player.get比赛信息() + "";
				players[i] = new PlayerRankInfoPanel(i, player, info,"14-15");
				this.add(players[i]);
			}
		}
		this.repaint();
	}


}
