package bhwz.seac3.ui.user;


import bhwz.seac3.RMIClientBLServiceManager;
import bhwz.seac3.blservice.query.PlayerQueryBLService;
import bhwz.seac3.blservice.query.TeamQueryBLService;
import bhwz.seac3.util.TextButton;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.text.DecimalFormat;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import bhwz.seac3.util.PictureManager;
import bhwz.seac3.vo.PlayerVo;

/**
 * 作者 :byc
 * 创建时间：2015年4月2日 上午12:08:22
 * 类说明:排名信息条目
 */
public class PlayerRankInfoPanel extends JPanel{
	String season;
	PlayerRankInfoPanel pp;
	private static final long serialVersionUID = 3711145339557566298L;
    public PlayerRankInfoPanel(int i,PlayerVo player,String info,String season){
    	this.season=season;
    	pp=this;
    	this.setBackground(Color.WHITE);
    	this.setLayout(null);
		this.setSize(700, 57);
		
		JLabel rankLabel = new JLabel(i+1+"");
		rankLabel.setHorizontalAlignment(SwingConstants.CENTER);
		rankLabel.setFont(new Font("幼圆", Font.BOLD, 28));
		rankLabel.setBounds(20, 10, 29, 37);
		this.add(rankLabel);
		
		
		JLabel playerImageLabel = new JLabel("");
		playerImageLabel.setBounds(88, 10, 46, 37);
		ImageIcon ii = null;
		try {
			ii = new ImageIcon(new PictureManager("png").decodeImage(player
					.getPortraitPicture()));
		} catch (ClassNotFoundException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ii.setImage(ii.getImage().getScaledInstance(40,30,Image.SCALE_DEFAULT));
		playerImageLabel.setIcon(ii);
		this.add(playerImageLabel);
		
		TextButton textButton=new TextButton(player.getName());
		textButton.setHorizontalAlignment(SwingConstants.CENTER);
		textButton.setFont(new Font("华文楷体", Font.BOLD, 18));
		textButton.setBounds(150, 13, 150, 30);
		this.add(textButton);
		textButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					new PlayerDetailDialog(
							null,
							((PlayerQueryBLService) RMIClientBLServiceManager
									.getBLService(PlayerQueryBLService.class))
									.query赛季总数据ByName(player.getName(),
											season),season);
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(pp, "服务器连接发生异常\n"
							+ e1.getMessage());
					e1.printStackTrace();
				}
			}
		});
		
	
		
		JLabel positionLabel = new JLabel(player.getPosition());
		positionLabel.setFont(new Font("华文楷体", Font.BOLD, 18));
		positionLabel.setBounds(393, 13, 46, 30);
		this.add(positionLabel);
		
		TextButton textButton2=new TextButton(player.get所属球队());
		textButton2.setHorizontalAlignment(SwingConstants.CENTER);
		textButton2.setFont(new Font("华文楷体", Font.BOLD, 18));
		textButton2.setBounds(481, 13, 134, 30);
		this.add(textButton2);
		textButton2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					new TeamDetailDialog(
							null,
							((TeamQueryBLService) RMIClientBLServiceManager
									.getBLService(TeamQueryBLService.class))
									.query赛季总数据ByName(player.get所属球队(),
											season),season);
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(pp, "服务器连接发生异常\n"
							+ e1.getMessage());
					e1.printStackTrace();
				}
			}
		});
		info=format(Double.valueOf(info));
		JLabel infoLabel = new JLabel(info);
		infoLabel.setFont(new Font("幼圆", Font.BOLD, 22));
		infoLabel.setBounds(640, 10, 60, 37);
		this.add(infoLabel);
    }
    
    public String format(double d){
    	DecimalFormat df=new DecimalFormat("0.00");
    	return df.format(d);
    }
}
