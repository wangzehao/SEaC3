package bhwz.seac3.ui.admin;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;

import bhwz.seac3.bl.import_.crawler.DataPersistenceFactory;
import bhwz.seac3.bl.import_.crawler.NBACrawler;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ImportFromInternetDialog extends JDialog {

	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JTextField textField;
	private JTextField textField_1;

	/**
	 * Create the dialog.
	 */
	public ImportFromInternetDialog() {
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setLayout(new FlowLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		{
			JLabel label = new JLabel("\u5F00\u59CB\u8D5B\u5B63\uFF1A");
			contentPanel.add(label);
		}
		{
			textField = new JTextField("2012-13");
			contentPanel.add(textField);
			textField.setColumns(10);
		}
		{
			JLabel label = new JLabel("\u7ED3\u675F\u8D5B\u5B63\uFF1A");
			contentPanel.add(label);
		}
		{
			textField_1 = new JTextField("2013-14");
			contentPanel.add(textField_1);
			textField_1.setColumns(10);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						NBACrawler crawler=new NBACrawler(textField.getText(),textField_1.getText());
						crawler.concurrentCrawling("http://www.basketball-reference.com/leagues/", DataPersistenceFactory.create("DataPersistenceInDatabase"));
						ImportFromInternetDialog.this.dispose();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						ImportFromInternetDialog.this.dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}

}
