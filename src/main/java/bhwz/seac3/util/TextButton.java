package bhwz.seac3.util;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;

/**
 * 作者 :byc 
 * 创建时间：2015年4月1日 上午10:34:34 
 * 类说明:自定义文字按钮
 */
public class TextButton extends JButton {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7219519473170927856L;

	public TextButton(String text) {
		this.setText(text);
		this.setFont(new Font("华文楷体", Font.PLAIN, 20));
		this.setBorder(null);
		this.setContentAreaFilled(false);
		this.setForeground(Color.BLACK);
		this.setFocusPainted(false);
		this.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				setForeground(Color.BLUE);
			}

			@Override
			public void mouseExited(MouseEvent e) {
				setForeground(Color.BLACK);
			}
		});
	}
}
