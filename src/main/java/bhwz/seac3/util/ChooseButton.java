package bhwz.seac3.util;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JButton;

/**
 * 作者 :byc
 * 创建时间：2015年4月2日 下午7:07:10
 * 类说明:热点筛选条件按钮
 */
public class ChooseButton extends JButton{

	private static final long serialVersionUID = 4638902315529445145L;
	public ChooseButton(String text){
		this.setContentAreaFilled(false);
		this.setText(text);
		this.setBorderPainted(false);
		this.setFocusPainted(false);
		this.setBackground(Color.GRAY);
		this.setFont(new Font("华文楷体", Font.BOLD, 16));
		this.setForeground(Color.WHITE);
		this.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				setBackground(Color.DARK_GRAY);
				
			}
			@Override
			public void mouseExited(MouseEvent e) {
				setBackground(Color.GRAY);
			}
		});
	
	}

}
