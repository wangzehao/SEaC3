package bhwz.seac3.util;

import java.util.List;
import java.util.function.Function;

public class ListSorter {
	public static enum SortOrder{
		ESC,DESC
	}
	/**
	 * 
	 * @param order
	 * order="esc" or "desc"<br>
	 * if not use default value "desc"
	 */
	public static <T> List<T> sort(List<T> list,Function<T, Comparable<?>> sortColumn,SortOrder order){
		list.sort((t1,t2)->{
			@SuppressWarnings("unchecked")
			int sortValue=((Comparable<Object>)sortColumn.apply(t1)).compareTo(sortColumn.apply(t2));
			if(order.equals(SortOrder.ESC))
				return sortValue;
			else
				return -sortValue;			
		});
		return list;
	}
}
