package bhwz.seac3.util;

public class TeamMatchWin {
	
	private int win;
	private int matches;
	
	public void winAGame(){
		win++;
		matches++;
	}
	
	public void LoseAGame(){
		matches++;
	}
	
	public double getWinPer(){
		return win*1.0/matches;
	}
}
