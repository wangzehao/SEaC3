package bhwz.seac3.util;

import java.util.Vector;

import javax.swing.table.DefaultTableModel;

public class SortableTableModel extends DefaultTableModel {
	private static final long serialVersionUID = -6458606807214734790L;

	public SortableTableModel() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SortableTableModel(int rowCount, int columnCount) {
		super(rowCount, columnCount);
		// TODO Auto-generated constructor stub
	}

	public SortableTableModel(Object[] columnNames, int rowCount) {
		super(columnNames, rowCount);
		// TODO Auto-generated constructor stub
	}

	public SortableTableModel(Object[][] data, Object[] columnNames) {
		super(data, columnNames);
		// TODO Auto-generated constructor stub
	}

	public SortableTableModel(Vector<?> columnNames, int rowCount) {
		super(columnNames, rowCount);
		// TODO Auto-generated constructor stub
	}

	public SortableTableModel(Vector<?> data, Vector<?> columnNames) {
		super(data, columnNames);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Class<?> getColumnClass(int column) {
		Class<?> returnValue;
		if ((column >= 0) && (column < getColumnCount())) {
			try {
				returnValue = getValueAt(0, column).getClass();
			} catch (Exception e) {
				returnValue = Object.class;
			}
		} else {
			returnValue = Object.class;
		}
		return returnValue;
	}
};