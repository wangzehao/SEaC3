package bhwz.seac3.util;

import java.awt.image.BufferedImage;
import java.io.*;

import javax.imageio.*;

public class PictureManager {
	private String imageType;

	public PictureManager(String imageType) {
		this.imageType = imageType;
	}

	public byte[] encodeImage(String path) {
		if (path == null)
			return null;
		File f = new File(path);
		BufferedImage image;
		try {
			image = ImageIO.read(f);
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write(image, imageType, baos);
			return baos.toByteArray();
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	public BufferedImage decodeImage(byte[] image)
			throws ClassNotFoundException, IOException {
		if (image == null)
			return null;
		ByteArrayInputStream bais = new ByteArrayInputStream(image);
		try {
			return ImageIO.read(bais);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
