package bhwz.seac3.util;

public class ComparableData implements Comparable<ComparableData> {
	
	private String name;
	private String season;
	private double data;
	
	@Override
	public int compareTo(ComparableData o) {
		// TODO Auto-generated method stub
		if(this.getData()<o.getData()){
			return -1;
		}else if(this.getData()==o.getData()){
			return 0;
		}else{
			return 1;
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSeason() {
		return season;
	}

	public void setSeason(String season) {
		this.season = season;
	}

	public double getData() {
		return data;
	}

	public void setData(double data) {
		this.data = data;
	}
	
	
	

}
