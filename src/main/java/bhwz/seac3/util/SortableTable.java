package bhwz.seac3.util;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

public class SortableTable extends JTable {
	private static final long serialVersionUID = 457619097444252742L;

	private static final DecimalFormat df = new DecimalFormat("0.00");

	public SortableTable(TableModel model) {
		super(model);
		TableRowSorter<TableModel> sorter = new TableRowSorter<TableModel>(
				model);
		this.setRowSorter(sorter);
		this.setShowHorizontalLines(false);
		this.setShowVerticalLines(false);
		this.getTableHeader().setOpaque(false);
		this.getTableHeader().setBorder(null);
		this.getTableHeader().setBackground(Color.gray);
		this.getTableHeader().setForeground(Color.WHITE);
		this.getTableHeader().setReorderingAllowed(false);// 表格列不可移动
		DefaultTableCellRenderer hr = (DefaultTableCellRenderer) this
				.getTableHeader().getDefaultRenderer();
		hr.setHorizontalAlignment(SwingConstants.CENTER);// 列名居中
		DefaultTableCellRenderer r = new DefaultTableCellRenderer();
		r.setHorizontalAlignment(SwingConstants.CENTER);
		for (int i = 0; i < this.getColumnCount(); i++) {
			TableColumn column = this.getColumn(this.getColumnName(i));
			column.setCellRenderer(r);
		}// 数据居中
		this.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseClicked(MouseEvent e) {
				if (listenerReceiveDataMap == null
						|| listenerReceiveDataMap.isEmpty())
					return;
				if (e.getClickCount() >= 2) {
					int selectedRow = SortableTable.this.getSelectedRow();
					if (selectedRow != -1) {
						int modelRow = SortableTable.this
								.convertRowIndexToModel(selectedRow);
						listenerReceiveDataMap.forEach((l, ci) -> {
							l.onDoubleClick(ci
									.stream()
									.map(column -> SortableTable.this
											.getModel().getValueAt(modelRow,
													column)).toArray());
						});
					}
				}
			}
		});
		for (int i = 0; i < this.getColumnCount(); i++) {
			this.getColumnModel().getColumn(i)
					.setCellRenderer(new DefaultTableCellRenderer() {
						private static final long serialVersionUID = 1L;

						{
							setHorizontalAlignment(SwingConstants.CENTER);
						}
						@Override
						public Component getTableCellRendererComponent(
								JTable table, Object value, boolean isSelected,
								boolean hasFocus, int row, int column) {
							Component result = super
									.getTableCellRendererComponent(table,
											value, isSelected, hasFocus, row,
											column);
							if (value instanceof Double) {
								setText(df.format(value));
							}
							return result;
						}
					});
			
		}

	}

	@Override
	public boolean isCellEditable(int row, int column) {
		return false;
	};

	public interface DoubleClickOnRowDataListener {
		public void onDoubleClick(Object... o);
	}

	private Map<DoubleClickOnRowDataListener, List<Integer>> listenerReceiveDataMap;

	public void addDoubleClickOnDataListener(
			DoubleClickOnRowDataListener listener, int... returnDataColumns) {
		if (listener == null)
			return;
		List<Integer> columns = new ArrayList<Integer>();
		for (int i = 0; i < returnDataColumns.length; i++) {
			columns.add(returnDataColumns[i]);
		}
		for (Integer c : columns) {
			if (c < 0 || c > this.getColumnCount())
				return;
		}
		if (listenerReceiveDataMap == null)
			listenerReceiveDataMap = new HashMap<>();
		listenerReceiveDataMap.put(listener, columns);
	}

	public void removeDoubleClickOnDataListener(
			DoubleClickOnRowDataListener listener) {
		if (listener == null)
			return;
		if (listenerReceiveDataMap == null)
			return;
		listenerReceiveDataMap.remove(listener);
	}

}
