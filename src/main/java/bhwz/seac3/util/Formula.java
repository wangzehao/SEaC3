package bhwz.seac3.util;

/**
 * 代替之前的MatchInfoComputer类, 对参数类型进行了修改，公式进行了简化
 */

public class Formula {

	public double playerProgress(double a_five, double a_rest) {
		if (a_rest <= 0) {
			return 0;
		}
		return (a_five - a_rest) / a_rest;
	}

	public double teamATKReboundEff(double ATKRebound, double rDEFRebound) {
		if (ATKRebound + rDEFRebound == 0) {
			return 0;
		}
		return ATKRebound / (ATKRebound + rDEFRebound);
	}

	public double teamDEFReboundEff(double DEFRebound, double rATKRebound) {
		if (DEFRebound + rATKRebound == 0) {
			return 0;
		}
		return DEFRebound / (DEFRebound + rATKRebound);
	}

	public double teamFieldGoalPer(double fieldGoal, double shot) {
		if (shot == 0) {
			return 0;
		}
		return fieldGoal / shot;
	}

	public double teamWinPer(double winMatches, int matches) {
		if (matches == 0) {
			return 0;
		}
		return winMatches / matches;
	}

	public double teamATKRoundNum(double shot, double freeThrow,
			double ATKRebound, double RivalDEFRebound, double fieldGoal,
			double turnover) {
		if (ATKRebound + RivalDEFRebound == 0) {
			return 0;
		}
		return shot
				+ 0.4
				* freeThrow
				- 1.07
				* (ATKRebound / (ATKRebound + RivalDEFRebound) * (shot - fieldGoal))
				+ 1.07 * turnover;
	}

	public double teamATKEffPer(double ATKRound, double score) {
		if (ATKRound == 0) {
			return 0;
		}
		return 100.0/ATKRound * score;
	}

	public double teamDEFEffPer(double rATKRound, double rScore) {
		if (rATKRound == 0) {
			return 0;
		}
		return 100 * rScore / rATKRound;
	}

	public double teamReboundEffPer(double rebound, double rivalRebound) {
		if (rivalRebound == 0) {
			return 0;
		}
		return rebound / (rivalRebound + rebound);
	}

	public double teamStealEffPer(double rATKRound, double steal) {
		if (rATKRound == 0) {
			return 0;
		}
		return 100 * steal / rATKRound;
	}

	public double teamAssistPer(double ATKRound, double assist) {
		return 100 * assist / ATKRound;
	}

	public double playerFieldGoalPer(double fieldGoal, double shot) {
		if (shot == 0) {
			return 0;
		}
		return fieldGoal / shot;
	}

	public double playerEffPer(double score, double rebound, double assist,
			double steal, double block, double shot, double fieldGoal,
			double foulShot, double foulFieldGoal, double turnover) {
		return (score + rebound + assist + steal + block) - (shot - fieldGoal)
				- (foulShot - foulFieldGoal) - turnover;
	}

	public double playerGmScPer(double score, double fieldGoal, double shot,
			double throwShot, double throwFieldGoal, double ATKRebound,
			double DEFRebound, double steal, double assist, double block,
			double foul, double turnover) {
		return score + 0.4 * fieldGoal - 0.7 * shot - 0.4
				* (throwShot - throwFieldGoal) + 0.7 * ATKRebound + 0.3
				* DEFRebound + steal + 0.7 * assist + 0.7 * block - 0.4 * foul
				- turnover;
	}

	public double playerRealFieldGoalPer(double score, double shot,
			double throwShot) {
		if (shot + 0.44 * throwShot == 0) {
			return 0;
		}
		return score / (2 * (shot + 0.44 * throwShot));
	}

	public double playerShotEffPer(double fieldGoal,
			double threePointFieldGoal, double shot) {
		if (shot == 0) {
			return 0;
		}
		return (fieldGoal + 0.5 * threePointFieldGoal) / shot;
	}

	public double playerReboundPer(double playerRebound, double teamTime,
			double playerTime, double teamRebound, double rivalRebound) {
		if (playerTime == 0 || teamRebound + rivalRebound == 0) {
			return 0;
		}
		return playerRebound * (teamTime / 5) / playerTime
				/ (teamRebound + rivalRebound);
	}

	public double playerATKReboundPer(double playerATKRebound, double teamTime,
			double playerTime, double teamATKRebound, double rivalATKRebound) {
		if (playerTime == 0 || teamATKRebound + rivalATKRebound == 0) {
			return 0;
		}
		return playerATKRebound * (teamTime / 5) / playerTime
				/ (teamATKRebound + rivalATKRebound);
	}

	public double playerDEFReboundPer(double playerDEFRebound, double teamTime,
			double playerTime, double teamDEFRebound, double rivalDEFRebound) {
		if (playerTime == 0 || teamDEFRebound + rivalDEFRebound == 0) {
			return 0;
		}
		return playerDEFRebound * (teamTime / 5) / playerTime
				/ (teamDEFRebound + rivalDEFRebound);
	}

	public double playerAssistPer(double assist, double playerTime,
			double teamTime, double teamFieldGoal, double playerFieldGoal) {
		if (teamFieldGoal==0|| teamTime==0||teamFieldGoal==0||playerTime==0) {
			return 0;
		}
		return assist
				/ (playerTime / (teamTime / 5) * teamFieldGoal - playerFieldGoal);
	}

	public double playerStealPer(double steal, double playerTime,
			double teamTime, double rivalATKRound) {
		if (playerTime == 0 || rivalATKRound == 0) {
			return 0;
		}
		return steal * (teamTime / 5) / playerTime / rivalATKRound;
	}

	public double playerBlockPer(double block, double playerTime,
			double teamTime, double rivalTwoPoint) {
		if (playerTime == 0 || rivalTwoPoint == 0) {
			return 0;
		}
		return block * (teamTime / 5) / playerTime / rivalTwoPoint;
	}

	public double playerTurnoverPer(double turnover, double twoPointShot,
			double foulShot) {
		if (twoPointShot + 0.44 * foulShot + turnover == 0) {
			return 0;
		}
		return turnover / (twoPointShot + 0.44 * foulShot + turnover);
	}

	public double playerWorkPer(double shot, double throwShot, double turnover,
			double teamTime, double playerTime, double teamShot,
			double teamThrowShot, double teamTurnover) {
		if (playerTime == 0
				|| teamShot + 0.44 * teamThrowShot + teamTurnover == 0) {
			return 0;
		}
		return (shot + 0.44 * throwShot + turnover) * (teamTime / 5)
				/ playerTime / (teamShot + 0.44 * teamThrowShot + teamTurnover);
	}

	public double average(double data, int matches) {
		return data / matches;
	}

}
