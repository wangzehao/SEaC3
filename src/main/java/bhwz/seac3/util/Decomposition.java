package bhwz.seac3.util;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;

import bhwz.seac3.po.MatchPo;
import bhwz.seac3.po.PlayerCaculationDataPo;
import bhwz.seac3.po.PlayerGameDataPo;
import bhwz.seac3.po.PlayerScoreTablePo;
import bhwz.seac3.po.TeamGameDataPo;
import bhwz.seac3.vo.MatchVo;
import bhwz.seac3.vo.PlayerScoreTableVo;
import bhwz.seac3.vo.PlayerVo;
import bhwz.seac3.vo.TeamVo;

public class Decomposition {
	
	public List<TeamVo> mtot(List<MatchPo> m_po, String teamName) {
		List<TeamVo> t_vo = new LinkedList<TeamVo>();
		Iterator<MatchPo> itor = m_po.iterator();
		TeamVo his = new TeamVo();
		his.set球队简称(teamName);
		while (itor.hasNext()) {
			TeamVo temp = mtot(itor.next(), teamName, his);
			
			t_vo.add(temp);
		}
		return t_vo;
	}

	public TeamVo mtot(MatchPo m_po, String teamName, TeamVo his) {
		String season = m_po.getSeason();
		TeamVo tvo = new TeamVo();
		his.set赛季(season);
		tvo.set球队简称(teamName);
		tvo.set赛季(season);
		TeamVo rival = null;
		if (m_po.getTeam1().equals(teamName)) {
			rival = getRival(m_po, teamName, season);
			tvo = pstot(m_po.getTeam1PlayersScore(), tvo, his, rival);
		}
		if (m_po.getTeam2().equals(teamName)) {
			rival = getRival(m_po, teamName, season);
			tvo = pstot(m_po.getTeam2PlayersScore(), tvo, his, rival);
		}
		tvo.set比赛日期(m_po.getDate());
		return tvo;
	}

	public TeamVo getRival(MatchPo m_po, String name, String season) {
		Iterator<PlayerScoreTablePo> itor = null;
		if (m_po.getTeam1().equals(name)) {
			itor = m_po.getTeam2PlayersScore().iterator();
		} else {
			itor = m_po.getTeam1PlayersScore().iterator();
		}
		TeamVo rival = new TeamVo();
		rival.set球队简称(name);
		rival.set赛季(season);
		double score=0;
		double defRebound=0;
		//double atkRound=0;
		double shot=0;
		double freethrow=0;
		double fieldgoal=0;
		double atkRebound=0;
		double turnover=0;
		PlayerScoreTablePo temp=null;
		while (itor.hasNext()) {
			temp=itor.next();
			//得分
            score+=temp.get个人得分();			
			// 后场篮板数
            defRebound+=temp.get防守篮板数();
			//进攻回合(放弃)
            //atkRound=f.teamATKRoundNum(shot, freeThrow, ATKRebound, RivalDEFRebound, fieldGoal, turnover);
            //出手数
            shot+=temp.get投篮出手数();
            //罚球数
            freethrow+=temp.get罚球出手数();
            //命中数
            fieldgoal+=temp.get罚球命中数();
            //失误数
            turnover+=temp.get失误数();
			//进攻篮板数
            atkRebound+=temp.get进攻篮板数();
		}
		rival.set比赛得分(score);
		rival.set后场篮板数(defRebound);
		rival.set投篮数(shot);
		rival.set罚球数(freethrow);
		rival.set前场篮板数(atkRebound);
		rival.set投篮命中数(fieldgoal);
		rival.set失误数(turnover);
		return rival;
	}

	public TeamVo pstot(Set<PlayerScoreTablePo> score,TeamVo vo,TeamVo his,TeamVo rival){
		Iterator<PlayerScoreTablePo> itor=score.iterator();
		Formula f=new Formula();
		
		//历史数据
		int 胜利场数=his.get胜利场数();                    //ok
		int 比赛场数=his.get比赛场数(); 					  //ok
		
		//对手数据
		double 对手得分=rival.get比赛得分();
		double 对手防守篮板=rival.get后场篮板数();
		double 对手出手数=rival.get投篮数();
		double 对手罚球数=rival.get罚球数();
		double 对手命中数=rival.get投篮命中数();
		double 对手失误数=rival.get失误数();
		double 对手进攻篮板=rival.get前场篮板数();
		
		double 投篮数=0;							     //ok
		double 罚球数=0; 							     //ok
		double 失误数=0;								 //ok
		double 断球数=0;                               //ok
		double 助攻数=0;                               //ok
		double 盖帽数=0;                               //ok
		double 犯规数=0;                               //ok
		double 比赛得分=0;                              //ok
		double 投篮命中数=0;                             //ok
		double 三分命中数=0;                            //ok
		double 三分出手数=0;                            //ok
		double 罚球命中数=0;                            //ok
		double 前场篮板数=0;                            //ok
		double 后场篮板数=0;	                         //ok
		//from DataComputer
		double 命中率=0;								 //ok
		double 三分命中率=0;						     //ok
		double 罚球命中率=0; 						     //ok
		double 篮板数=0;                               //ok
		double 胜率;                                  //ok
		double 进攻效率=0;								//ok
		double 防守效率=0;                             //ok
		double 进攻篮板效率=0;                          //ok
		double 防守篮板效率=0;                          //ok
		double 抢断效率=0;                             //ok
		double 助攻率=0;                              //ok
		
		
		while(itor.hasNext()){
		 PlayerScoreTablePo temp=itor.next();
			//直接获得
	        投篮命中数+=temp.get投篮命中数();
	        投篮数+=temp.get投篮出手数();
		    三分命中数+=temp.get三分命中数();
		    三分出手数+=temp.get三分出手数();
		    罚球命中数+=temp.get罚球命中数();
		    罚球数+=temp.get罚球出手数();
		    前场篮板数+=temp.get进攻篮板数();
		    后场篮板数+=temp.get防守篮板数();
		    篮板数+=前场篮板数+后场篮板数;
		    助攻数+=temp.get助攻数();
		    断球数+=temp.get抢断数();
		    盖帽数+=temp.get盖帽数();
		    失误数+=temp.get失误数();
		    犯规数+=temp.get犯规数();
		    比赛得分+=temp.get个人得分();
		}
		
		if(比赛得分>对手得分){
			胜利场数+=1;
		}
		比赛场数+=1;
		胜率=f.teamWinPer(胜利场数, 比赛场数);
		
		命中率=f.teamFieldGoalPer(投篮命中数, 投篮数);
		三分命中率=f.teamFieldGoalPer(三分命中数, 三分出手数);
		罚球命中率=f.teamFieldGoalPer(罚球命中数, 罚球数);
		
		double 进攻回合=f.teamATKRoundNum(投篮命中数, 罚球数, 前场篮板数, 对手防守篮板, 命中率, 失误数);
		进攻效率=f.teamATKEffPer(进攻回合, 比赛得分);
        
		double 对手进攻回合=f.teamATKRoundNum(对手出手数, 对手罚球数, 对手进攻篮板, 后场篮板数, 对手命中数, 对手失误数);
		防守效率=f.teamDEFEffPer(对手进攻回合, 对手得分);
		
		进攻篮板效率=f.teamATKReboundEff(前场篮板数, 对手防守篮板);
		防守篮板效率=f.teamDEFReboundEff(后场篮板数, 对手进攻篮板);
		
		抢断效率=f.teamStealEffPer(对手进攻回合, 断球数);
		助攻率=f.teamAssistPer(进攻回合, 助攻数);
		
		//设置历史信息
		his.set比赛场数(比赛场数);
		his.set胜利场数(胜利场数);
		
		//设置vo信息
		vo.set投篮命中数(投篮命中数);
        vo.set投篮数(投篮数);     
        vo.set三分命中数(三分命中数);
        vo.set三分出手数(三分出手数);
        vo.set罚球命中数(罚球命中数);
        vo.set罚球数(罚球数);
        vo.set前场篮板数(前场篮板数);
        vo.set后场篮板数(后场篮板数);
        vo.set篮板数(篮板数);
        vo.set助攻数(助攻数);
        vo.set断球数(断球数);
        vo.set盖帽数(盖帽数);
        vo.set失误数(失误数);
        vo.set犯规数(犯规数);
        vo.set比赛得分(比赛得分);
        vo.set命中率(命中率);
        vo.set三分命中率(三分命中率);
        vo.set罚球命中率(罚球命中率);
        vo.set篮板数(篮板数);
        vo.set胜率(胜率);
        vo.set进攻效率(进攻效率);
        vo.set防守效率(防守效率);
        vo.set进攻篮板效率(进攻篮板效率);
        vo.set防守篮板效率(防守篮板效率);
        vo.set抢断效率(抢断效率);
        vo.set助攻率(助攻率);
        
        vo.set对手得分((int)对手得分);
		
		return vo;
		
	}
	
	public PlayerVo mtop(MatchVo match,PlayerVo player,Queue<PlayerVo> five,Queue<PlayerVo> rest){
		String name=player.getName();
		TeamDetail ownTeam=new TeamDetail();
		TeamDetail rivalTeam=new TeamDetail();
		TeamDetail tempTeam=new TeamDetail();
		TeamDetail tempTeam2=new TeamDetail();
		PlayerScoreTableVo pst=null;
		boolean isFound=false;
		
		Set<PlayerScoreTableVo> score1=match.getTeam1PlayersScore();
		Iterator<PlayerScoreTableVo> itor=score1.iterator();
		while(itor.hasNext()){
			PlayerScoreTableVo temp=itor.next();
			if(temp.get球员名().equals(name)){
				isFound=true;
				pst=temp;
			}
			tempTeam.add在场时间(temp.get在场时间());
			tempTeam.add失误数(temp.get失误数());
			tempTeam.add投篮数(temp.get投篮出手数());
			tempTeam.add罚球数(temp.get罚球出手数());
			tempTeam.add进攻篮板数(temp.get进攻篮板数());
			tempTeam.add防守篮板数(temp.get防守篮板数());
		}
		/**
		if(isFound){
		    ownTeam=tempTeam;	
		}
		tempTeam=new TeamDetail();
		*/
		
		Set<PlayerScoreTableVo> score2=match.getTeam2PlayersScore();
		itor=score2.iterator();
		while(itor.hasNext()){
			PlayerScoreTableVo temp=itor.next();
			tempTeam2.add在场时间(temp.get在场时间());
			tempTeam2.add失误数(temp.get失误数());
			tempTeam2.add投篮数(temp.get投篮出手数());
			tempTeam2.add罚球数(temp.get罚球出手数());
			tempTeam2.add进攻篮板数(temp.get进攻篮板数());
			tempTeam2.add防守篮板数(temp.get防守篮板数());
			tempTeam2.add命中数(temp.get投篮命中数());
			if(temp.get球员名().equals(name)){
				pst=temp;
			}
		}
		if(isFound){
			ownTeam=tempTeam;
			rivalTeam=tempTeam2;
		}else{
			ownTeam=tempTeam2;
			rivalTeam=tempTeam;
		}
		
		double 得分=pst.get个人得分();
		double 助攻=pst.get助攻数();
		double 篮板=pst.get进攻篮板数()+pst.get防守篮板数();
		
		PlayerVo pvo=new PlayerVo();
		pvo.set比赛得分(得分);
		pvo.set助攻数(助攻);
		pvo.set篮板数(篮板);
		
		if(five.size()!=5){
			five.add(pvo);
		}else{
            rest.add(five.remove());
            five.add(pvo);
		}
		
		return psttopvo(pst,five,rest,ownTeam,rivalTeam,match.getSeason(),match.getDate());
		
	}
	
	private PlayerVo psttopvo(PlayerScoreTableVo pst,Queue<PlayerVo> five,Queue<PlayerVo> rest,TeamDetail owntd,TeamDetail rivaltd,String season,long date){
		PlayerVo newPlayer=new PlayerVo();
		Formula f=new Formula();
	    newPlayer.set赛季(season);
	    double 投篮数=pst.get投篮出手数();
	    newPlayer.set投篮数(投篮数);
	    double 罚球数=pst.get罚球出手数();
	    newPlayer.set罚球数(罚球数);
	    double 失误数=pst.get失误数();
	    newPlayer.set失误数(失误数);
	    double 断球数=pst.get抢断数();
	    newPlayer.set断球数(断球数);
	    double 助攻数=pst.get助攻数();
	    newPlayer.set助攻数(助攻数);
	    double 盖帽数=pst.get盖帽数();
        newPlayer.set盖帽数(盖帽数);
        double 犯规数=pst.get犯规数();
        newPlayer.set犯规数(犯规数);
        double 比赛得分=pst.get个人得分();
        newPlayer.set比赛得分(比赛得分);
        double 投篮命中数=pst.get投篮命中数();
        newPlayer.set投篮命中数(投篮命中数);
        double 三分命中数=pst.get三分命中数();
        newPlayer.set三分命中数(三分命中数);
        double 三分出手数=pst.get三分出手数();
        newPlayer.set三分出手数(三分出手数);
        double 罚球命中数=pst.get罚球命中数();
        newPlayer.set罚球命中数(罚球命中数);
        double 前场篮板数=pst.get进攻篮板数();
        newPlayer.set前场篮板数(前场篮板数);
        double 后场篮板数=pst.get防守篮板数();
        newPlayer.set后场篮板数(后场篮板数);
		double 命中率=f.playerFieldGoalPer(投篮命中数, 投篮数);
		newPlayer.set命中率(命中率);
		double 效率=f.playerEffPer(比赛得分, 前场篮板数+后场篮板数, 助攻数, 断球数, 盖帽数, 投篮数, 投篮命中数, 罚球数, 罚球命中数, 失误数);
		newPlayer.set效率(效率);
		double avg5_score=0;
		double avg5_rebound=0;
		double avg5_assist=0;
		double avgr_score=0;
		double avgr_rebound=0;
		double avgr_assist=0;
		Iterator<PlayerVo> itor=five.iterator();
		
		while(itor.hasNext()){
			PlayerVo temp=itor.next();
			avg5_score+=temp.get比赛得分();
			avg5_rebound+=temp.get篮板数();
			avg5_assist+=temp.get助攻数();
		}
		itor=rest.iterator();
		
		while(itor.hasNext()){
			PlayerVo temp=itor.next();
			avgr_score+=temp.get比赛得分();
			avgr_rebound+=temp.get篮板数();
			avgr_assist+=temp.get助攻数();
		}
		if(five.size()==5&&rest.size()!=0){
		   double 得分近五场提升率=f.playerProgress(avg5_score/5, avgr_score/rest.size());
		   double 篮板近五场提升率=f.playerProgress(avg5_rebound/5, avgr_rebound/rest.size());
		   double 助攻近五场提升率=f.playerProgress(avg5_assist/5, avgr_assist/rest.size());
		   newPlayer.set得分近五场提升率(得分近五场提升率);
		   newPlayer.set篮板近五场提升率(篮板近五场提升率);
		   newPlayer.set助攻近五场提升率(助攻近五场提升率);
		}else{
			newPlayer.set得分近五场提升率(0);
			newPlayer.set篮板近五场提升率(0);
			newPlayer.set助攻近五场提升率(0);
		}
		
		long teamTime=owntd.get在场时间();
		double rebound=owntd.get篮板数();
		double r_rebound=rivaltd.get篮板数();
		double r_atkrebound=rivaltd.get进攻篮板数();
		double r_defrebound=rivaltd.get防守篮板数();
		double atkrebound=owntd.get进攻篮板数();
		double defrebound=owntd.get防守篮板数();
		double fieldGoal=owntd.get命中数();
		double r_shot=rivaltd.get投篮数();
		double shot=owntd.get投篮数();
		double freethrow=owntd.get罚球数();
		double turnover=owntd.get失误数();
		double GmSc效率值=f.playerGmScPer(比赛得分, 投篮命中数, 投篮数, 罚球数, 罚球命中数, 前场篮板数, 后场篮板数, 断球数, 助攻数, 盖帽数, 犯规数, 失误数);
		newPlayer.setGmSc效率值(GmSc效率值);
		double 真实投篮命中率=f.playerRealFieldGoalPer(比赛得分, 投篮数, 罚球数);
		newPlayer.set真实投篮命中率(真实投篮命中率);
		double 投篮效率=f.playerShotEffPer(投篮命中数, 三分命中数, 投篮数);
		newPlayer.set投篮效率(投篮效率);
		double 篮板率=f.playerReboundPer(前场篮板数+后场篮板数, teamTime, pst.get在场时间(), rebound, r_rebound);
		newPlayer.set篮板率(篮板率);
		double 篮板数=前场篮板数+后场篮板数;
		newPlayer.set篮板数(篮板数);
		double 罚球命中率=f.playerFieldGoalPer(罚球命中数, 罚球数);
		newPlayer.set罚球命中率(罚球命中率);
		double 三分命中率=f.playerFieldGoalPer(三分命中数, 三分出手数);
		newPlayer.set三分命中率(三分命中率);
		double 进攻篮板率=f.playerATKReboundPer(前场篮板数, teamTime, pst.get在场时间(), atkrebound, r_atkrebound);
		newPlayer.set进攻篮板率(进攻篮板率);
		double 防守篮板率=f.playerDEFReboundPer(后场篮板数, teamTime, pst.get在场时间(), defrebound, r_defrebound);
		newPlayer.set防守篮板率(防守篮板率);
		double 助攻率=f.playerAssistPer(助攻数, pst.get在场时间(), teamTime, fieldGoal, 投篮命中数);
		newPlayer.set助攻率(助攻率);
		double 抢断率=f.playerStealPer(断球数, pst.get在场时间(), teamTime, r_atkrebound);
		newPlayer.set抢断率(抢断率);
		double 盖帽率=f.playerBlockPer(盖帽数, pst.get盖帽数(), teamTime, r_shot);
		newPlayer.set盖帽率(盖帽率);
		double 失误率=f.playerTurnoverPer(失误数, 投篮数, 罚球数);
		newPlayer.set失误率(失误率);
		double 使用率=f.playerWorkPer(投篮数, 罚球数, 失误数, teamTime, pst.get在场时间(),shot, freethrow, turnover);
		newPlayer.set使用率(使用率);
		
		newPlayer.set比赛日期(date);
		return newPlayer;
	}
	
	private class TeamDetail{
		
		private long 在场时间;
		private double 进攻篮板数;
		private double 防守篮板数;
		private double 失误数;
		private double 投篮数;
		private double 罚球数;
		private double 命中数;
		
		public void add命中数(double fieldGoal){
			命中数+=fieldGoal;
		}
		
		public double get命中数(){
			return 命中数;
		}
		
		public void add在场时间(long time){
			在场时间+=time;
		}
		
		public void add进攻篮板数(double rebound){
			进攻篮板数+=rebound;
		}
		
		public void add防守篮板数(double rebound){
			防守篮板数+=rebound;
		}
		
		public void add失误数(double turnover){
			失误数+=turnover;
		}
		
		public void add投篮数(double shot){
			投篮数+=shot;
		}
		
		public void add罚球数(double freethrow){
			罚球数+=freethrow;
		}
		
		public long get在场时间(){
			return 在场时间;
		}
		
		public double get进攻篮板数(){
			return 进攻篮板数;
		}
		
		public double get防守篮板数(){
			return 防守篮板数;
		}
		
		public double get篮板数(){
			return 进攻篮板数+防守篮板数;
		}
		
		public double get失误数(){
			return 失误数;
		} 
		
		public double get投篮数(){
			return 投篮数;
		}

		public double get罚球数(){
			return 罚球数;
		}
	}
	
	public TeamVo tgdpototvo(TeamGameDataPo tgdpo){
		TeamVo tvo=new TeamVo();
		Formula f=new Formula();
		
		String 球队简称=tgdpo.get球队简称();
		int 胜利场数=tgdpo.get胜利场数();
		int 对方防守篮板数=tgdpo.get对方防守篮板数();
		double 进攻回合=tgdpo.get进攻回合();
		double 防守回合=tgdpo.get防守回合();
		int 对手进攻篮板数=tgdpo.get对手进攻篮板数();
		int 对手得分=tgdpo.get对手得分();
		String 赛季=tgdpo.get赛季();
		int 比赛场数=tgdpo.get比赛场数();
		int 投篮数=tgdpo.get投篮数();
		int 罚球数=tgdpo.get罚球数();
		int 失误数=tgdpo.get失误数();	
		int 断球数=tgdpo.get断球数();
		int 助攻数=tgdpo.get助攻数();
		int 盖帽数=tgdpo.get盖帽数();
		int 犯规数=tgdpo.get犯规数();
		int 比赛得分=tgdpo.get比赛得分();
		int 投篮命中数=tgdpo.get投篮命中数();
		int 三分命中数=tgdpo.get三分命中数();
		int 三分出手数=tgdpo.get三分出手数();
		int 罚球命中数=tgdpo.get罚球命中数();
		int 前场篮板数=tgdpo.get前场篮板数();
		int 后场篮板数=tgdpo.get后场篮板数();
		
		tvo.set球队简称(球队简称);
		tvo.set胜利场数(胜利场数);
        tvo.set对手得分(对手得分);
        tvo.set进攻回合(进攻回合);
        tvo.set防守回合(防守回合);
        tvo.set对手进攻篮板数(对手进攻篮板数);
        tvo.set对方防守篮板数(对方防守篮板数);
        tvo.set赛季(赛季);
        tvo.set比赛场数(比赛场数);
        tvo.set投篮数(投篮数);
        tvo.set罚球数(罚球数);
        tvo.set失误数(失误数);
        tvo.set断球数(断球数);
        tvo.set助攻数(助攻数);
        tvo.set盖帽数(盖帽数);
        tvo.set犯规数(犯规数);
        tvo.set比赛得分(比赛得分);
        tvo.set投篮命中数(投篮命中数);
        tvo.set三分命中数(三分命中数);
        tvo.set三分出手数(三分出手数);
        tvo.set罚球命中数(罚球命中数);
        tvo.set前场篮板数(前场篮板数);
        tvo.set后场篮板数(后场篮板数);
        
        double 命中率=投篮命中数/投篮数;
        tvo.set命中率(命中率);
        double 三分命中率=三分命中数/三分出手数;
        tvo.set三分命中率(三分命中率);
        double 罚球命中率=罚球命中数/罚球数;
        tvo.set罚球命中率(罚球命中率);
        double 篮板数=前场篮板数+后场篮板数;
        tvo.set篮板数(篮板数);
        double 胜率=胜利场数/比赛场数;
        tvo.set胜率(胜率);
        double 进攻效率=f.teamATKEffPer(进攻回合, 比赛得分);
        tvo.set进攻效率(进攻效率);
        double 防守效率=f.teamDEFEffPer(防守回合, 对手得分);
        tvo.set防守效率(防守效率);
        double 进攻篮板效率=f.teamATKReboundEff(前场篮板数, 对方防守篮板数);
        tvo.set进攻篮板效率(进攻篮板效率);
        double 防守篮板效率=f.teamDEFReboundEff(后场篮板数, 对手进攻篮板数);
        tvo.set防守篮板效率(防守篮板效率);
        double 抢断效率=f.teamStealEffPer(防守回合, 断球数);
        tvo.set抢断效率(抢断效率);
        double 助攻率=f.teamAssistPer(进攻回合, 助攻数);
        tvo.set助攻率(助攻率);
		
        
        return tvo;
	}
	
	public PlayerVo pgdpotopvo(PlayerGameDataPo pgdpo,PlayerVo pvo){
		int 比赛场数=pgdpo.get比赛场数();
		pvo.set比赛场数(比赛场数);
		int 投篮数=pgdpo.get投篮数();
		pvo.set投篮数(投篮数);
		int 罚球数=pgdpo.get罚球数();
		pvo.set罚球数(罚球数);
		int 失误数=pgdpo.get失误数();
		pvo.set失误数(失误数);
		int 断球数=pgdpo.get断球数();
		pvo.set断球数(断球数);
		int 助攻数=pgdpo.get助攻数();
		pvo.set助攻数(助攻数);
		int 盖帽数=pgdpo.get盖帽数();
		pvo.set盖帽数(盖帽数);
		int 犯规数=pgdpo.get犯规数();
		pvo.set犯规数(犯规数);
		int 比赛得分=pgdpo.get比赛得分();
		pvo.set比赛得分(比赛得分);
		int 投篮命中数=pgdpo.get投篮命中数();
		pvo.set投篮命中数(投篮命中数);
		int 三分命中数=pgdpo.get三分命中数();
		pvo.set三分命中数(三分命中数);
		int 三分出手数=pgdpo.get三分出手数();
		pvo.set三分出手数(三分出手数);
		int 罚球命中数=pgdpo.get罚球命中数();
		pvo.set罚球命中数(罚球命中数);
		int 前场篮板数=pgdpo.get前场篮板数();
		pvo.set前场篮板数(前场篮板数);
		int 后场篮板数=pgdpo.get后场篮板数();
		pvo.set后场篮板数(后场篮板数);
		int 在场时间=pgdpo.get在场时间();
		pvo.set在场时间(在场时间);
		
		return pvo;
	}
	
	public PlayerVo pcpotopvo(PlayerCaculationDataPo pcpo,PlayerVo pvo){
		double 得分近五场提升率=pcpo.get得分近五场提升率();
		pvo.set得分近五场提升率(得分近五场提升率);
		double 篮板近五场提升率=pcpo.get篮板近五场提升率();
		pvo.set篮板近五场提升率(篮板近五场提升率);
		double 助攻近五场提升率=pcpo.get助攻近五场提升率();
		pvo.set助攻近五场提升率(助攻近五场提升率);
	    double 篮板率=pcpo.get篮板率();
	    pvo.set篮板率(篮板率);
		double 进攻篮板率=pcpo.get进攻篮板率();
		pvo.set进攻篮板率(进攻篮板率);
		double 防守篮板率=pcpo.get防守篮板率();
		pvo.set防守篮板率(防守篮板率);
		double 助攻率=pcpo.get助攻率();
		pvo.set助攻率(助攻率);
	    double 抢断率=pcpo.get抢断率();
	    pvo.set抢断率(抢断率);
		double 盖帽率=pcpo.get盖帽率();
		pvo.set盖帽率(盖帽率);
		double 失误率=pcpo.get失误率();
		pvo.set失误率(失误率);
		double 使用率=pcpo.get使用率();
		pvo.set使用率(使用率);
		
		return pvo;
	}
	
	
}
