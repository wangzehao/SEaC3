package bhwz.seac3;

/*
 import java.net.MalformedURLException;
 import java.rmi.Naming;
 import java.rmi.NotBoundException;
 import java.rmi.RemoteException;
 */
import bhwz.seac3.bl.predict.PredictBL;
import bhwz.seac3.bl.query.HotSpotQueryBL;
import bhwz.seac3.bl.query.MatchQueryBL;
import bhwz.seac3.bl.query.PlayerQueryBL;
import bhwz.seac3.bl.query.SeasonQueryBL;
import bhwz.seac3.bl.query.TeamQueryBL;
import bhwz.seac3.blservice.predict.PredictBLService;
import bhwz.seac3.blservice.query.HotspotQueryBLService;
import bhwz.seac3.blservice.query.MatchQueryBLService;
import bhwz.seac3.blservice.query.PlayerQueryBLService;
import bhwz.seac3.blservice.query.SeasonQueryBLService;
import bhwz.seac3.blservice.query.TeamQueryBLService;

public class RMIClientBLServiceManager {
	// private static final int PORT=8657;
	// private static String ip;

	public static void initClient(String serverIp) {
		// ip=serverIp;
	}

	@SuppressWarnings("unchecked")
	// fake RMI
	public static <T> T getBLService(@SuppressWarnings("rawtypes") Class clazz) {
		String name = clazz.getName();
		try {
			if (name.equals(HotspotQueryBLService.class.getName()))
				return (T) (new HotSpotQueryBL());
			else if (name.equals(MatchQueryBLService.class.getName()))
				return (T) (new MatchQueryBL(
						DBServiceManager.getMatchDBService(),
						DBServiceManager.getPlayerMatchDBService(),
						DBServiceManager.geTeamMatchDBService(),
						DBServiceManager.getHotSpotDBService()));
			else if (name.equals(TeamQueryBLService.class.getName()))
				return (T) (new TeamQueryBL(
						LocalBLServiceManager.getGameTeamDataComputer(),
						DBServiceManager.getTeamDBService(),
						DBServiceManager.getTeamGameDataDBService(),
						DBServiceManager.getTeamIconDBService()));
			else if (name.equals(PlayerQueryBLService.class.getName()))
				return (T) (new PlayerQueryBL(
						LocalBLServiceManager.getPerGamePlayerDataComputer(),
						DBServiceManager.getPlayerDBService(),
						DBServiceManager.getPlayerGameDataDBService(),
						DBServiceManager.getPlayerActionPictureDBService(),
						DBServiceManager.getPlayerPortraitPictureDBService(),
						DBServiceManager.getPlayerCaculationDataDBService()));
			else if (name.equals(SeasonQueryBLService.class.getName()))
				return (T) (new SeasonQueryBL(
						DBServiceManager.getSeasonDBService()));
			else if (name.equals(PredictBLService.class.getName()))
				return (T) (new PredictBL(
						getBLService(TeamQueryBLService.class),
						getBLService(SeasonQueryBLService.class)));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/*
	 * @SuppressWarnings("unchecked") public static <T> T
	 * getBLService(@SuppressWarnings("rawtypes") Class clazz){ try { return
	 * (T)clazz.cast(Naming.lookup("rmi://"+ip+":"+PORT+"/"+clazz.getName())); }
	 * catch (MalformedURLException | RemoteException | NotBoundException e) {
	 * e.printStackTrace(); return null; } }
	 */
}
