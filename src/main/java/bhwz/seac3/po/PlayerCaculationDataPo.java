package bhwz.seac3.po;

/** id=name+";"+赛季; */
public class PlayerCaculationDataPo extends Po implements SimpleSaveQuery{
	private String name;
	private String 赛季;
	private double 得分近五场提升率;
	private double 篮板近五场提升率;
	private double 助攻近五场提升率;
	private double 篮板率;
	private double 进攻篮板率;
	private double 防守篮板率;
	private double 助攻率;
	private double 抢断率;
	private double 盖帽率;
	private double 失误率;
	private double 使用率;
	@Override
	public String getId() {
		return getName()+";"+get赛季();
	}
	public void setId(String id){
		String[] temp=id.split(";");
		setName(temp[0]);
		set赛季(temp[1]);
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String get赛季() {
		return 赛季;
	}
	public void set赛季(String 赛季) {
		this.赛季 = 赛季;
	}
	public double get得分近五场提升率() {
		return 得分近五场提升率;
	}
	public void set得分近五场提升率(double 得分近五场提升率) {
		this.得分近五场提升率 = 得分近五场提升率;
	}
	public double get篮板近五场提升率() {
		return 篮板近五场提升率;
	}
	public void set篮板近五场提升率(double 篮板近五场提升率) {
		this.篮板近五场提升率 = 篮板近五场提升率;
	}
	public double get助攻近五场提升率() {
		return 助攻近五场提升率;
	}
	public void set助攻近五场提升率(double 助攻近五场提升率) {
		this.助攻近五场提升率 = 助攻近五场提升率;
	}
	public double get篮板率() {
		return 篮板率;
	}
	public void set篮板率(double 篮板率) {
		this.篮板率 = 篮板率;
	}
	public double get进攻篮板率() {
		return 进攻篮板率;
	}
	public void set进攻篮板率(double 进攻篮板率) {
		this.进攻篮板率 = 进攻篮板率;
	}
	public double get防守篮板率() {
		return 防守篮板率;
	}
	public void set防守篮板率(double 防守篮板率) {
		this.防守篮板率 = 防守篮板率;
	}
	public double get助攻率() {
		return 助攻率;
	}
	public void set助攻率(double 助攻率) {
		this.助攻率 = 助攻率;
	}
	public double get抢断率() {
		return 抢断率;
	}
	public void set抢断率(double 抢断率) {
		this.抢断率 = 抢断率;
	}
	public double get盖帽率() {
		return 盖帽率;
	}
	public void set盖帽率(double 盖帽率) {
		this.盖帽率 = 盖帽率;
	}
	public double get失误率() {
		return 失误率;
	}
	public void set失误率(double 失误率) {
		this.失误率 = 失误率;
	}
	public double get使用率() {
		return 使用率;
	}
	public void set使用率(double 使用率) {
		this.使用率 = 使用率;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		long temp;
		temp = Double.doubleToLongBits(使用率);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(助攻率);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(助攻近五场提升率);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(失误率);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(得分近五场提升率);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(抢断率);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(盖帽率);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(篮板率);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(篮板近五场提升率);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((赛季 == null) ? 0 : 赛季.hashCode());
		temp = Double.doubleToLongBits(进攻篮板率);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(防守篮板率);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PlayerCaculationDataPo other = (PlayerCaculationDataPo) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (Double.doubleToLongBits(使用率) != Double.doubleToLongBits(other.使用率))
			return false;
		if (Double.doubleToLongBits(助攻率) != Double.doubleToLongBits(other.助攻率))
			return false;
		if (Double.doubleToLongBits(助攻近五场提升率) != Double
				.doubleToLongBits(other.助攻近五场提升率))
			return false;
		if (Double.doubleToLongBits(失误率) != Double.doubleToLongBits(other.失误率))
			return false;
		if (Double.doubleToLongBits(得分近五场提升率) != Double
				.doubleToLongBits(other.得分近五场提升率))
			return false;
		if (Double.doubleToLongBits(抢断率) != Double.doubleToLongBits(other.抢断率))
			return false;
		if (Double.doubleToLongBits(盖帽率) != Double.doubleToLongBits(other.盖帽率))
			return false;
		if (Double.doubleToLongBits(篮板率) != Double.doubleToLongBits(other.篮板率))
			return false;
		if (Double.doubleToLongBits(篮板近五场提升率) != Double
				.doubleToLongBits(other.篮板近五场提升率))
			return false;
		if (赛季 == null) {
			if (other.赛季 != null)
				return false;
		} else if (!赛季.equals(other.赛季))
			return false;
		if (Double.doubleToLongBits(进攻篮板率) != Double
				.doubleToLongBits(other.进攻篮板率))
			return false;
		if (Double.doubleToLongBits(防守篮板率) != Double
				.doubleToLongBits(other.防守篮板率))
			return false;
		return true;
	}
	
}
