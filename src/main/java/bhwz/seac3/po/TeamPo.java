package bhwz.seac3.po;

public class TeamPo extends Po implements SimpleSaveQuery{
	private String 球队全名;
	/**id*/
	private String 缩写;
	private String 所在地;
	private String 赛区;
	private String 分区;
	private String 主场;
	private String 建立时间;
	
	public String get球队全名() {
		return 球队全名;
	}
	public void set球队全名(String 球队全名) {
		this.球队全名 = 球队全名;
	}
	public String get缩写() {
		return 缩写;
	}
	public void set缩写(String 缩写) {
		this.缩写 = 缩写;
	}
	public String get所在地() {
		return 所在地;
	}
	public void set所在地(String 所在地) {
		this.所在地 = 所在地;
	}
	public String get赛区() {
		return 赛区;
	}
	public void set赛区(String 赛区) {
		this.赛区 = 赛区;
	}
	public String get分区() {
		return 分区;
	}
	public void set分区(String 分区) {
		this.分区 = 分区;
	}
	public String get主场() {
		return 主场;
	}
	public void set主场(String 主场) {
		this.主场 = 主场;
	}
	public String get建立时间() {
		return 建立时间;
	}
	public void set建立时间(String 建立时间) {
		this.建立时间 = 建立时间;
	}
	
	@Override
	public String getId() {
		return 缩写;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((主场 == null) ? 0 : 主场.hashCode());
		result = prime * result + ((分区 == null) ? 0 : 分区.hashCode());
		result = prime * result + ((建立时间 == null) ? 0 : 建立时间.hashCode());
		result = prime * result + ((所在地 == null) ? 0 : 所在地.hashCode());
		result = prime * result + ((球队全名 == null) ? 0 : 球队全名.hashCode());
		result = prime * result + ((缩写 == null) ? 0 : 缩写.hashCode());
		result = prime * result + ((赛区 == null) ? 0 : 赛区.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TeamPo other = (TeamPo) obj;
		if (主场 == null) {
			if (other.主场 != null)
				return false;
		} else if (!主场.equals(other.主场))
			return false;
		if (分区 == null) {
			if (other.分区 != null)
				return false;
		} else if (!分区.equals(other.分区))
			return false;
		if (建立时间 == null) {
			if (other.建立时间 != null)
				return false;
		} else if (!建立时间.equals(other.建立时间))
			return false;
		if (所在地 == null) {
			if (other.所在地 != null)
				return false;
		} else if (!所在地.equals(other.所在地))
			return false;
		if (球队全名 == null) {
			if (other.球队全名 != null)
				return false;
		} else if (!球队全名.equals(other.球队全名))
			return false;
		if (缩写 == null) {
			if (other.缩写 != null)
				return false;
		} else if (!缩写.equals(other.缩写))
			return false;
		if (赛区 == null) {
			if (other.赛区 != null)
				return false;
		} else if (!赛区.equals(other.赛区))
			return false;
		return true;
	}
	
}
