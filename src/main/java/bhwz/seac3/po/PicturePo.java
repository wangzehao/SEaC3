package bhwz.seac3.po;

import java.util.Arrays;

/**id=key;*/
public class PicturePo extends Po implements SimpleSaveQuery{
	private String key;
	private byte[] picture;
	
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public byte[] getPicture() {
		return picture;
	}
	public void setPicture(byte[] picture) {
		this.picture = picture;
	}
	@Override
	public PicturePo clone() {
		PicturePo clone=new PicturePo();
		clone.setKey(new String(this.getKey()));
		clone.setPicture(this.getPicture().clone());
		return clone;
	}
	@Override
	public String getId() {
		return key;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((key == null) ? 0 : key.hashCode());
		result = prime * result + Arrays.hashCode(picture);
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PicturePo other = (PicturePo) obj;
		if (key == null) {
			if (other.key != null)
				return false;
		} else if (!key.equals(other.key))
			return false;
		if (!Arrays.equals(picture, other.picture))
			return false;
		return true;
	}
	
	
}
