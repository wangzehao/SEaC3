package bhwz.seac3.po;

public class PlayerScoreTablePo extends Po implements SimpleSaveQuery{
	private long id;	
	private String 球员名;
	private String 位置;
	private int 在场时间;
	private int 投篮命中数;
	private int 投篮出手数;
	private int 三分命中数;
	private int 三分出手数;
	private int 罚球命中数;
	private int 罚球出手数;
	private int 进攻篮板数;
	private int 防守篮板数;
	private int 总篮板数;
	private int 助攻数;
	private int 抢断数;
	private int 盖帽数;
	private int 失误数;
	private int 犯规数;
	private int 个人得分;
	
	public String get球员名() {
		return 球员名;
	}
	public void set球员名(String 球员名) {
		this.球员名 = 球员名;
	}
	public String get位置() {
		return 位置;
	}
	public void set位置(String 位置) {
		this.位置 = 位置;
	}
	public int get在场时间() {
		return 在场时间;
	}
	public void set在场时间(int 在场时间) {
		this.在场时间 = 在场时间;
	}
	public int get投篮命中数() {
		return 投篮命中数;
	}
	public void set投篮命中数(int 投篮命中数) {
		this.投篮命中数 = 投篮命中数;
	}
	public int get投篮出手数() {
		return 投篮出手数;
	}
	public void set投篮出手数(int 投篮出手数) {
		this.投篮出手数 = 投篮出手数;
	}
	public int get三分命中数() {
		return 三分命中数;
	}
	public void set三分命中数(int 三分命中数) {
		this.三分命中数 = 三分命中数;
	}
	public int get三分出手数() {
		return 三分出手数;
	}
	public void set三分出手数(int 三分出手数) {
		this.三分出手数 = 三分出手数;
	}
	public int get罚球命中数() {
		return 罚球命中数;
	}
	public void set罚球命中数(int 罚球命中数) {
		this.罚球命中数 = 罚球命中数;
	}
	public int get罚球出手数() {
		return 罚球出手数;
	}
	public void set罚球出手数(int 罚球出手数) {
		this.罚球出手数 = 罚球出手数;
	}
	public int get进攻篮板数() {
		return 进攻篮板数;
	}
	public void set进攻篮板数(int 进攻篮板数) {
		this.进攻篮板数 = 进攻篮板数;
	}
	public int get防守篮板数() {
		return 防守篮板数;
	}
	public void set防守篮板数(int 防守篮板数) {
		this.防守篮板数 = 防守篮板数;
	}
	public int get总篮板数() {
		return 总篮板数;
	}
	public void set总篮板数(int 总篮板数) {
		this.总篮板数 = 总篮板数;
	}
	public int get助攻数() {
		return 助攻数;
	}
	public void set助攻数(int 助攻数) {
		this.助攻数 = 助攻数;
	}
	public int get抢断数() {
		return 抢断数;
	}
	public void set抢断数(int 抢断数) {
		this.抢断数 = 抢断数;
	}
	public int get盖帽数() {
		return 盖帽数;
	}
	public void set盖帽数(int 盖帽数) {
		this.盖帽数 = 盖帽数;
	}
	public int get失误数() {
		return 失误数;
	}
	public void set失误数(int 失误数) {
		this.失误数 = 失误数;
	}
	public int get犯规数() {
		return 犯规数;
	}
	public void set犯规数(int 犯规数) {
		this.犯规数 = 犯规数;
	}
	public int get个人得分() {
		return 个人得分;
	}
	public void set个人得分(int 个人得分) {
		this.个人得分 = 个人得分;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@Override
	public Long getId() {
		return id;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + 三分出手数;
		result = prime * result + 三分命中数;
		result = prime * result + 个人得分;
		result = prime * result + ((位置 == null) ? 0 : 位置.hashCode());
		result = prime * result + 助攻数;
		result = prime * result + 在场时间;
		result = prime * result + 失误数;
		result = prime * result + 总篮板数;
		result = prime * result + 投篮出手数;
		result = prime * result + 投篮命中数;
		result = prime * result + 抢断数;
		result = prime * result + 犯规数;
		result = prime * result + ((球员名 == null) ? 0 : 球员名.hashCode());
		result = prime * result + 盖帽数;
		result = prime * result + 罚球出手数;
		result = prime * result + 罚球命中数;
		result = prime * result + 进攻篮板数;
		result = prime * result + 防守篮板数;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PlayerScoreTablePo other = (PlayerScoreTablePo) obj;
		if (三分出手数 != other.三分出手数)
			return false;
		if (三分命中数 != other.三分命中数)
			return false;
		if (个人得分 != other.个人得分)
			return false;
		if (位置 == null) {
			if (other.位置 != null)
				return false;
		} else if (!位置.equals(other.位置))
			return false;
		if (助攻数 != other.助攻数)
			return false;
		if (在场时间 != other.在场时间)
			return false;
		if (失误数 != other.失误数)
			return false;
		if (总篮板数 != other.总篮板数)
			return false;
		if (投篮出手数 != other.投篮出手数)
			return false;
		if (投篮命中数 != other.投篮命中数)
			return false;
		if (抢断数 != other.抢断数)
			return false;
		if (犯规数 != other.犯规数)
			return false;
		if (球员名 == null) {
			if (other.球员名 != null)
				return false;
		} else if (!球员名.equals(other.球员名))
			return false;
		if (盖帽数 != other.盖帽数)
			return false;
		if (罚球出手数 != other.罚球出手数)
			return false;
		if (罚球命中数 != other.罚球命中数)
			return false;
		if (进攻篮板数 != other.进攻篮板数)
			return false;
		if (防守篮板数 != other.防守篮板数)
			return false;
		return true;
	}
	
	
	
}
