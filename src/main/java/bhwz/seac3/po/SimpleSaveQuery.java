package bhwz.seac3.po;

import java.io.Serializable;

public interface SimpleSaveQuery {
	public Serializable getId();
}
