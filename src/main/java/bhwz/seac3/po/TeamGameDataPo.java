package bhwz.seac3.po;

import java.io.Serializable;

/** id=球队简称+";"+赛季; */
public class TeamGameDataPo extends GameDataPo implements SimpleSaveQuery{
	private String 球队简称;
	private int 胜利场数;
	private int 对方防守篮板数;
	private double 进攻回合;
	private double 防守回合;
	private int 对手进攻篮板数;
	private int 对手得分;
	
	@Override
	public Serializable getId() {
		return get球队简称()+";"+get赛季();
	}
	
	public void setId(String id){
		String[] temp=id.split(";");
		set球队简称(temp[0]);
		set赛季(temp[1]);
	}

	public String get球队简称() {
		return 球队简称;
	}

	public void set球队简称(String 球队简称) {
		this.球队简称 = 球队简称;
	}

	public int get胜利场数() {
		return 胜利场数;
	}

	public void set胜利场数(int 胜利场数) {
		this.胜利场数 = 胜利场数;
	}

	public int get对方防守篮板数() {
		return 对方防守篮板数;
	}

	public void set对方防守篮板数(int 对方防守篮板数) {
		this.对方防守篮板数 = 对方防守篮板数;
	}

	public double get进攻回合() {
		return 进攻回合;
	}

	public void set进攻回合(double 进攻回合) {
		this.进攻回合 = 进攻回合;
	}

	public double get防守回合() {
		return 防守回合;
	}

	public void set防守回合(double 防守回合) {
		this.防守回合 = 防守回合;
	}

	public int get对手进攻篮板数() {
		return 对手进攻篮板数;
	}

	public void set对手进攻篮板数(int 对手进攻篮板数) {
		this.对手进攻篮板数 = 对手进攻篮板数;
	}

	public int get对手得分() {
		return 对手得分;
	}

	public void set对手得分(int 对手得分) {
		this.对手得分 = 对手得分;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + 对手得分;
		result = prime * result + 对手进攻篮板数;
		result = prime * result + 对方防守篮板数;
		result = prime * result + ((球队简称 == null) ? 0 : 球队简称.hashCode());
		result = prime * result + 胜利场数;
		long temp;
		temp = Double.doubleToLongBits(进攻回合);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(防守回合);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TeamGameDataPo other = (TeamGameDataPo) obj;
		if (对手得分 != other.对手得分)
			return false;
		if (对手进攻篮板数 != other.对手进攻篮板数)
			return false;
		if (对方防守篮板数 != other.对方防守篮板数)
			return false;
		if (球队简称 == null) {
			if (other.球队简称 != null)
				return false;
		} else if (!球队简称.equals(other.球队简称))
			return false;
		if (胜利场数 != other.胜利场数)
			return false;
		if (Double.doubleToLongBits(进攻回合) != Double
				.doubleToLongBits(other.进攻回合))
			return false;
		if (Double.doubleToLongBits(防守回合) != Double
				.doubleToLongBits(other.防守回合))
			return false;
		return true;
	}
	
}