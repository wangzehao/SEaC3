package bhwz.seac3.po;

public abstract class GameDataPo extends Po{
	private String 赛季;
	private int 比赛场数;
	private int 投篮数;
	private int 罚球数;
	private int 失误数;	
	private int 断球数;
	private int 助攻数;
	private int 盖帽数;
	private int 犯规数;
	private int 比赛得分;
	private int 投篮命中数;
	private int 三分命中数;
	private int 三分出手数;
	private int 罚球命中数;
	private int 前场篮板数;
	private int 后场篮板数;
	public String get赛季() {
		return 赛季;
	}
	public void set赛季(String 赛季) {
		this.赛季 = 赛季;
	}
	public int get比赛场数() {
		return 比赛场数;
	}
	public void set比赛场数(int 比赛场数) {
		this.比赛场数 = 比赛场数;
	}
	public int get投篮数() {
		return 投篮数;
	}
	public void set投篮数(int 投篮数) {
		this.投篮数 = 投篮数;
	}
	public int get罚球数() {
		return 罚球数;
	}
	public void set罚球数(int 罚球数) {
		this.罚球数 = 罚球数;
	}
	public int get失误数() {
		return 失误数;
	}
	public void set失误数(int 失误数) {
		this.失误数 = 失误数;
	}
	public int get断球数() {
		return 断球数;
	}
	public void set断球数(int 断球数) {
		this.断球数 = 断球数;
	}
	public int get助攻数() {
		return 助攻数;
	}
	public void set助攻数(int 助攻数) {
		this.助攻数 = 助攻数;
	}
	public int get盖帽数() {
		return 盖帽数;
	}
	public void set盖帽数(int 盖帽数) {
		this.盖帽数 = 盖帽数;
	}
	public int get犯规数() {
		return 犯规数;
	}
	public void set犯规数(int 犯规数) {
		this.犯规数 = 犯规数;
	}
	public int get比赛得分() {
		return 比赛得分;
	}
	public void set比赛得分(int 比赛得分) {
		this.比赛得分 = 比赛得分;
	}
	public int get投篮命中数() {
		return 投篮命中数;
	}
	public void set投篮命中数(int 投篮命中数) {
		this.投篮命中数 = 投篮命中数;
	}
	public int get三分命中数() {
		return 三分命中数;
	}
	public void set三分命中数(int 三分命中数) {
		this.三分命中数 = 三分命中数;
	}
	public int get三分出手数() {
		return 三分出手数;
	}
	public void set三分出手数(int 三分出手数) {
		this.三分出手数 = 三分出手数;
	}
	public int get罚球命中数() {
		return 罚球命中数;
	}
	public void set罚球命中数(int 罚球命中数) {
		this.罚球命中数 = 罚球命中数;
	}
	public int get前场篮板数() {
		return 前场篮板数;
	}
	public void set前场篮板数(int 前场篮板数) {
		this.前场篮板数 = 前场篮板数;
	}
	public int get后场篮板数() {
		return 后场篮板数;
	}
	public void set后场篮板数(int 后场篮板数) {
		this.后场篮板数 = 后场篮板数;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + 三分出手数;
		result = prime * result + 三分命中数;
		result = prime * result + 前场篮板数;
		result = prime * result + 助攻数;
		result = prime * result + 后场篮板数;
		result = prime * result + 失误数;
		result = prime * result + 投篮命中数;
		result = prime * result + 投篮数;
		result = prime * result + 断球数;
		result = prime * result + 比赛场数;
		result = prime * result + 比赛得分;
		result = prime * result + 犯规数;
		result = prime * result + 盖帽数;
		result = prime * result + 罚球命中数;
		result = prime * result + 罚球数;
		result = prime * result + ((赛季 == null) ? 0 : 赛季.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GameDataPo other = (GameDataPo) obj;
		if (三分出手数 != other.三分出手数)
			return false;
		if (三分命中数 != other.三分命中数)
			return false;
		if (前场篮板数 != other.前场篮板数)
			return false;
		if (助攻数 != other.助攻数)
			return false;
		if (后场篮板数 != other.后场篮板数)
			return false;
		if (失误数 != other.失误数)
			return false;
		if (投篮命中数 != other.投篮命中数)
			return false;
		if (投篮数 != other.投篮数)
			return false;
		if (断球数 != other.断球数)
			return false;
		if (比赛场数 != other.比赛场数)
			return false;
		if (比赛得分 != other.比赛得分)
			return false;
		if (犯规数 != other.犯规数)
			return false;
		if (盖帽数 != other.盖帽数)
			return false;
		if (罚球命中数 != other.罚球命中数)
			return false;
		if (罚球数 != other.罚球数)
			return false;
		if (赛季 == null) {
			if (other.赛季 != null)
				return false;
		} else if (!赛季.equals(other.赛季))
			return false;
		return true;
	}
	
}
