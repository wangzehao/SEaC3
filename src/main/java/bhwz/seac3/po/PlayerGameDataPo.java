package bhwz.seac3.po;

/** id=球员姓名+";"+赛季; */
public class PlayerGameDataPo extends GameDataPo implements SimpleSaveQuery{
	private String 球员姓名;
	private int 先发场数;
	private int 在场时间;
	private String 所属球队;
	private String 所属联盟;
	
	@Override
	public String getId() {
		return get球员姓名()+";"+get赛季();
	}
	
	public void setId(String id){
		String[] temp=id.split(";");
		set球员姓名(temp[0]);
		set赛季(temp[1]);
	}

	public String get球员姓名() {
		return 球员姓名;
	}

	public void set球员姓名(String 球员姓名) {
		this.球员姓名 = 球员姓名;
	}

	public int get先发场数() {
		return 先发场数;
	}

	public void set先发场数(int 先发场数) {
		this.先发场数 = 先发场数;
	}

	public int get在场时间() {
		return 在场时间;
	}

	public void set在场时间(int 在场时间) {
		this.在场时间 = 在场时间;
	}

	public String get所属球队() {
		return 所属球队;
	}

	public void set所属球队(String 所属球队) {
		this.所属球队 = 所属球队;
	}

	public String get所属联盟() {
		return 所属联盟;
	}

	public void set所属联盟(String 所属联盟) {
		this.所属联盟 = 所属联盟;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + 先发场数;
		result = prime * result + 在场时间;
		result = prime * result + ((所属球队 == null) ? 0 : 所属球队.hashCode());
		result = prime * result + ((所属联盟 == null) ? 0 : 所属联盟.hashCode());
		result = prime * result + ((球员姓名 == null) ? 0 : 球员姓名.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		PlayerGameDataPo other = (PlayerGameDataPo) obj;
		if (先发场数 != other.先发场数)
			return false;
		if (在场时间 != other.在场时间)
			return false;
		if (所属球队 == null) {
			if (other.所属球队 != null)
				return false;
		} else if (!所属球队.equals(other.所属球队))
			return false;
		if (所属联盟 == null) {
			if (other.所属联盟 != null)
				return false;
		} else if (!所属联盟.equals(other.所属联盟))
			return false;
		if (球员姓名 == null) {
			if (other.球员姓名 != null)
				return false;
		} else if (!球员姓名.equals(other.球员姓名))
			return false;
		return true;
	}

}
