package bhwz.seac3.blservice.query;

import java.rmi.Remote;
import java.util.List;
import bhwz.seac3.vo.PlayerVo;
import bhwz.seac3.vo.TeamVo;

/**
 * 作者 :byc 
 * 创建时间：2015年4月1日 下午11:05:48 
 * 类说明:热点查看类的接口
 */
public interface HotspotQueryBLService extends Remote {
	/*public List<PlayerVo> QueryPlayersRankedBy得分();

	public List<PlayerVo> QueryPlayersRankedBy篮板();

	public List<PlayerVo> QueryPlayersRankedBy助攻();

	public List<PlayerVo> QueryPlayersRankedBy盖帽();

	public List<PlayerVo> QueryPlayersRankedBy抢断();

	public List<PlayerVo> QueryPlayersRankedBy场均得分();

	public List<PlayerVo> QueryPlayersRankedBy场均篮板();

	public List<PlayerVo> QueryPlayersRankedBy场均助攻();

	public List<PlayerVo> QueryPlayersRankedBy场均盖帽();

	public List<PlayerVo> QueryPlayersRankedBy场均抢断();
	
	public List<PlayerVo> QueryPlayersRankedBy三分命中率();
	
	public List<PlayerVo> QueryPlayersRankedBy投篮命中率();
	
	public List<PlayerVo> QueryPlayersRankedBy罚球命中率();

	public List<TeamVo> QueryTeamRankedBy场均得分();

	public List<TeamVo> QueryTeamRankedBy场均篮板();

	public List<TeamVo> QueryTeamsRankedBy场均助攻();

	public List<TeamVo> QueryTeamRankedBy场均盖帽();

	public List<TeamVo> QueryTeamRankedBy场均抢断();

	public List<TeamVo> QueryTeamRankedBy三分命中率();

	public List<TeamVo> QueryTeamRankedBy投篮命中率();

	public List<TeamVo> QueryTeamRankedBy罚球命中率();
	
	public List<PlayerVo> QueryPlayersRankedBy场均得分进步();
	
	public List<PlayerVo> QueryPlayersRankedBy场均篮板进步();
	
	public List<PlayerVo> QueryPlayersRankedBy场均助攻进步();
	
	/*筛选条件包括当日热点球员的得分,篮板,助攻,盖帽,抢断；
	 * 赛季球员的场均得分,场均篮板,场均助攻,场均盖帽,场均抢断,三分投篮罚球命中率
	 * 进步最快球员场均得分进步,场均篮板进步,场均助攻进步
	 * 返回球员以及其对应筛选条件的数据
	 * */
	public List<PlayerVo> QueryPlayersRankedBy条件(String 筛选条件) throws Exception;
	
	//筛选条件包括赛季热点球队的场均得分,场均篮板,场均助攻,场均盖帽,场均抢断,三分投篮罚球命中率,犯规,失误,防守篮板数,进攻篮板数,返回球队以及其对应筛选条件的数据
	
	public List<TeamVo> QueryTeamsRankedBy条件(String 筛选条件) throws Exception;
	
	

}
