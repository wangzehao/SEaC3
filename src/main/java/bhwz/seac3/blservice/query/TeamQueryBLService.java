package bhwz.seac3.blservice.query;

import java.rmi.Remote;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import bhwz.seac3.util.ListSorter.SortOrder;
import bhwz.seac3.vo.TeamVo;

public interface TeamQueryBLService extends Remote {
	public default List<TeamVo> query赛季总数据(String season) throws Exception {
		return query赛季总数据(season, vo -> vo.get胜率(), SortOrder.DESC);
	}

	public List<TeamVo> query赛季总数据(String season,
			Function<TeamVo, Comparable<?>> sortColumn, SortOrder order)
			throws Exception;

	public default List<TeamVo> query场均数据(String season) throws Exception {
		return query场均数据(season, vo -> vo.get胜率(), SortOrder.DESC);
	}

	public List<TeamVo> query场均数据(String season,
			Function<TeamVo, Comparable<?>> sortColumn, SortOrder order)
			throws Exception;

	public TeamVo query赛季总数据ByName(String nameOfTeam, String season)
			throws Exception;

	public TeamVo query场均数据ByName(String nameOfTeam, String season)
			throws Exception;

	public List<TeamVo> query每场比赛表现(String season, TeamVo player)
			throws Exception;

	public List<TeamVo> query各个赛季表现(TeamVo player) throws Exception;
	
	/**
	 * @return <br>
	 * 返回包括4条项目的列表<br>
	 * 第一条项目为排名1/4的值<br>
	 * 第n条项目为排名n/4的值<br>
	 */
	public default List<TeamVo> query所有球队赛季表现四分位数(String season,Function<TeamVo, Comparable<?>> sortColumn) throws Exception{
		ArrayList<TeamVo> result=new ArrayList<>();
		List<TeamVo> allData=query场均数据(season,sortColumn,SortOrder.ESC);
		int maxIndex=allData.size()-1;
		if(maxIndex<0){
			return result;
		}
		result.add(allData.get(maxIndex/4));
		result.add(allData.get(maxIndex/2));
		result.add(allData.get(maxIndex*3/4));
		result.add(allData.get(maxIndex));
		return result;
	}
	
	public Map<String,Double> query克制指数(TeamVo team) throws Exception;
}
