package bhwz.seac3.blservice.query;

import java.util.List;

import bhwz.seac3.vo.PlayerVo;
import bhwz.seac3.vo.TeamVo;

public interface SeasonQueryBLService {
	/**
	 * 所有返回的Season按照时间降序排列，最近赛季的index为0
	 */
	List<String> queryAllSeasons() throws Exception;
	/**
	 * 所有返回的Season按照时间降序排列，最近赛季的index为0
	 */
	List<String> queryJoinedSeasons(PlayerVo player) throws Exception;
	/**
	 * 所有返回的Season按照时间降序排列，最近赛季的index为0
	 */
	List<String> queryJoinedSeasons(TeamVo team) throws Exception;

}
