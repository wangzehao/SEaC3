package bhwz.seac3.blservice.query;

import java.rmi.Remote;
import java.util.List;

import bhwz.seac3.vo.MatchVo;

public interface MatchQueryBLService extends Remote{
      public List<MatchVo> queryAllMatches(String season) throws Exception;
      public List<MatchVo> queryByTeam(String team,String season) throws Exception;
      public List<MatchVo> queryByPlayer(String player,String season) throws Exception;
      public MatchVo queryById(String id) throws Exception;
      public List<MatchVo> query当天比赛() throws Exception;
}
