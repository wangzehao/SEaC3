package bhwz.seac3.blservice.query;

import java.rmi.Remote;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import bhwz.seac3.util.ListSorter.SortOrder;
import bhwz.seac3.vo.PlayerVo;

public interface PlayerQueryBLService extends Remote {

	public default List<PlayerVo> query赛季总数据(String season) throws Exception {
		return query赛季总数据(season, vo -> vo.get比赛得分(), SortOrder.DESC);
	}

	public List<PlayerVo> query赛季总数据(String season,
			Function<PlayerVo, Comparable<?>> sortColumn, SortOrder order)
			throws Exception;

	public default List<PlayerVo> query场均数据(String season) throws Exception {
		return query场均数据(season, vo -> vo.get比赛得分(), SortOrder.DESC);
	}

	public List<PlayerVo> query场均数据(String season,
			Function<PlayerVo, Comparable<?>> sortColumn, SortOrder order)
			throws Exception;

	public PlayerVo query赛季总数据ByName(String nameOfPlayer, String season)
			throws Exception;

	public PlayerVo query场均数据ByName(String nameOfPlayer, String season)
			throws Exception;

	public List<PlayerVo> query当天球员数据(long date) throws Exception;

	public List<PlayerVo> query每场比赛表现(String season, PlayerVo player)
			throws Exception;

	public List<PlayerVo> query各个赛季表现(PlayerVo player) throws Exception;
	
	/**
	 * @return <br>
	 * 返回包括4条项目的列表<br>
	 * 第一条项目为排名1/4的值<br>
	 * 第n条项目为排名n/4的值<br>
	 */
	public default List<PlayerVo> query所有球员赛季表现四分位数(String season,Function<PlayerVo, Comparable<?>> sortColumn) throws Exception{
		ArrayList<PlayerVo> result=new ArrayList<>();
		List<PlayerVo> allData=query场均数据(season,sortColumn,SortOrder.ESC);
		int maxIndex=allData.size()-1;
		if(maxIndex<0){
			return result;
		}
		result.add(allData.get(maxIndex/4));
		result.add(allData.get(maxIndex/2));
		result.add(allData.get(maxIndex*3/4));
		result.add(allData.get(maxIndex));
		return result;
	}
}
