package bhwz.seac3.blservice;

public interface ReDisplayControllerBLService {
	public void register(AutoRedisplay item);
	public void remove(AutoRedisplay item);
	public void redisplay();
	public void forceRedisplay();
}
