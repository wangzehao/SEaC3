package bhwz.seac3.blservice.predict;

import bhwz.seac3.vo.PredictVo;
import bhwz.seac3.vo.TeamVo;

public interface PredictBLService {
	public PredictVo predict(TeamVo team1,TeamVo team2) throws Exception;
}
