package bhwz.seac3.blservice.datacomputer;

import bhwz.seac3.vo.PlayerVo;

public interface PerGamePlayerDataComputeBLService {
	public PlayerVo computePlayerDataPerGame(PlayerVo allGameData);
	public PlayerVo computePlayerDataAllGame(PlayerVo allGameData);
}
