package bhwz.seac3.blservice.datacomputer;

import bhwz.seac3.vo.MatchVo;

public interface MatchDataAppendBLService {
	public void AppendMatchDataToPlayerAndTeam(MatchVo match) throws Exception;
}
