package bhwz.seac3.blservice.datacomputer;

import bhwz.seac3.vo.TeamVo;

public interface PerGameTeamDataComputeBLService {
	public TeamVo computeTeamDataPerGame(TeamVo allGameData);
	public TeamVo computeTeamDataAllGame(TeamVo allGameData);
}
