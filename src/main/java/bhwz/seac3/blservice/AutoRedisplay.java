package bhwz.seac3.blservice;

import java.awt.Component;
import java.awt.Container;

public interface AutoRedisplay {
	public void redisplay();
	
	/**
	 * 如果一个对象没有获得焦点，为了性能考虑返回值应为false
	 * @return
	 */
	public default boolean needAutoRedisplay(){
		if(this instanceof Component)
			return hasFocus((Component)this);
		else
			return false;
	}
	
	static boolean hasFocus(Component com){
		if(com.hasFocus()){
			return true;
		}else{
			try{
				Container con=(Container)com;
				for(Component c:con.getComponents()){
					if(hasFocus(c)){
						return true;
					}
				}
			}catch(ClassCastException e){
			}
		}
		return false;
	}
}
