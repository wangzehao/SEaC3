from scipy import integrate as itg
from scipy.stats import norm
import sys

def p(x,y):
    return p1(x)*p2(y)

if __name__ == '__main__':
    args=sys.argv[1:]
    e1=float(args[0])
    v1=float(args[1])
    e2=float(args[2])
    v2=float(args[3])
    rv1=norm(loc=e1,scale=v1**0.5)
    rv2=norm(loc=e2,scale=v2**0.5)
    p1=rv1.pdf
    p2=rv2.pdf
    win1=itg.dblquad(p, 0, 1, lambda x:x, lambda x:1)
    win2=itg.dblquad(p, 0, 1, lambda x:0, lambda x:x)
    print(win1[0]/(win1[0]+win2[0]))
    print(win2[0]/(win1[0]+win2[0]))