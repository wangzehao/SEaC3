#-*- coding:utf-8 -*-
import sys
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import norm

def get_data():
    # example data
    mu = 100 # mean of distribution
    sigma = 15 # standard deviation of distribution
    x = []
    args=sys.argv[2:]
    for arg in args:
        x.append(float(arg))
    return x

#you can write your code here
def draw(path):
    #get input data
    x = get_data()
    # the histogram of the data
    plt.hist(x, bins=20, color='g', alpha=0.5)
    #show image
    
    
    avg=mean(x)
    s=var(x)**0.5
    
    x2=np.linspace(0.4,0.6,50)
    rv=norm(loc=avg,scale=s)
    plt.plot(x2,rv.pdf(x2),color='red')
    
    plt.savefig(path)
    
    print(avg)
    print(s)
    
def mean(list):
    narray=np.array(list)
    sum=narray.sum()
    return sum/len(list)

def var(list):
    N=len(list)
    narray=np.array(list)
    sum1=narray.sum()
    narray2=narray*narray
    sum2=narray2.sum()
    mean=sum1/N
    var=sum2/N-mean**2
    return var
#the code should not be changed
if __name__ == '__main__':
    draw(sys.argv[1])