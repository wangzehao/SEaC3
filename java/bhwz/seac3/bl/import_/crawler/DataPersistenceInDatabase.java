package bhwz.seac3.bl.import_.crawler;

import java.text.SimpleDateFormat;

import bhwz.seac3.bl.RedisplayControllerBL;
import bhwz.seac3.bl.import_.MatchImportBL;
import bhwz.seac3.vo.MatchVo;

public class DataPersistenceInDatabase extends DataPersistence {

	static MatchImportBL bl = new MatchImportBL(new RedisplayControllerBL());

	public void save(MatchVo vo) {
		if(vo!=null){
			bl.importOneMatch(vo);
			System.out.println("save "+new SimpleDateFormat("yyyy-MM-dd").format(vo.getDate())+"_"+vo.getTeam1()+"_"+vo.getTeam2());
		}
	}

}
