package bhwz.seac3.bl.import_.crawler;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

import bhwz.seac3.vo.MatchVo;
import bhwz.seac3.vo.PlayerScoreTableVo;

public class DataPersistenceInFile extends DataPersistence {
	private String path;

	DataPersistenceInFile(String path) {
		this.path = path;
	}

	@Override
	public void save(MatchVo vo) {
		String season = vo.getSeason();
		String date = new SimpleDateFormat("MM-dd").format(new Date(vo
				.getDate()));
		String team1 = vo.getTeam1();
		String team2 = vo.getTeam2();
		File folder = new File(path + vo.getSeason());
		if (!folder.exists()) {
			folder.mkdir();
		}
		File file = new File(folder, season + "_" + date + "_" + team1 + "-"
				+ team2);
		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		String wordSeparator = ";";
		String lineSeparator = System.getProperty("line.separator");
		String line1 = stringMaker(wordSeparator, date, team1 + "-" + team2,
				vo.getTeam1Score() + "-" + vo.getTeam2Score());
		String line2 = stringMaker(
				";",
				vo.getSectionScores().toArray(
						new String[vo.getSectionScores().size()]));

		String[] playerPerformance = { "", "" };
		for (int t = 0; t < 2; t++) {
			Set<PlayerScoreTableVo> playerSet = null;
			if (t == 0) {
				playerSet = vo.getTeam1PlayersScore();
			} else {
				playerSet = vo.getTeam2PlayersScore();
			}
			String[] players = new String[playerSet.size()];
			int i = 0;
			for (PlayerScoreTableVo p : playerSet) {
				players[i] = convertToString(wordSeparator, p);
				i++;
			}
			playerPerformance[t] = stringMaker(lineSeparator, players);
		}

		String content = stringMaker(lineSeparator, line1, line2, team1,
				playerPerformance[0].substring(0,
						playerPerformance[0].length() - 1), team2,
				playerPerformance[1].substring(0,
						playerPerformance[1].length() - 1));
		writeFile(file, content);
		System.out.println("save in " + file.getName());
	}

	private void writeFile(File file, String content) {
		BufferedWriter bw = null;
		try {
			bw = new BufferedWriter(new FileWriter(file, true));
			bw.write(content);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// 关闭流
			if (bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					bw = null;
				}
			}
		}
	}

	private String stringMaker(String wordSeparator, String... words) {
		StringBuilder sb = new StringBuilder();
		for (String s : words) {
			sb.append(s + wordSeparator);
		}
		return sb.toString();
	}

	private String convertToString(String wordSeparator,
			PlayerScoreTableVo onePlayer) {
		return stringMaker(wordSeparator, onePlayer.get球员名(),
				onePlayer.get位置(), toMinutes(onePlayer.get在场时间()),
				onePlayer.get投篮命中数() + "", onePlayer.get投篮出手数() + "",
				onePlayer.get三分命中数() + "", onePlayer.get三分出手数() + "",
				onePlayer.get罚球命中数() + "", onePlayer.get罚球出手数() + "",
				onePlayer.get进攻篮板数() + "", onePlayer.get防守篮板数() + "",
				onePlayer.get总篮板数() + "", onePlayer.get助攻数() + "",
				onePlayer.get抢断数() + "", onePlayer.get盖帽数() + "",
				onePlayer.get失误数() + "", onePlayer.get犯规数() + "",
				onePlayer.get个人得分() + "");
	}

	private String toMinutes(int seconds) {
		return seconds % 60 == 0 ? seconds / 60 + "" : seconds / 60 + ":"
				+ seconds % 60;
	}

	public static void main(String[] args) {
		HtmlParser parser = new HtmlParser();
		Downloader down = new Downloader();
		MatchVo vo = parser
				.encapsulateMatchVo(down
						.download("http://www.basketball-reference.com/boxscores/201312090PHI.html"));
		DataPersistenceInFile d = new DataPersistenceInFile("/home/ivan/");
		d.save(vo);
	}

}
